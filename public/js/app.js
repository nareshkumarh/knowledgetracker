function AppJS() {
	var _this = this;
	//scrollMove method
	//myApp.scrollMove(100);

	_this.scrollMove = function(top) {
		$("html,body").stop().animate({
			scrollTop : top
		}, "fast");
	};
	_this.checkbox = function(options) {
		$((options) ? options.id || "[data-ui='checkbox'],[data-ui='radio']" : "[data-ui='checkbox'],[data-ui='radio']").checkBox(options);
		return this;
	};
	_this.dragDrop = function(options) {
		$("[data-dragdrop='btn']").dragDrop(options || {});
		return this;
	};
	_this.scroll = function(scrollID, options) {
		$(scrollID).pureScroll(options || {});
		_this.apply(arguments);
		return this;
	};
	_this.formreset = function() {
		$("input,select,textarea", "form").each(function(e) {
			var self = $(this);
			if (self.attr("type") == "radio" || self.attr("type") == "checkbox") {
				self.prop("checked", false);
			} else if ($("option", self).size()) {
				self.val($("option", self).eq(0).val());
			} else {
				self.val("");
			}
		});
		return this;
	};
	_this.widgetCollapse = function() {
		$('[data-ui="widget-collapse"]').each(function() {
			var self = $(this), _parent = self.attr("data-widget-parent"), _child = self.attr("data-widget-child");
			$('[data-ui="widget-collapse"]').widgetCollapse({
				storagename : "widget",
				parent : _parent,
				child : _child
			});
		});
		_this.apply(arguments);
		return this;
	};
	_this.alphabetClickEvent = function() {
		$("#alphabet_list [data-browse-tab-link]").unbind().on("click", function(e) {
			var self = $(this), self_target = $(self.attr("data-browse-tab-link"));
			if (self_target.size()) {
				self.parent("li").siblings().removeClass("active");
				self.parent("li").addClass("active");
				$("#alphabet_list ~ .alphabet-list-content .alphabet-pane").not(self_target.addClass("active")).removeClass("active");
			}
			var alphabet_content_h = $("#alphabet_list").height();
			alphabet_content_h = (alphabet_content_h < 200) ? 200 : alphabet_content_h;
			_this.scroll("#alphabet_scroll", {
				contentHeight : alphabet_content_h
			});
			_this.apply(arguments);
			e.preventDefault();
		});
		_this.apply(arguments);
		return this;
	};
	_this.loadPage = function(obj) {
		if (obj && obj.url) {
			if (obj.throbber == undefined || obj.throbber != false) {
				_this.throbber(obj.throbber);
			}
			_this.ajax(obj);
		}
		_this.apply(arguments);
		return this;
	};
	_this.apply = function(arguments) {
		for (var i in arguments) {
			if ( typeof arguments[i] === "function") {
				arguments[i]();
			}
		}
		return this;
	};
	//generic methods
	_this.ajax = function(post) {
		var methods = {
			init : function() {
				if (post.data || post.datatype) {
					_this.ajaxID = $[post.method||"post"](post.url, post.data, function(data) {
					}, post.datatype);
				} else {
					_this.ajaxID = $[post.method||"post"](post.url, function(data) {
						//console.log(data)
					});
				}
				_this.ajaxID.done(methods.done);
				_this.ajaxID.fail(methods.fail);
				_this.ajaxID.always(methods.always);
			},
			done : function(data) {
				if (post.success) {
					post.success.apply(this, arguments);
				}
				_this.throbber();
			},
			fail : function(data) {
				if (post.fail) {
					post.fail.apply(this, arguments);
				}
				_this.throbber();
			},
			always : function(data) {
				if (post.always) {
					post.always.apply(this, arguments);
				}
			},
			error : function(data) {
				if (post.error) {
					post.error.apply(this, arguments);
				}
				_this.throbber();
			}
		};

		methods.init();
		_this.apply(arguments);
		return this;
	};
	_this.ajaxAbort = function() {
		if ($('#progress_outer').size()) {
			$('#progress_outer').remove();
		}
		_this.ajaxID.abort();
		_this.apply(arguments);
		return this;

	};
	_this.ajaxError = function(e) {
		if (String(e.status).match(/404/g)) {
			_this.notification({
				message : "Page not found",
				duration : 5000
			});
		}
		return this;
	};

	// If you need throbber please use pelow code
	//myApp.throbber({throbberText : "Please Give your content here"});

	// If you dont need throbber please use pelow code
	//myApp.throbber();

	_this.throbber = function(obj) {
		var l_obj = {}, l_throbber = $("#throbber"), l_throbber_text = $("#throbber_text", l_throbber), l_throbberAnimation = $("#throbber_animation", l_throbber), l_throbberContinue = $("#throbber_continue", l_throbber), l_throbber_continue_text = $("#throbber_continue_text", l_throbber);
		if (obj) {
			l_obj.throbberText = obj.throbberText || "Loading Please Wait...";
			l_obj.throbberContinueText = obj.throbberContinueText || "This process is taking more time than usual. Do you want to keep trying? Or cancel";
			l_throbber_text.html(l_obj.throbberText), l_throbber_continue_text.html(l_obj.throbberContinueText);
			l_throbber.show();
			_this.loader();
		} else {
			l_throbber.hide();
		}
		return this;
	};

	_this.loader = function() {
		if ($('[data-load-animate]').is(":visible")) {
			$('[data-load-animate]').animate({
				'background-position' : '-=10%'
			}, 500, 'linear', function() {
				_this.loader();
			});
		}
		return this;
	};

	_this.applyPageMethod = function(method, data) {
		if (_this[method]) {
			_this[method](data);
		}
		return this;
	};

	_this.notification = function(obj) {
		if (obj) {
			if (!$("[data-notifyid='" + obj.id + "']").size()) {
				var cloned = $("[data-ui='alert_notification']").eq(0).hide().clone();
				loc_status = {
					"success" : "fa-check-circle",
					"danger" : "fa-times-circle",
					"info" : "fa-info-circle",
					"warning" : "fa-exclamation-circle"
				};
				loc_icon = '<i class="fa ' + loc_status[obj.status || "info"] + '"></i>';
				obj.message = ( typeof obj.message === "string") ? [obj.message] : obj.message;
				var html = ( function(obj) {
						var i, loc_joinstr = "<ul class='list-unstyled'>";
						for (i in obj.message) {
							loc_joinstr += '<li>' + loc_icon + obj.message[i] + '</li>';
						}
						loc_joinstr += "</ul>";
						return loc_joinstr;
					}(obj));
				if (obj.alertid) {
					cloned.attr("data-alertid", obj.alertid);
				}

				cloned.attr("data-notifyid", obj.id).append(html);
				$("[data-ui='alert_notification']:first").after(cloned);
				cloned.removeClass("alert-info").addClass("alert-" + obj.status || "info").fadeIn(1000);
				cloned.delay(obj.duration || 6000).fadeOut(1000, function() {
					$(this).remove();
				});
			}
		}
		return this;
	};

	_this.handlebar = function(_obj) {
		var obj = _obj;
		obj.id = $(obj.id);

		if (obj.id.size()) {
			var source = $(obj.template).html();
			var template = Handlebars.compile(source);
			if (obj.json == undefined) {
				obj.id.html(template());
			} else {
				obj.id.html(template(obj.json));
			}
			if ( typeof obj.complete == "function")
				obj.complete.apply();
		}
		return _this;
	};
	_this.matchHeight = function(id1, id2) {
		$(id1).css("height", "auto");
		$(id2).css("height", "auto");
		var h = Math.max($(id1).height(), $(id2).height()) + 56;
		$(id1).height(h);
		$(id2).height(h);
		return this;
	};

	_this.scrollTop = function() {
		var _window = $(window), _scrollpos = _window.scrollTop(), _newscrollpos;
		$(window).scroll(function() {
			_newscrollpos = _window.scrollTop();
			if (_newscrollpos > _scrollpos) {
				$('[data-scrolltop]').stop().animate({
					right : 5,
					opacity : '0.7'
				}, 500);
			} else {
				$('[data-scrolltop]').stop().animate({
					right : -70
				}, 500);
			}
		});
		$(document).off('click', '[data-scrolltop]').on('click', '[data-scrolltop]', function(e) {
			e.preventDefault();
			e.stopPropagation();
			$('html,body').stop().animate({
				scrollTop : 0
			}, 1500);
		});
		return this;
	};
	_this.dateConvertion = function(obj) {
		if (obj != undefined && obj.data != undefined && obj.key) {
			var i;
			for (i in obj.data) {
				if ( typeof obj.data[i][obj.key] == "string") {
					var startsplit = obj.data[i][obj.key].split(",");
					obj.data[i][obj.key] = new Date(startsplit[2], startsplit[0] - 1, startsplit[1], startsplit[3], startsplit[4]);
				}
			}
		}
	};
	_this.objectToArray = function(obj, joinedArray) {
		var loc_joinedArray = joinedArray || [];
		for (var i in obj) {
			if ( typeof obj[i] === "string" || typeof obj[i] === "number") {
				loc_joinedArray.push(obj[i]);
			} else {
				_this.objectToArray(obj[i], loc_joinedArray);
			}
		}
		return loc_joinedArray;
	};
	_this.tryCatch = function(trycode, callback) {
		try {
			trycode();
		} catch(er) {
			if (callback && typeof callback === "function") {
				callback(er);
			}
		}
	};

	_this.apply = function(arguments, data) {
		for (var i in arguments) {
			if ( typeof arguments[i] === "function") {
				arguments[i](data);
			}
		}
	};

	_this.jqGrid = function(options) {
		if (options) {
			var grid_selector = options.gridId, pager_selector = options.gridPagerId;
			//replace icons with FontAwesome icons like above
			var constructColumnHideShow = function(colName, colModel) {
				var colCofigPrefix = '<ul class="dropdown-menu arrow-right pull-right" id="column_configure"><div class="dropdown-header">Setting <button type="button" class="close dropdown-close" aria-hidden="true" data-dropdown-close="speciality_list"><i class="fa fa-times-circle"></i></button></div>';
				var colConfigSurfix = '</ul>';
				var li = [];

				for ( i = 0; i < colModel.length; i++) {
					li.push('<li class="list-item"><div data-ui="checkbox" class="checkbox-wrap margin-right10px">' + '<input type="checkbox" value="' + colModel[i].name + '" checked="checked" id="' + colModel[i].name + '" name="' + colModel[i].name + '"  class="checkbox-hide">' + '<span class="checkbox-style"><i class="fa fa-check"></i></span><span class="checkbox-text">' + colName[i] + '</span></div></li>');
				}
				$(".ui-jqgrid-title").append(colCofigPrefix + li.join("") + colConfigSurfix);
				$(".ui-jqgrid .ui-jqgrid-titlebar-close").hide();
			};

			//resize to fit page size
			$(window).on('resize.jqGrid', function() {
				$(grid_selector).jqGrid('setGridWidth', $(".ui-jqgrid:visible").parent().width());
			});

			if (options.title && String(options.title).match(/column_hide_show/g) == null) {
				options.title = '<span class="jq-grid-head text-bold"> ' + options.title + ' </span> <a class="ui-jqgrid-titlebar-config" id="column_hide_show" role="link" data-toggle="dropdown" title="' + options.title + ' - setting" ><i class="fa fa-wrench"></i></a>';
			}

			var jqGrid = jQuery(grid_selector).jqGrid({
				data : options.data,
				jsonReader : {
					repeatitems : false
				},
				datatype : "local",
				//height: 325,
				colNames : options.column.colNames,
				colModel : options.column.colModel,
				viewrecords : true,
				rowNum : options.rowNum,
				//emptyrecords : options.noRecordMessage || "<p class='text-danger'><strong>No records found!</strong></p>",
				//rowList:[10,20,30],
				pager : pager_selector,
				altRows : true,
				//toppager: true,
				multiselect : options.multiselect,
				//multiselect: true,
				//multikey: "ctrlKey",

				subGrid : options.subGrid || false,
				subGridRowExpanded : function(param1, param2) {
					if ( typeof options.subGridRowExpanded === "function") {
						options.subGridRowExpanded(param1, param2);
					}
				},
				subGridRowColapsed : function(param1, param2) {
					if ( typeof options.subGridRowColapsed === "function") {
						options.subGridRowColapsed(param1, param2);
					}
				},
				multiboxonly : true,
				loadComplete : function() {
					var table = this;
					options.sortColumnName = options.sortColumnName || $(grid_selector).getGridParam("sortname");
					options.sortOrder = $(grid_selector).getGridParam("sortorder");
					if ($(grid_selector).getGridParam("records") != 0) {
						setTimeout(function() {

							if (!$("#column_configure").size()) {
								constructColumnHideShow(options.column.colNames, options.column.colModel);
							}

							clearTimeout(this);
						}, 1);
						if (options.loadComplete) {
							options.loadComplete(options);
						}
						options.page = arguments;
						/*setTimeout(function() {
						 $(options.gridId).jqGrid('setGridWidth', $(".ui-jqgrid:visible").parent().width());
						 clearTimeout(this);
						 }, 100);*/
						$(options.gridId).jqGrid('setGridWidth', $(".ui-jqgrid:visible").parent().width());
						_this.jqGridResize();
						$(pager_selector).show();
					} else {
						$("tbody tr", grid_selector).after("<tr><td colspan='12'><div class='alert alert-warning margin10px pull-hide text-center' data-norecords='" + grid_selector + "'>" + (options.noRecordsMessage || "No records found!") + "</div></td></tr>");
						$(pager_selector).hide();
						$("[data-norecords='" + grid_selector + "']").show();
					}
					$(".ui-jqgrid .ui-jqgrid-titlebar-close").hide();
					$("tr.jqgrow:odd").css("background", "#f7f7f7");

				},
				onCellSelect : function(row, cell, cellcontent) {
					if (options.onCellClick) {
						options.onCellClick(row, cell, cellcontent);
					}
				},
				onSelectRow : function(rowid, status, e) {
					if (options.onRowClick) {
						options.onRowClick(rowid, status, e);
					}
				},
				onPaging : function(id) {
					if (options.onPaging) {
						options.onPaging(id, jqGrid, options);
					}
				},
				onSortCol : function(name, index) {
					$(options.gridId).jqGrid('setGridWidth', $(".ui-jqgrid:visible").parent().width());
					if (options.onSortCol) {
						options.onSortCol(name, index, options);
					}

				},
				caption : options.title

			});

			if (options.gridCustomPager) {
				options.gridCustomPager(options);
			}

			$(window).triggerHandler('resize.jqGrid');
			//trigger window resize to make the grid get the correct size

			$(document).off("click", "#column_configure [data-ui='checkbox']").on("click", "#column_configure [data-ui='checkbox']", function(e) {
				var self = $(this), chkBoxInput = $("input[type='checkbox']", self), chkBoxIcon = $(".checkbox-style i", self), checked = $("#column_configure input[type='checkbox']:checked").length;
				if (chkBoxInput.prop("checked") && checked > 1) {
					chkBoxInput.prop("checked", false);
					$(chkBoxIcon).addClass("fa-uncheck").removeClass("fa-check");
					$(grid_selector).jqGrid('hideCol', chkBoxInput.val());
					$(grid_selector).jqGrid('setGridWidth', $(".ui-jqgrid").parent().width());
				} else {
					chkBoxInput.prop("checked", true);
					$(chkBoxIcon).addClass("fa-check").removeClass("fa-uncheck");
					$(grid_selector).jqGrid('showCol', chkBoxInput.val());
					$(grid_selector).jqGrid('setGridWidth', $(".ui-jqgrid").parent().width());
				}
				e.stopPropagation();
			});

		}

		return this;
	};
};
