$(document).ready(function() {
	var now = new Date();
	var myApp = new AppJS();
	myApp.json = new AppJson();

	myApp.errorPage = "#";
	myApp.load_page_id = $("#load_page");
	myApp.root = "pages/";
	myApp.rootModal = "modal/";
	myApp.eleKeypage = $("[data-load-keypage]");
	myApp.core = cnsiJS.core;
	myApp.historyPage = myApp.historyPage || [];
	myApp.current_latLng = {
		lat : "",
		lng : ""
	};
	myApp.dobStartDate = myApp.core.date.format((now.getFullYear() - 110) + "/" + (now.getMonth()) + "/" + now.getDate(), "mm/dd/yyyy");
	myApp.dobEndDate = myApp.core.date.format(now.getFullYear() + "/" + (now.getMonth() + 1) + "/" + now.getDate(), "mm/dd/yyyy");

	myApp.common = function() {
		var myApp = this;
		$(document).off('click', '[data-dropdown-close],.dropdown-close').on('click', '[data-dropdown-close],.dropdown-close', function() {
			var self = $(this);
			self.closest('.open').removeClass('open');
		});
		return this;
	};
	myApp.zeroFormat = function(val) {
		return (val < 10) ? "0" + val : val;
	};
	myApp.mediaScreen = function() {
		var loc_userAgent = navigator.userAgent, myApp = this, i;
		myApp.window = $(window);
		var cond = {
			miniDevice : (myApp.window.width() <= 480),
			smallDevice : (myApp.window.width() <= 768),
			mediumDevice : (myApp.window.width() > 768 && myApp.window.width() <= 1024),
			largeDevice : (myApp.window.width() > 1024)
		};
		for (i in cond) {
			if (cond[i]) {
				myApp.deviceName = i;
				break;
			}
		}
		return this;
	};
	myApp.customSerialize = function(obj, avoidkey) {
		if (obj) {
			var o, data = {}, formid;
			for (o in obj) {
				var backendField = $("[name='" + obj[o].name + "']"), formid = backendField.parents("form");
				if (avoidkey == undefined || String(obj[o].name).match(avoidkey) == null) {
					data[obj[o].name] = (String(data[obj[o].name]).match(/undefined/)) ? obj[o].value : data[obj[o].name] + "," + obj[o].value;
				}
				if (String(backendField.attr("data-backend")).match(/^$|undefined/) == null) {
					var backendAttr = String(backendField.attr("data-backend")).replace(/[\'\[\]]/g, ""), backendObj = (backendAttr.match(",")) ? backendAttr.split(",") : [backendAttr];
					for (i in backendObj) {
						var ikey = backendObj[i], iArray = ikey.split(":");
						data[iArray[0]] = iArray[1];
					}
				}
			}
			$("input[disabled]", formid).each(function() {
				var self = $(this), selfname = self.attr("name");
				if (avoidkey == undefined || String(selfname).match(avoidkey) == null) {
					data[selfname] = (String(data[selfname]).match(/undefined/)) ? self.val() : data[selfname] + "," + self.val();
				}
			});
			return data;
		} else {
			return obj;
		}
	};

	myApp.datePicker = function(id, options) {
		var loc_detaults = {
			forceParse : true,
			format : "mm/dd/yyyy",
			startDate : null,
			endDate : null,
			autoclose : false,
			orientation : "auto bottom"
		}, loc_options;
		loc_options = $.extend({}, loc_detaults, options || {});
		$("#" + $(id).prev().attr('id')).datepicker(loc_options).on("changeDate", function(e) {
			if (loc_options.changeDate) {
				loc_options.changeDate(e);
			}
		}).on("blur", function() {
			var self = $(this);
			setTimeout(function() {
				if (self.val() != "") {
					$(".error-msg").slideUp("fast", function() {
						$(this).remove();
					});
				}
				clearTimeout(this);
			}, 500);
		});
		if (options.focus != undefined && !options.focus) {
			$(document).off("click", id).on("click", id, function() {
				$("#" + $(this).prev().attr('id')).focus();
			});
		}
		$(document).off("click", "#datepicker_close").on("click", "#datepicker_close", function() {
			$(".error-msg:visible").remove();
		});
		return this;
	};

	myApp.dateRangePicker = function(obj) {
		if (obj) {
			$(obj.id).daterangepicker({
				startDate : obj.startDate || moment().subtract('days', now.getDay() - 1),
				endDate : obj.endDate || moment().add("days", (6 - (now.getDay() - 1))),
				//minDate : '01/01/2012',
				//maxDate : now,
				// dateLimit : {
				// //days : (12 * 30)
				// },
				showDropdowns : true,
				showWeekNumbers : true,
				timePicker : false,
				timePickerIncrement : 1,
				timePicker12Hour : true,
				ranges : {
					'Last 30 Days' : [moment().subtract('days', 30), moment()],
					'Last 60 Days' : [moment().subtract('days', 60), moment()],
					'Last 90 Days' : [moment().subtract('days', 90), moment()],
					'Current Calendar Year' : [moment().startOf('years', 1), moment().endOf('years', 1)],
					'Last Calendar Year' : [moment().year(now.getFullYear() - 1).startOf('years', 1), moment().year(now.getFullYear() - 1).endOf('years', 1)],
				},
				opens : 'left',
				buttonClasses : ['btn cust-btn'],
				applyClass : 'btn-success',
				cancelClass : 'btn-cancel',
				format : 'MM/DD/YYYY',
				separator : ' to ',
				locale : {
					applyLabel : 'Apply',
					fromLabel : 'From',
					toLabel : 'To',
					customRangeLabel : 'Custom Range',
					daysOfWeek : ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
					monthNames : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
					firstDay : 1
				}
			}, function(start, end) {
				var split1 = String(start.format('YYYY/MM/DD')).split("/");
				var split2 = String(end.format('YYYY/MM/DD')).split("/");
				var sdate = new Date(split1[0], split1[1] - 1, split1[2]);
				var edate = new Date(split2[0], split2[1] - 1, split2[2]);
				$(".error-msg").remove();
			});
		}
	};

	myApp.timePicker = function(id, options) {
		var loc_detaults = {
			defaultTime : false,
			showSeconds : false,
			minuteStep : 10,
		}, loc_options, loc_timepickerId = $(id);
		loc_options = $.extend({}, loc_detaults, options || {});

		loc_timepickerId.timepicker(loc_detaults);
		loc_timepickerId.on('show.timepicker', function(e) {
			$(this).timepicker('setTime', e.time.value);
		});
		return this;
	};
	myApp.setMaxHeight = function() {
		var heightArray = [], element = $("#counterwrapper .minheight");
		element.css("height", "auto").each(function(i, ival) {
			var self = $(this);
			heightArray.push(self.height());
			if (i >= element.length - 1) {
				var maxheight = myApp.core.array.sorting(heightArray, "asc");
				maxheight.reverse();
				$("#counterwrapper .minheight").css("height", (Number(maxheight[0]) + 10) + "px");
			}
		});
		myApp.jqGridResize();
	};

	myApp.setPageHeight = function() {
		this.matchHeight("#navigation_left", "#page_content");
		//$("#navigation_left", "#page_content").height($("#navigation_left").height()+56);
		return this;
	};
	myApp.jqGridResize = function() {
		$(".ui-jqgrid").each(function() {
			var self = $(this), gridid = String(self.attr("id")).replace(/gbox_/ig, "#");
			$(gridid).jqGrid('setGridWidth', $(".ui-jqgrid:visible").parent().width());
			if (gridid == "#jqgrid-instantunregistertask" || gridid == "#jqgrid-instantmodifytask") {
				var gridresizee = $(".modal-dialog").width() - 30;
				$(gridid).jqGrid('setGridWidth', gridresizee);
			} else {
				$(gridid).jqGrid('setGridWidth', $(".ui-jqgrid:visible").parent().width());
			}
		});
		myApp.setPageHeight();
	};

	myApp.leftMenuPos = function() {
		var myApp = this, device = myApp.mediaScreen().deviceName, elem = $("#page_content");
		myApp.largeDeviceFn = function() {
			elem.stop().animate({
				marginLeft : "240px"
			}, 400, function() {
				$('.leftmeu-collapse-btn .fa').addClass('fa-indent').removeClass('fa-outdent');
				$('[data-menu="submenu"].open').children('ul').slideDown();
				myApp.jqGridResize();
				myApp.setMaxHeight();

			});
			$('.footerver').addClass('pull-hide').removeClass('pull-hide');
			return this;
		};
		myApp.smallDeviceFn = function() {
			elem.stop().animate({
				marginLeft : "50px"
			}, 400, function() {
				$('.leftmenu-trigger .fa').addClass('fa-outdent').removeClass('fa-indent');
				$('[data-menu="submenu"]').children('ul').slideUp();
				myApp.jqGridResize();
				myApp.setMaxHeight();
			});
			$('.footerver').addClass('pull-hide').removeClass('pull-hide');
			return this;
		};
		myApp.miniDeviceFn = function() {
			elem.stop().animate({
				marginLeft : "0"
			}, 400, function() {
				$('.leftmenu-trigger .fa').addClass('fa-indent').removeClass('fa-outdent');
				myApp.jqGridResize();
				myApp.setMaxHeight();
			});
			$('.footerver').addClass('pull-hide').removeClass('pull-hide');
			return this;
		};
		if (device == "largeDevice" || device == "mediumDevice") {
			myApp.largeDeviceFn();
		} else if (device == "smallDevice") {
			myApp.smallDeviceFn();
		} else {
			myApp.miniDeviceFn();
		}
		$(document).click(function(event) {
			var target = $(event.target), parent = target.closest('[data-template]');
			if (parent.hasClass('nav-sidebar') || parent.hasClass('sidebar-profile')) {
				elem.stop().animate({
					marginLeft : "240px"
				}, 400);
				$('.leftmenu-trigger .fa').removeClass('fa-outdent').addClass('fa-indent');
				myApp.jqGridResize();
				myApp.setMaxHeight();
			}
		});
		$('.footerver').addClass('pull-hide').removeClass('pull-hide');
		return this;
	};

	myApp.sideMenuCollapse = function() {
		var myApp = this, device = myApp.mediaScreen().deviceName;
		$(document).off("click", '[data-trigger="leftmenu-collapse"]').on("click", '[data-trigger="leftmenu-collapse"]', function() {
			var elem = $("#page_content"), elem_margin = elem.css('margin-left'), icon = $(this).children("i.fa");
			var elem = $("#page-container");
			var icon = $(this).children("i.fa");
			elem.toggleClass("collapse-leftmenu");
			icon.toggleClass("fa-angle-double-right fa-angle-double-left");
			if ($("#navigation_left").width() < 100) {
				$("li.open ul").hide();
			} else {
				$("li.open ul").show();
			}
			myApp.scroll($("[data-ui='scroll']"), {
				trigger : true
			});
			if (elem_margin == '240px') {
				myApp.leftMenuPos().smallDeviceFn();
			} else {
				myApp.leftMenuPos().largeDeviceFn();
			}
			myApp.setPageHeight();

		});
		this.apply(arguments);
		return this;
	};
	myApp.loginEnginePage = function() {
		//$("#loginFormComponent").formValidate();
		$(document).off("click", '#loginSubmit').on("click", '#loginSubmit', function() {
			if ($("#loginFormComponent").formValidate("validate")) {
				alert("hi");

			} else {
				alert("hello");
			}
			return false;
		});

	};
	myApp.menuClick = function() {
		var myApp = this;
		myApp.loc_smallPortMenuHide = function(self) {
			if (myApp.deviceName.match(/(small)/ig)) {
				//self.closest("li.open").children("ul").hide();
			}
		};
		$(document).off("click", "[data-link]:not('.active'),[data-menu='submenu'] > a:not('.active')").on("click", "[data-link]:not('.active'),[data-menu='submenu'] > a:not('.active')", function() {
			var self = $(this), loc_url = self.attr("data-link"), loc_keypage = (self.attr("data-keypage")) ? self.attr("data-keypage") : "none";
			if (loc_url != "" && loc_keypage != "none") {
				myApp.loadPage({
					url : myApp.root + loc_url,
					success : function(data) {
						myApp.load_page_id.html(data);
						myApp.eleKeypage.attr("data-load-keypage", loc_keypage);
						myApp.historyPage.push({
							keypage : loc_keypage,
							url : loc_url
						});
						myApp.setLeftMenuActive(self).applyPageMethod(loc_keypage);
						if (myApp.deviceName.match(/(small)/ig)) {
							myApp.leftMenuPos().smallDeviceFn();
						} else if (myApp.deviceName.match(/(mini)/ig)) {
							myApp.leftMenuPos().miniDeviceFn();
						}
					},
					fail : function(e) {
						myApp.ajaxError(e);
					}
				});
				if (myApp.deviceName.match(/(mini)/ig)) {
					$('[data-trigger="leftmenu-collapse"]').trigger("click");
				}
				if (!$('.collapse-leftmenu').size()) {
					myApp.loc_smallPortMenuHide(self);
				}
				if (myApp.deviceName.match(/(mini)/ig) && !$('.collapse-leftmenu').size() || myApp.deviceName.match(/(large)/ig) && $('.collapse-leftmenu').size()) {
					self.closest("li.open").children("ul").hide();
				}
			} else {
				if (self.parent("li[data-menu='submenu']").size()) {
					myApp.setLeftMenuActive(self);
				} else {
					if (self.attr("data-modal-url") == undefined && self.attr("id") != "logout") {
						myApp.notification({
							"id" : "page_not_found",
							message : "Page not found",
							duration : 5000,

						});
					}
					if (myApp.deviceName.match(/(mini)/ig)) {
						$('[data-trigger="leftmenu-collapse"]').trigger("click");
					}
				}
			}
			myApp.apply(arguments);
		});

		$(document).off("click", "#page_content,.navbar-collapse,.navbar-brand > a:not('.leftmeu-collapse-btn')").on("click", "#page_content,.navbar-collapse,.navbar-brand > a:not('.leftmeu-collapse-btn')", function() {
			if (myApp.deviceName.match(/(small)/ig)) {
				myApp.leftMenuPos().smallDeviceFn();
			} else if (myApp.deviceName.match(/(mini)/ig)) {
				myApp.leftMenuPos().miniDeviceFn();
			}
		});
		return this;
	};

	myApp.setLeftMenuActive = function(ele) {
		var self = ele, li = self.parent("li"), siblings = li.siblings(".submenu");
		var iconright = li.find("[data-toggle-class]"), iconrightsiblings = siblings.find("[data-toggle-class]");
		siblings.removeClass("open").children("ul").slideUp();
		iconrightsiblings.removeClass(iconrightsiblings.attr("data-toggle-class"));
		if (li.hasClass("open") && li.children("ul:visible").size()) {
			li.children("ul").slideUp().parent('li').removeClass("open");
			iconright.removeClass(iconright.attr("data-toggle-class"));
		} else {
			li.children("ul").slideDown().parent('li').addClass("open");
			iconright.addClass(iconright.attr("data-toggle-class"));
		}
		$("[data-link] , [data-menu='submenu'] > a").not(self.not(self.parent("li[data-menu='submenu']").children("a")).addClass("active")).removeClass("active");
		return this;
	};

	myApp.navigationTopMenu = function() {
		var myApp = this, loc_topMenu = $("#tmp_cnt_navigationtop"), loc_tmpID = "#" + loc_topMenu.attr("data-template");

		return this;
	};

	myApp.navigationLeftProfile = function() {
		var loc_profile = $("#tmp_cnt_navigation_profile"), loc_tmpProfileID = "#" + loc_profile.attr("data-template");
		return this;
	};

	myApp.navigationLeftProfileToggle = function() {
		$("#tmp_cnt_navigation_profile.active").removeClass("active");
		return this;
	};

	myApp.navigationLeftMenu = function() {
		myApp.setPageHeight().sideMenuCollapse();
		return this;
	};
	myApp.navigationLeft = function() {
		var myApp = this;
		myApp.navigationLeftProfile().navigationLeftMenu();
		myApp.setMaxHeight();
		return this;
	};

	myApp.headerLogo = function() {
		var myApp = this;

		myApp.setPageHeight();
		return this;
	};
	myApp.footer = function() {
		var myApp = this;
		myApp.setPageHeight();
		return this;
	};

	myApp.dashboard = function() {
		var initCharts = function() {
			var charts = $('[data-ui="piechart"]');
			/*charts.easyPieChart({
				trackColor : "#fff",
				scaleColor : "#f0ff00",
				lineCap : "square",
				lineWidth : 7,
				animate : 1000,
				size : 70,
				onStart : function() {
					var elem = $(this.el), chart_label = elem.siblings('.chart-label');
					var percent = Math.round(elem.data('percent'));
					if (percent <= 30) {
						this.options.barColor = '#fcba64';
						chart_label.text('Normal').css('color', '#fcba64');
					} else if (percent > 30 && percent <= 60) {
						this.options.barColor = '#e88f49';
						chart_label.text('Moderate').css('color', '#e88f49');
					} else {
						this.options.barColor = '#de1a1c';
						chart_label.text('High').css('color', '#de1a1c');
					}
				},
				onStep : function(value) {
					var span = this.$el.find('span');
					if (!span.hasClass('chart-icon')) {
						span.text(~~value);
					}
				}
			});*/
		}();

		myApp.setPageHeight();

		myApp.jqGrid(myApp.json.recentloggedJSON).setPageHeight();
		myApp.jqGrid(myApp.json.recenttaskJSON).setPageHeight();
		myApp.setMaxHeight();

		$(".hidepaneltitle .ui-jqgrid-titlebar").hide();

		jQuery.jqplot.config.enablePlugins = true;
		plot1 = jQuery.jqplot('chart1', [[['&nbsp;', 30], ['&nbsp;', 30], ['&nbsp;', 10], ['&nbsp;', 30]]], {
			title : ' ',
			seriesDefaults : {
				shadow : false,
				renderer : jQuery.jqplot.PieRenderer,
				rendererOptions : {
					padding : 2,
					sliceMargin : 2,
					showDataLabels : true
				}
			},
			legend : {
				show : true,
				location : 'e'
			}
		});
		setTimeout(function() {
			plot1.replot({
				resetAxes : true
			});
			clearTimeout(this);
		}, 1000);
		$(window).on("resize", function() {
			if ($("#chart1").size()) {
				setTimeout(function() {
					plot1.replot({
						resetAxes : true
					});
					clearTimeout(this);
				}, 1000);
			}

		});

		return this;
	};

	myApp.autoComplete = function(obj) {
		var myApp = this;
		$(obj.element).typeahead({
			source : obj.source || [],
			items : obj.minLength || 8,
			minLength : obj.minLength || 1,
			updater : function(selection) {
				if (obj.customSelectEvent != undefined) {
					if ( typeof obj.customSelectEvent == 'function') {
						var loc_obj = myApp.json.providerlocatorJSON.ProviderLocatorPopulateVO.m_mapSpecilityTree, loc_key = "";
						for (var i in loc_obj) {
							for (var j in loc_obj[i]) {
								if (loc_obj[i][j] == selection) {
									loc_key = j;
									break;
								}
							}
						}
						obj.customSelectEvent(selection, loc_key);
					}
				} else {
					return selection;
				}
			}
		});
	};

	myApp.autoPageLoad = function(parent) {
		var myApp = this, loc_arg = arguments;
		$("[data-load-page]", parent || document).each(function() {
			var self = $(this), loc_keypage = (self.attr("data-keypage")) ? self.attr("data-keypage") : "none";

			myApp.loadPage({
				throbber : {},
				url : myApp.root + self.attr("data-load-page"),
				success : function(data) {
					self.html(data);
					if (loc_keypage != "none") {
						myApp.applyPageMethod(loc_keypage, data);
					}
					myApp.apply(loc_arg, data);
				},
				fail : myApp.ajaxError
			});
		});
		return this;
	};

	myApp.menuClick = function() {
		var myApp = this;
		myApp.loc_smallPortMenuHide = function(self) {
			if (myApp.deviceName.match(/(small)/ig)) {
				self.closest("li.open").children("ul").hide();
			}
		};
		$(document).off("click", "[data-link]:not('.active'),[data-menu='submenu'] > a:not('.active')").on("click", "[data-link]:not('.active'),[data-menu='submenu'] > a:not('.active')", function() {
			var self = $(this), loc_url = self.attr("data-link"), loc_keypage = (self.attr("data-keypage")) ? self.attr("data-keypage") : "none";
			if (loc_url != "" && loc_keypage != "none") {
				myApp.loadPage({
					url : myApp.root + loc_url,
					success : function(data) {
						myApp.load_page_id.html(data);
						myApp.eleKeypage.attr("data-load-keypage", loc_keypage);
						myApp.historyPage.push({
							keypage : loc_keypage,
							url : loc_url
						});
						myApp.setLeftMenuActive(self).applyPageMethod(loc_keypage);
						myApp.navigationLeftProfileToggle();
					},
					fail : function(e) {
						myApp.ajaxError(e);
					}
				});
				if (myApp.deviceName.match(/(mini)/ig)) {
					$('[data-trigger="leftmenu-collapse"]').trigger("click");
				}
				if (!$('.collapse-leftmenu').size()) {
					myApp.loc_smallPortMenuHide(self);
				}
				if (myApp.deviceName.match(/(mini)/ig) && !$('.collapse-leftmenu').size() || myApp.deviceName.match(/(large)/ig) && $('.collapse-leftmenu').size()) {
					self.closest("li.open").children("ul").hide();
				}
			} else {
				if (self.parent("li[data-menu='submenu']").size()) {
					myApp.setLeftMenuActive(self);
				} else {
					if (self.attr("data-modal-url") == undefined && self.attr("id") != "logout") {
						myApp.notification({
							"id" : "page_not_found",
							message : "Page not found",
							duration : 5000,

						});
					}
					if (myApp.deviceName.match(/(mini)/ig)) {
						$('[data-trigger="leftmenu-collapse"]').trigger("click");
					}
				}
			}
			myApp.apply(arguments);
		});
		return this;
	};

	myApp.modalHandle = function() {
		var myApp = this;
		var loc_removeEscapeNotifiation = function() {
			if ($("[data-alertid='modalEscape']").size()) {
				$("[data-alertid='modalEscape']").stop().fadeOut(function() {
					$(this).remove();
				});
			}
		};
		myApp.modalId = $("#pageModal");
		myApp.modalId.on("shown.bs.modal", function() {
			myApp.autoPageLoad(myApp.modalId);
		});
		myApp.modalId.on("hidden.bs.modal", function() {
			$(".modal-dialog", myApp.modalId).html("");
			$(".modal-dialog", myApp.modalId).removeClass($(myApp.modalWhere).attr("data-modal-class"));
			loc_removeEscapeNotifiation();
		});
		$(document).on("keyup keypress", function() {
			loc_removeEscapeNotifiation();
		});
		return this;
	};

	myApp.modalClick = function() {
		var myApp = this;
		$(document).off("click", "[data-ui='modal']").on("click", "[data-ui='modal']", function(e) {
			var self = $(this);
			$(".modal-dialog", myApp.modalId).attr("data-load-page", myApp.rootModal + self.attr('data-modal-url')).attr("data-keypage", self.attr('data-keypage'));
			$(".modal-dialog", myApp.modalId).addClass($(self).attr("data-modal-class"));
			myApp.modalWhere = self;
			myApp.modalId.modal("toggle");
		});
		return this;
	};

	myApp.collapsible = function() {
		var myApp = this;
		$(document).off("click", "[data-ui='collapsible']").on("click", "[data-ui='collapsible']", function(e) {
			var self = $(this), loc_parent_selector = self.attr('data-collapse-parent'), loc_child_selector = self.attr('data-collapse-child');
			var loc_parent = self.closest(loc_parent_selector), loc_child = loc_parent.find(loc_child_selector);
			loc_child.slideToggle(500, function() {
				self.children('.fa').toggleClass(self.attr('data-toggle-class'));
				myApp.setPageHeight();
			});
		});
	};

	myApp.popOver = function(obj) {
		obj = obj || {};
		$('[data-toggle="popover"]').popover({
			animation : obj.animation,
			content : obj.content,
			placement : obj.placement,
			container : obj.container
		});
		$(document).off('click', '[data-popover-dismiss="true"]').on('click', '[data-popover-dismiss="true"]', function() {
			var self = $(this), loc_parent = self.closest('.popover');
			loc_parent.removeClass('in').remove();
		});
		return this;
	};

	myApp.modalBox = function() {
		this.popOver({
			content : "Popover Content comes here"
		}).toolTip({
			content : "Tooltip Content comes here"
		});
	};
	myApp.toolTip = function(obj) {
		obj = obj || {};
		$('[data-toggle="tooltip"]').tooltip({
			animation : obj.animation,
			content : obj.content,
			placement : obj.placement,
			container : obj.container
		});
		return this;
	};

	myApp.datepicker = function() {
		var myApp = this, loc_startDate = myApp.zeroFormat(now.getMonth() + 1) + "/01/" + now.getFullYear(), loc_endDate = myApp.zeroFormat(now.getMonth() + 1) + "/" + myApp.core.date.numberOfDays(now.getFullYear(), now.getMonth() + 1) + "/" + now.getFullYear(), loc_initDate = loc_startDate + " to " + loc_endDate;
		myApp.datePicker('#datepicker_group .input-group-addon', {
			focus : false,
			startDate : myApp.dobStartDate,
			endDate : myApp.dobEndDate
		}).timePicker('#timepicker', {}).dateRangePicker({
			id : "#daterangepicker",
			startDate : loc_startDate,
			endDate : loc_endDate
		});
	};

	myApp.calendarEventView = function() {
		if (calendarEvent != "") {
			$("#calendar_view_event").fullCalendar("destroy");
		}
		var calendarEvent = $("#calendar_view_event").fullCalendar({
			header : {
				left : 'prev,next,today',
				center : 'title',
				right : 'month,agendaWeek,agendaDay'
			},
			lazyFetching : false,
			editable : false,
			events : [],
			eventMouseover : function(event, jsEvent, view) {
				var ev = ($(jsEvent.target).hasClass("fc-event")) ? $(jsEvent.target) : $(jsEvent.target).closest(".fc-event");
				var pos = (ev.position().top <= 50) ? "bottom" : "top";
				var desc = (String(event.description).length < 30) ? String(event.description) : String(event.description).substr(0, 30) + "...";
				ev.attr("title", desc);
			},
			viewDisplay : function() {
				// calendar event getting date string to date fortmate change
				myApp.dateConvertion({
					data : myApp.json.calendarJSON.eventData,
					key : "start"
				});
				myApp.dateConvertion({
					data : myApp.json.calendarJSON.eventData,
					key : "end"
				});
				// calendar event getting date string to date fortmate change
				$('#calendar_view_event').fullCalendar('removeEvents');
				$('#calendar_view_event').fullCalendar('addEventSource', myApp.json.calendarJSON.eventData);
				dataDate = $('#calendar_view_event').fullCalendar('getDate');
			}
		});
	};

	myApp.ranges = function() {
		var initCharts = function() {
			var charts = $('[data-ui="piechart"]');
			charts.easyPieChart({
				trackColor : "#d9dde0",
				scaleColor : "#d3d3d5",
				lineCap : "square",
				lineWidth : 10,
				animate : 1000,
				size : 110,
				onStart : function() {
					var elem = $(this.el), chart_label = elem.siblings('.chart-label');
					var percent = Math.round(elem.data('percent'));
					if (percent <= 30) {
						this.options.barColor = '#95c136';
						chart_label.text('Normal').css('color', '#95c136');
					} else if (percent > 30 && percent <= 60) {
						this.options.barColor = '#e88f49';
						chart_label.text('Moderate').css('color', '#e88f49');
					} else {
						this.options.barColor = '#de1a1c';
						chart_label.text('High').css('color', '#de1a1c');
					}
				},
				onStep : function(value) {
					var span = this.$el.find('span');
					if (!span.hasClass('chart-icon')) {
						span.text(~~value);
					}
				}
			});
		}();

		$('#range_slider1').rangeSlider();
		$('#range_slider2').rangeSlider({
			minValue : 0,
			maxValue : 200,
			range : true
		});
	};

	myApp.autocomplete = function() {
		var myApp = this;
		myApp.prepopulateSpecialityList = myApp.objectToArray(myApp.json.providerlocatorJSON.ProviderLocatorPopulateVO.m_mapSpecilityTree);
		myApp.autoComplete({
			element : "#autocomplete",
			items : 15,
			source : function(query, process) {
				return $.ajax({
					url : "",
					type : 'post',
					success : function(data) {
						data = ["10, 2nd Avenue", "11, 3rd Avenue", "12, 4th Avenue", "13, 5th Avenue", "14, 6th Avenue"];
						return process(data);
					}
				});
			},
		});
		myApp.autoComplete({
			element : "#tokeninput",
			source : myApp.prepopulateSpecialityList,
			items : 15,
			customSelectEvent : function(selection, key) {
				var self = $(this), loc_value = selection, loc_token_container = $('.token-field'), loc_token_id = $('[data-browse-selected="' + key + '"]');
				if (loc_token_container.hasClass('pull-hide')) {
					loc_token_container.removeClass('pull-hide');
				}
				if (loc_token_id.size() <= 0) {
					loc_token_container.append(myApp.tokenCreation(loc_value, key));
				} else {
					myApp.notification({
						"message" : [loc_value + " already selected."],
						"duration" : "5000",
						"status" : "danger"
					});
					loc_token_id.fadeOut(500).fadeIn(500);
				}
			}
		});
	};
	myApp.userProfileLink = function() {
		$('#password').passwordStrength();
		$("#addprofileUserformComponent").formValidate();
		$(document).off("click", '#userProfilesubmit').on("click", '#userProfilesubmit', function() {
			if ($("#addprofileUserformComponent").formValidate("validate")) {
				alert("hi");

			} else {
				alert("hello");
			}
			return false;
		});
	};
	myApp.userForgotPassword = function() {

	};
	myApp.userPasswordExpiry = function() {
		$('#password').passwordStrength();
	};

	myApp.tokenCreation = function(options) {
		if (options) {
			var myApp = this, token_elem = '<div data-token-tagrelation="' + $(options.formTargetField).attr("data-token-relation") + '" class="label label-token" data-value="' + options.value + '" data-browse-selected="' + options.key + '">' + options.value + '<a href="javascript:void(0)" title="close" data-token-dismiss="close" class="close" tabindex="-1"> &#120; </a></div>';
			$(token_elem).append(token_elem);
			$(options.parentid).show();
			myApp.tokenDeletion(options);
			return token_elem;
		}
	};

	myApp.tokenDeletion = function(options) {
		$(document).off('click', '[data-token-dismiss]').on('click', '[data-token-dismiss]', function() {
			var self = $(this), loc_id = self.parent().attr('data-browse-selected'), loc_token_container = $(options.parentid || '.tokenresult'), selfParent = $("[data-token-relation='" + self.parents("[data-token-tagrelation]").attr("data-token-tagrelation") + "']");
			self.parent(".label").fadeOut(500, function() {
				$(this).remove();
				if (options) {
					if (!$(".label", options.parentid).size()) {
						$(options.parentid).hide();
					}
					if (options.formTargetField) {
						$("input[type='checkbox']", selfParent).each(function() {
							var target = $(this);
							if (target.val() == self.parent(".label").attr("data-value")) {
								target.prop("checked", false);
							}
						});
					}
				}
				$("[data-token-result='" + self.parents("[data-token-tagrelation]").attr("data-token-tagrelation") + "']:empty").hide();
			});

		});
		return this;
	};

	myApp.users = function() {
		myApp.jqGrid(myApp.json.recentuserJSON).setPageHeight();
		$(".hidepaneltitle .ui-jqgrid-titlebar").hide();
		$(document).off('click', '#excelsave').on('click', '#excelsave', function() {

		});
		$(document).off('click', '[data-toggle="deleteConfirmbox"]').on('click', '[data-toggle="deleteConfirmbox"]', function() {
			$("#confirmbox").confirmBox({
				message : ["Do you want to delete this Record"],
				yes : function() {
				}
			});
			setTimeout("$('#confirmbox').fadeOut();", 3000);
			$("#confirmbox").confirmBox("show");
		});

		setTimeout("$('.statealert').fadeOut();", 1000);

	};
	myApp.tokenHandle = function(option) {
		var tokenField = option.tokenField, tokenFieldFor = tokenField.attr('data-token');
		var tokenFieldRelation = $("[data-token-relation='" + tokenFieldFor + "']");

		$(document).off('click', "[data-token='" + tokenFieldFor + "']").on('click', "[data-token='" + tokenFieldFor + "']", function() {
			$("[data-token-relation='" + tokenFieldFor + "']").toggle();

		});
		$(document).off('click focus', "[data-token='" + tokenFieldFor + "']").on('click focus', "[data-token='" + tokenFieldFor + "']", function() {
			$("[data-token-relation='" + tokenFieldFor + "']").show();
		}).on("keyup", "[data-token='" + tokenFieldFor + "']", function() {
			var self = $("[data-token='" + tokenFieldFor + "']");
			var searchVal = self.val().toUpperCase();
			var searchCon = $(".selectcontentwrap label", "[data-token-relation='" + tokenFieldFor + "']");
			$(searchCon).each(function() {
				var curntObj = $(this), $text = curntObj.text().toUpperCase();
				($text.indexOf(searchVal) != "-1") ? curntObj.show() : curntObj.hide();
			});
			if (!$(".selectcontentwrap label:visible", "[data-token-relation='" + tokenFieldFor + "']").size()) {
				if (!$(".selectcontentwrap .no-data", "[data-token-relation='" + tokenFieldFor + "']").size()) {
					$(".selectcontentwrap", "[data-token-relation='" + tokenFieldFor + "']").append("<span class='pie-first no-data'>--No Data--</span>");
				}
			} else {
				$(".selectcontentwrap .no-data", "[data-token-relation='" + tokenFieldFor + "']").remove();
			}
		});

		$(document).off("click", "[data-token-submit='" + tokenFieldFor + "']").on("click", "[data-token-submit='" + tokenFieldFor + "']", function() {
			$("[data-token-result='" + tokenFieldFor + "']").html("");

			$("[data-token-relation='" + tokenFieldFor + "'] input[type='checkbox']").each(function() {
				var target = $(this);
				if (target.prop("checked")) {
					var token = myApp.tokenCreation({
						value : target.val(),
						parentid : "[data-token-result='" + tokenFieldFor + "']",
						formTargetField : target.parents("[data-token-relation]")
					});
					$("[data-token-result='" + tokenFieldFor + "']").append(token);
				}
			});
			$("[data-token-result='" + tokenFieldFor + "']:empty").hide();
			$("[data-token-relation='" + tokenFieldFor + "']").hide();
		});
		$(document).off("click", "[data-token-cancel='" + tokenFieldFor + "']").on("click", "[data-token-cancel='" + tokenFieldFor + "']", function() {
			$("[data-token-relation='" + tokenFieldFor + "']").hide();
		});
	};
	myApp.userAdd = function() {
		$('#password').passwordStrength();

		$("[data-token]").each(function() {
			myApp.tokenHandle({
				tokenField : $(this)
			});
		});
		myApp.addUserSubmit();
		$("#addUserformComponent").formValidate();
		$(document).off("click", '#submitUser').on("click", '#submitUser', function() {
			var weekprocess = $('.progress-bar').text();
			
			if ($("#addUserformComponent").formValidate("validate") && weekprocess != 'Weak' ) {
			alert('hi');

			} else {
				alert("hello");
			}
			weekprocess == 'Weak' ? $('#password').focus() : ' ';
			return false;
		});
		$(document).off("focus", '#password').on("focus", '#password', function() {
			$('#popover-left .popover').show();
		 
		});
		$(document).off("blur", '#password').on("blur", '#password', function() {
			$('#popover-left .popover').hide();
		 
		});
	};

	myApp.userEditModal = function() {

		$('#password').passwordStrength();
		$("[data-token]").each(function() {
			myApp.tokenHandle({
				tokenField : $(this)
			});
		});

		$("#editUserformComponent").formValidate();
		$(document).off("click", '#updateUser').on("click", '#updateUser', function() {
			if ($("#editUserformComponent").formValidate("validate")) {
				alert("hi");

			} else {
				alert("hello");
			}
			return false;
		});

	};

	myApp.addUserSubmit = function() {
		$(document).off('click', '#submitUser').on('click', '#submitUser', function() {
			var a = roleid_One.value;
			var b = roleId_Two.value;
			alert(a);
		});
	};

	myApp.importUser = function() {

	};

	myApp.role = function() {
		myApp.jqGrid(myApp.json.recentroleJSON).setPageHeight();
		$(document).off('click', '[data-toggle="deleteConfirmbox"]').on('click', '[data-toggle="deleteConfirmbox"]', function() {
			$("#confirmbox").confirmBox({
				message : ["Do you want to delete this Record"],
				yes : function() {
				}
			});
			$("#confirmbox").confirmBox("show");
		});
		setTimeout("$('.statealert').fadeOut();", 1000);
		myApp.textareamaxlength();

	};
	myApp.addRole = function() {
		$("#roleselector").formValidate();
		$(document).off("click", '#roleSubmitted').on("click", '#roleSubmitted', function() {
			if ($("#roleselector").formValidate("validate")) {
				alert("hi");

			} else {
				alert("hello");
			}
			return false;
		});

	};

	myApp.engine = function() {

		myApp.jqGrid(myApp.json.recentengineJSON).setPageHeight();
		$("#formComponent").formValidate();
		$(document).off('click', '[data-toggle="deleteConfirmbox"]').on('click', '[data-toggle="deleteConfirmbox"]', function() {
			$("#confirmbox").confirmBox({
				message : ["Do you want to delete this Record"],
				yes : function() {
				}
			});
			$("#confirmbox").confirmBox("show");
		});
		myApp.textareamaxlength();

	};
	myApp.editEngine = function() {
		myApp.textareamaxlength();
	};

	myApp.constant = function() {
		myApp.jqGrid(myApp.json.recentconstantJSON).setPageHeight();
		$("#formComponent").formValidate();
		$(document).off('click', '[data-toggle="deleteConfirmbox"]').on('click', '[data-toggle="deleteConfirmbox"]', function() {
			var self = $(this), loc_url = self.attr("data-link-button"), loc_keypage = (self.attr("data-keypage")) ? self.attr("data-keypage") : "none";
			$("#confirmbox").confirmBox({
				message : ["Do you want to delete this Record"],
				yes : function() {

					// console.log(loc_url);
					myApp.loadPage({
						url : myApp.root + loc_url,
						success : function(data) {
							myApp.load_page_id.html(data);
							myApp.eleKeypage.attr("data-load-keypage", loc_keypage);
							myApp.historyPage.push({
								keypage : loc_keypage,
								url : loc_url
							});
							myApp.applyPageMethod(loc_keypage);
						},
						fail : function(e) {
							myApp.ajaxError(e);
						}
					});

				}
			});
			$("#confirmbox").confirmBox("show");
		});
		myApp.textareamaxlength();
	};
	//console.log($("body").attr("class"));

	myApp.constantEdit = function() {
		var myApp = this;
		$(document).off("click", "#ConstantEditUpdate").on("click", "#ConstantEditUpdate", function() {
			var self = $(this), loc_url = self.attr("data-link-button"), loc_keypage = (self.attr("data-keypage")) ? self.attr("data-keypage") : "none";
			// console.log(loc_url);
			myApp.loadPage({
				url : myApp.root + loc_url,
				success : function(data) {
					myApp.load_page_id.html(data);
					myApp.eleKeypage.attr("data-load-keypage", loc_keypage);
					myApp.historyPage.push({
						keypage : loc_keypage,
						url : loc_url
					});
					myApp.applyPageMethod(loc_keypage);
				},
				fail : function(e) {
					myApp.ajaxError(e);
				}
			});
			myApp.apply(arguments);
			myApp.modalId.modal("hide");
		});
		myApp.textareamaxlength();
	};
	myApp.constantUpdate = function() {
		myApp.jqGrid(myApp.json.recentconstantupdateJSON).setPageHeight();

	};

	myApp.constantDelete = function() {
		myApp.jqGrid(myApp.json.recentremoveconstantJSON).setPageHeight();
	};
	myApp.textareamaxlength = function() {
		$(document).off('keyup', 'textarea').on('keyup', 'textarea', function() {
			var text_max = $(this).attr('data-maxlengthattr') || 250;
			var text_length = $(this).val().length;
			var text_remaining = text_max - text_length;
			 
			if ($(this).next('.feedbackof_textarea').size() == 0) {
				$(this).after('<lable class="pull-right btn-success pad-x5px feedbackof_textarea">You have typed' + ' ' + '<span class="counterincrease"> ' + text_remaining + '</span> / ' + text_max +' ' + 'characters remaining</label>');
			} else {
				if (text_length == 0) {
					$(this).next().children().text('0');
				} else {
					$(this).next().children().text(text_remaining);
				}
				if (text_remaining <= 0) {
					$(this).val($(this).val().substr(0, text_max));
					$(this).next().children().text('0');
				};
			};
		});
	};

	myApp.pickList = function() {
		myApp.jqGrid(myApp.json.recentpicklistJSON).setPageHeight();
		$(document).off('click', '[data-toggle="deleteConfirmbox"]').on('click', '[data-toggle="deleteConfirmbox"]', function() {
			$("#confirmbox").confirmBox({
				message : ["Do you want to delete this Record"],
				yes : function() {
				}
			});
			$("#confirmbox").confirmBox("show");
		});
		myApp.textareamaxlength();
	};

	myApp.ruleTag = function() {
		myApp.jqGrid(myApp.json.recentruletagJSON).setPageHeight();
		$(document).off('click', '[data-toggle="deleteConfirmbox"]').on('click', '[data-toggle="deleteConfirmbox"]', function() {
			$("#confirmbox").confirmBox({
				message : ["Do you want to delete this Record"],
				yes : function() {
				}
			});
			$("#confirmbox").confirmBox("show");
		});
	};

	myApp.instantService = function() {
		$(document).off('click', '#instantupload').on('click', '#instantupload', function() {
			$(".instantnone").show();
			myApp.jqGrid(myApp.json.recentinstantserviceJSON).setPageHeight();
			myApp.jqGridResize();
			return false;
		});
		$(document).off("click", '[data-wizard-menu]').on("click", '[data-wizard-menu]', function() {
			var self = $(this);
			$("[data-wizard-menu]").removeClass("active");
			$(self).addClass("active");
			$(".wizard-panel").removeClass("active");
			$(".wizard-panel#" + self.attr("data-wizard-menu")).addClass("active");
			myApp.jqGrid(myApp.json.recentinstantserviceRegisterJSON).setPageHeight();
			myApp.jqGridResize();
		});

		$(document).off('click', '#instantsearchsubmit').on('click', '#instantsearchsubmit', function() {
			$("[data-wizard-menu='menuTwo']").trigger("click");
			myApp.jqGridResize();

		});

		$(document).off('click', '[data-toggle="deleteConfirmbox"]').on('click', '[data-toggle="deleteConfirmbox"]', function() {
			$("#confirmbox").confirmBox({
				message : ["Do you want to delete this Record"],
				yes : function() {
				}
			});
			$("#confirmbox").confirmBox("show");
		});
	};

	myApp.instantServiceUNRegister = function() {
		myApp.jqGrid(myApp.json.recentinstantServiceUNRegisterJSON).setPageHeight();
		myApp.jqGridResize();

	};

	myApp.instantServiceModify = function() {
		myApp.jqGrid(myApp.json.recentinstantServiceModifyJSON).setPageHeight();
		myApp.jqGridResize();
	};

	myApp.stateVariable = function() {
		myApp.jqGrid(myApp.json.recentstatevariableJSON).setPageHeight();
		myApp.jqGridResize();
		var statevariableRadio = function(radioVal) {
			(radioVal == "Dynamic") ? $('#static_variablewrap').removeClass('hide') : $('#static_variablewrap').addClass('hide');
		};
		$(document).off("change", 'input[name="VariableRadio"]').on("change", 'input[name="VariableRadio"]', function() {
			var self = $(this), val = self.val();
			statevariableRadio(val);
		});
		myApp.textareamaxlength();
		setTimeout("$('.statealert').fadeOut();", 1000);
	};
	myApp.addRule = function() {

		$("#ruleselector").formValidate();
		$(document).off("click", '#roleSubmitted').on("click", '#roleSubmitted', function() {
			if ($("#ruleselector").formValidate("validate")) {
				alert("hi");

			} else {
				alert("hello");
			}
			return false;
		});

		myApp.textareamaxlength();

	};

	myApp.utilitiesValidator = function() {
		$("#validatorformComponent").formValidate();

		var demo2 = $('.demo2').bootstrapDualListbox({
			nonSelectedListLabel : 'Available to Validate',
			selectedListLabel : 'Assigned to Validate',
			preserveSelectionOnMove : 'moved',
			moveOnSelect : false,
			//nonSelectedFilter : 'ion ([7-9]|[1][0-2])'
		});

		$("#selectRepositry").on("change", function() {
			$("#engineDisabled").prop('disabled', false);
		});
		$("#engineDisabled").on("change", function() {
			$("#groupDisabled").prop('disabled', false);
		});
		$("#groupDisabled").on("change", function() {
			$("#ruleBoxwrap").prop('disabled', false);
		});
		$("#ruleBoxwrap").on("change", function() {
			$("#filteroptions").prop('disabled', false);
		});

		$(document).off("click", '#submitValidator').on("click", '#submitValidator', function() {

			var self = $('#validatorDBToggle');
			loc_parent_selector = self.attr('data-collapse-parent'), loc_child_selector = self.attr('data-collapse-child');
			var loc_parent = self.closest(loc_parent_selector), loc_child = loc_parent.find(loc_child_selector);
			loc_child.slideToggle(500, function() {
				self.children('.fa').toggleClass(self.attr('data-toggle-class'));
				myApp.setPageHeight();
			});
			$('#validatorTabWrap').show();
			$('.panel-title a').on('click', function() {
				$('.panel-body:visible').slideUp();
			});
			return false;

		});

		$(document).off("click", '#validatorTabWrap').on("click", '#validatorTabWrap', function() {
			// var self = $('#validatorchangeToggle');
			// loc_parent_selector = self.attr('data-collapse-parent'), loc_child_selector = self.attr('data-collapse-child');
			// var loc_parent = self.closest(loc_parent_selector), loc_child = loc_parent.find(loc_child_selector);
			// loc_child.slideToggle(500, function() {
			// self.children('.fa').toggleClass(self.attr('data-toggle-class'));
			// myApp.setPageHeight();
			// });
			// return false;
			console.log('hi')

		});

		$(document).off("click", "#resetofValidator").on("click", "#resetofValidator", function() {
			alert("Please Reset Your function here");
		});
		myApp.setMaxHeight();
		myApp.jqGrid(myApp.json.recentvalidatorupdateJSON).setPageHeight();
		myApp.jqGridResize();

	};

	myApp.validatorError = function() {
		myApp.jqGrid(myApp.json.recentvalidatorerrorJSON).setPageHeight();
		myApp.jqGridResize();
		myApp.setMaxHeight();
	};
	myApp.utilitiesComparator = function() {
		$("#sourceRepositry").on("change", function() {
			$("#destinationRepositry").prop('disabled', false);
			$(" #swapwrap").prop('disabled', false);
		});

		$("#destinationRepositry").on("change", function() {
			$("#selectengine").prop('disabled', false);
		});
		$("#selectengine").on("change", function() {
			$("#compareOption").prop('disabled', false);
		});
		$("#compareOption").on("change", function() {
			$("#daterangeFrom").prop('disabled', false);
		});
		$("#daterangeFrom").on("change", function() {
			$("#daterangeTo").prop('disabled', false);
		});
		$("#daterangeTo").on("change", function() {
			$("#selectGroup").prop('disabled', false);
		});
		$("#selectGroup").on("change", function() {
			$("#selectRule").prop('disabled', false);
		});
		$(document).off("click", '#swapwrap').on("click", '#swapwrap', function() {
			alert('SWAP Content');
		});

		$("#comparatorformComponent").formValidate();
		myApp.datePicker("#rangeFrom .input-group-addon", {
			focus : false
		});
		myApp.datePicker("#rangeTo .input-group-addon", {
			focus : false
		});
		$(document).off("click", '#comparatorValidate').on("click", '#comparatorValidate', function() {
			if ($("#comparatorformComponent").formValidate("validate")) {
				alert("hi comparator");
			} else {
				alert("hello Compartor");

			}
			// myApp.throbber({throbberText : "Please Give your content here"});
			// myApp.throbber();
			var self = $('#compareInfoToggle');
			loc_parent_selector = self.attr('data-collapse-parent'), loc_child_selector = self.attr('data-collapse-child');
			var loc_parent = self.closest(loc_parent_selector), loc_child = loc_parent.find(loc_child_selector);
			loc_child.slideToggle(500, function() {
				self.children('.fa').toggleClass(self.attr('data-toggle-class'));
				myApp.setPageHeight();
			});
			return false;

		});

		$(document).off('change', '#compareOption').on('change', '#compareOption', function() {
			$(".pull-hide").hide();
			$('#' + $(this).val()).show();
		});

		myApp.setMaxHeight();
		myApp.formreset();
		myApp.jqGrid(myApp.json.recentcomparatorJSON).setPageHeight();
		myApp.jqGridResize();

		$(document).off("click", "#gotoMigrate").on("click", "#gotoMigrate", function() {
			alert("Please Migrate Your function here");
		});
	};

	myApp.utilitiesdb2db = function() {
		$("#migratorformComponent").formValidate();
		myApp.datePicker("#rangeFrom .input-group-addon", {
			focus : false
		});
		myApp.datePicker("#rangeTo .input-group-addon", {
			focus : false
		});
		$("#migratorsourceRepositry").on("change", function() {
			$("#destinationRepositry").prop('disabled', false);
		});
		$("#destinationRepositry").on("change", function() {
			$("#selectengine").prop('disabled', false);
		});
		$("#selectengine").on("change", function() {
			$("#migrateOptionsList").prop('disabled', false);
		});
		$(document).off("click", '#migratorRule').on("click", '#migratorRule', function() {
			$('#migratorModifiedDates').slideToggle('slow');

		});
		var demo2 = $('.demo2').bootstrapDualListbox({
			nonSelectedListLabel : 'Available to Validate',
			selectedListLabel : 'Assigned to Validate',
			preserveSelectionOnMove : 'moved',
			moveOnSelect : false,
			//nonSelectedFilter : 'ion ([7-9]|[1][0-2])'
		});
		$(document).off("click", '#migratorValidate').on("click", '#migratorValidate', function() {
			if ($("#migratorformComponent").formValidate("validate")) {
				$("#migratorTabWrap .tab ul.nav-tabs li a").hide();
				$("[data-tr-select]").hide();
				var firstTab = "";
				$("[data-tabindexed]", "#migratorformComponent").each(function(i, ival) {
					var self = $(this), selfIndex = self.attr("data-tabindexed");
					if (self.prop("checked")) {
						$("[href='#tab" + selfIndex + "']", "#migratorTabWrap").show();
						$("[data-tr-select='trtab" + selfIndex + "']").show();
						//console.log("[href='#tab" + selfIndex + "']")
						if (firstTab == "") {
							firstTab = $("[href='#tab" + selfIndex + "']", "#migratorTabWrap");
						}
					}
					//console.log(i, ival, self, $(this));
					myApp.setPageHeight();
				});
				firstTab.trigger("click");

				$('#migratorTabWrap').show();
				myApp.setPageHeight();
				myApp.setMaxHeight();
			} else {
				alert("hello DB To DB");
			}
			var self = $('#migratorDBToggle');
			loc_parent_selector = self.attr('data-collapse-parent'), loc_child_selector = self.attr('data-collapse-child');
			var loc_parent = self.closest(loc_parent_selector), loc_child = loc_parent.find(loc_child_selector);
			loc_child.slideToggle(500, function() {
				self.children('.fa').toggleClass(self.attr('data-toggle-class'));
				myApp.setPageHeight();
			});
			return false;
		});
		$(document).off("click", "#gotoMigrate").on("click", "#gotoMigrate", function() {
			alert("Please Migrate Your function Here ");
			myApp.setPageHeight();
		});
		myApp.formreset();
		myApp.jqGridResize();
	};

	myApp.utilitiesdb2xml = function() {
		$("#migratordb2xmlformComponent").formValidate();
		myApp.datePicker("#rangeFrom .input-group-addon", {
			focus : false
		});
		myApp.datePicker("#rangeTo .input-group-addon", {
			focus : false
		});

		$("#migratordb2xmlsourceRepositry").on("change", function() {
			$("#db2xmlselectengine").prop('disabled', false);
		});
		$("#db2xmlselectengine").on("change", function() {
			$("#migratedb2xmlOptionsList").prop('disabled', false);
		});
		$("#migratedb2xmlOptionsList").on("change", function() {
			$("#migratordb2xmlModifiedDates").prop('disabled', false);
		});
		$("#migratedb2xmlOptionsList").on("change", function() {
			$("#migratordb2xmlfilename").prop('disabled', false);
		});
		$(document).off("click", '#migratorRule').on("click", '#migratorRule', function() {
			$('#migratordb2xmlModifiedDates').slideToggle('slow');
		});

		var demo2 = $('.demo2').bootstrapDualListbox({
			nonSelectedListLabel : 'Available to Validate',
			selectedListLabel : 'Assigned to Validate',
			preserveSelectionOnMove : 'moved',
			moveOnSelect : false,
			//nonSelectedFilter : 'ion ([7-9]|[1][0-2])'
		});

		$(document).off("click", "#migratordb2xmlValidate").on("click", "#migratordb2xmlValidate", function() {
			if ($("#migratordb2xmlformComponent").formValidate("validate")) {
				$("#migratorTabWrap .tab ul.nav-tabs li a").hide();
				$("[data-tr-select]").hide();
				var firstTab = "";
				$("[data-tabindexed]", "#migratordb2xmlformComponent").each(function(i, ival) {
					var self = $(this), selfIndex = self.attr("data-tabindexed");
					if (self.prop("checked")) {
						$("[href='#tab" + selfIndex + "']", "#migratorTabWrap").show();
						$("[data-tr-select='trtab" + selfIndex + "']").show();
						//console.log("[href='#tab" + selfIndex + "']")
						if (firstTab == "") {
							firstTab = $("[href='#tab" + selfIndex + "']", "#migratorTabWrap");
						}
					}
					//console.log(i, ival, self, $(this));
					myApp.setPageHeight();
				});
				firstTab.trigger("click");

				$('#migratorTabWrap').show();
				myApp.setPageHeight();
				myApp.setMaxHeight();
			} else {
				alert("hello DB To DB");
			}
			var self = $('#migratorDBtoXMLToggle');
			loc_parent_selector = self.attr('data-collapse-parent'), loc_child_selector = self.attr('data-collapse-child');
			var loc_parent = self.closest(loc_parent_selector), loc_child = loc_parent.find(loc_child_selector);
			loc_child.slideToggle(500, function() {
				self.children('.fa').toggleClass(self.attr('data-toggle-class'));
				myApp.setPageHeight();
			});
			return false;

		});

		myApp.formreset();
	};

	myApp.utilitiesxml2db = function() {
		$("#migratorxml2dbformComponent").formValidate();
		myApp.datePicker("#rangeFrom .input-group-addon", {
			focus : false
		});
		myApp.datePicker("#rangeTo .input-group-addon", {
			focus : false
		});

		$("#exampleInputFile").on("change", function() {
			$("#xml2dbdestination").prop('disabled', false);
		});
		$("#xml2dbdestination").on("change", function() {
			$("#migratexml2dbOptionsList").prop('disabled', false);
		});
		$("#migratexml2dbOptionsList").on("change", function() {
			$("#migratexml2dbOptionsList").prop('disabled', false);
		});

		$(document).off("click", '#migratorRule').on("click", '#migratorRule', function() {
			$('#migratorxml2dbModifiedDates').slideToggle('slow');
		});

		$(document).off("click", '#migratordb2xmlValidate').on("click", '#migratordb2xmlValidate', function() {
			alert("hi Fredric")
		});

	};

	myApp.migrateSuccessorFail = function() {

	};

	myApp.userAuditReport = function() {
		myApp.notification({
			"id" : "page_not_found",
			message : "Page Not Found",
			duration : 5000,
			status : "danger"
		});
	};
	myApp.accessLogReport = function() {
		myApp.notification({
			"id" : "page_not_found",
			message : "checking",
			duration : 5000,
			status : "warning"
		});
	};
	myApp.groupLevelReport = function() {
		myApp.notification({
			"id" : "page_not_found",
			message : "checking",
			duration : 5000,
			status : "success"
		});
	};
	myApp.ruleLevelReport = function() {
		myApp.notification({
			"id" : "page_not_found",
			message : "checking",
			duration : 5000,
			status : "info"
		});
	};

	myApp.formComponents = function() {
		myApp.formreset();
		$("#formComponent").formValidate();
		myApp.setPageHeight();
		myApp.datePicker("#futurDate .input-group-addon", {
			focus : false
		});
		myApp.datePicker("#dateselected .input-group-addon", {
			focus : false
		});
		myApp.datePicker("#pastDate .input-group-addon", {
			focus : false
		});
		myApp.datePicker("#rangeFrom .input-group-addon", {
			focus : false
		});
		myApp.datePicker("#rangeTo .input-group-addon", {
			focus : false
		});
		$(document).off("click", '#formsubmitted').on("click", '#formsubmitted', function() {
			if ($("#formComponent").formValidate("validate")) {
				alert("hi");

			} else {
				alert("hello");
			}
			return false;
		});

		$(document).off('click', '[data-toggle="deleteConfirmbox"]').on('click', '[data-toggle="deleteConfirmbox"]', function() {
			$("#confirmbox").confirmBox({
				message : ["Do you want to delete this Record"],
				yes : function() {
				}
			});
			$("#confirmbox").confirmBox("show");
		});
	};

	myApp.actionName = $("[data-page-action]").attr("data-page-action");
	myApp.autoPageLoad(function() {
		var loc_activelink = $("#navigation_left [data-link][data-keypage='" + $("#load_page").attr("data-keypage") + "']");
		loc_activelink.addClass("active");
	}).headerLogo().modalHandle().menuClick().scrollTop();
	myApp.mediaScreen().leftMenuPos().common().modalClick().collapsible();

	$(window).on("resize", function() {
		myApp.scrollMove(0);
		myApp.setPageHeight().mediaScreen().leftMenuPos();
		myApp.setMaxHeight();
		myApp.jqGridResize();
	});

	myApp.setMaxHeight();

});

