function AppJson() {
	this.recentloggedJSON = {
		"totalPage" : 1,
		"totalRows" : 4,
		"rowNum" : 4,
		"start" : 1,
		"end" : 10,
		"title" : "Recent Logged in Users",
		"column" : {
			"colNames" : ["User Name", "Role", "Login Time","Logout Time"],
			"colModel" : [{
				"name" :  "User Name",
				"index" : "User Name",
				"width" : "25%"
			}, {
				"name" :  "Role",
				"index" : "Role",
				"width" : "25%"
			}, {
				"name" :  "Login Time",
				"index" : "Login Time",
				"width" : "25%"
			},
			{
				"name" :  "Logout Time",
				"index" : "Logout Time",
				"width" : "25%"
			}]
			
		},
		gridId : "#jqgrid-recentlogged",
		gridPagerId : "#jqgrid-pager-recentlogged",
		"data" : [{
			"User Name"   : 'Sathish',
			"Role"        : "Admin",
			"Login Time"  : "2014-11-06 12:00",
			"Logout Time" : "2014-11-06 12:33"
		}, {
			"User Name"   : 'Tester',
			"Role"        : "Office Assistat",
			"Login Time"  : "2014-12-06 10:00",
			"Logout Time" : "2014-12-06 11:33"
		}, {
			"User Name"   : 'RuleIT',
			"Role"        : "Admin",
			"Login Time"  : "2014-10-06 09:00",
			"Logout Time" : "2014-10-06 09:33"
		}, {
			"User Name"   : 'Sass',
			"Role"        : "Office Assistat",
			"Login Time"  : "2014-09-06 04:00",
			"Logout Time" : "2014-09-06 05:33"
		}, {
			"User Name"   : 'Sathish',
			"Role"        : "Admin",
			"Login Time"  : "2014-06-06 06:00",
			"Logout Time" : "2014-06-06 07:33"
		},{
			"User Name"   : 'Sathish',
			"Role"        : "Office Assistat",
			"Login Time"  : "2014-05-06 07:00",
			"Logout Time" : "2014-05-06 08:33"
		}, {
			"User Name"   : 'Tester',
			"Role"        : "Admin",
			"Login Time"  : "2014-04-06 11:00",
			"Logout Time" : "2014-04-06 11:33"
		}, {
			"User Name"   : 'RuleIT',
			"Role"        : "Office Assistat",
			"Login Time"  : "2014-03-06 11:00",
			"Logout Time" : "2014-03-06 11:33"
		}, {
			"User Name"   : 'Sass',
			"Role"        : "Admin",
			"Login Time"  : "2014-02-06 11:00",
			"Logout Time" : "2014-02-06 11:33"
		}, {
			"User Name"   : 'Sathish',
			"Role"        : "Office Assistat",
			"Login Time"  : "2014-01-06 11:00",
			"Logout Time" : "2014-01-06 11:33"
		}]
	};
	
	this.recenttaskJSON = {
		"totalPage" : 1,
		"totalRows" : 4,
		"rowNum" : 4,
		"start" : 1,
		"end" : 10,
		"title" : "Recent Logged in Users",
		"column" : {
			"colNames" : ["User Name", "Task", "Date/Time"],
			"colModel" : [{
				"name" :  "User Name",
				"index" : "User Name",
				"width" : "30%"
			}, {
				"name" :  "Task",
				"index" : "Task",
				"width" : "30%"
			}, {
				"name" :  "Date/Time",
				"index" : "Date/Time",
				"width" : "30%"
			}
			]
			
		},
		gridId : "#jqgrid-recenttask",
		gridPagerId : "#jqgrid-pager-recenttask",
		"data" : [{
			"User Name"   : 'Sathish',
			"Task"        : "State Variable has been created",
			"Date/Time"  : "2014-11-06 11:00",			
		}, {
			"User Name"   : 'Tester',
			"Task"        : "Admin",
			"Date/Time"  : "2014-12-12 02:00",			
		}, {
			"User Name"   : 'RuleIT',
			"Task"        : "Application Constant has been created",
			"Date/Time"  : "2014-10-25 05:00",			
		}, {
			"User Name"   : 'sass',
			"Task"        : "Upgrading scripts used in template",
			"Date/Time"  : "2014-02-31 11:00",			
		}, {
			"User Name"   : 'Sathish',
			"Task"        : "State Variable has been created",
			"Date/Time"  : "2014-05-10 11:00",			
		}, {
			"User Name"   : 'Tester',
			"Task"        : "Admin",
			"Date/Time"  : "2014-06-09 11:00",			
		}, {
			"User Name"   : 'RuleIT',
			"Task"        : "Application Constant has been created",
			"Date/Time"  : "2014-12-06 11:00",			
		}, {
			"User Name"   : 'sass',
			"Task"        : "Upgrading scripts used in template",
			"Date/Time"  : "2014-12-06 11:00",			
		},{
			"User Name"   : 'Sathish',
			"Task"        : "State Variable has been created",
			"Date/Time"  : "2014-12-06 11:00",			
		}, {
			"User Name"   : 'Tester',
			"Task"        : "Admin",
			"Date/Time"  : "2014-02-08 11:00",			
		}, {
			"User Name"   : 'RuleIT',
			"Task"        : "Application Constant has been created",
			"Date/Time"  : "2014-08-12 11:00",			
		}, {
			"User Name"   : 'sass',
			"Task"        : "Upgrading scripts used in template",
			"Date/Time"  : "2014-12-06 11:00",			
		}]
	};
	
	
	
	this.recentuserJSON = {
		"totalPage" : 1,
		"totalRows" : 4,
		"rowNum" : 4,
		"start" : 1,
		"end" : 10,
		"title" : "Recent Logged in Users",
		"column" : {
			"colNames" : ["User Name", "Role Name", "Components", "Last Login Date/Time", "Action"],
			"colModel" : [{
				"name" :  "User Name",
				"index" : "User Name",
				"width" : "25%"
			}, {
				"name" :  "Role Name",
				"index" : "Role Name",
				"width" : "25%"
			},
			 {
				"name" :  "Components",
				"index" : "Components",
				"width" : "20%"
			},
			 {
				"name" :  "Date/Time",
				"index" : "Date/Time",
				"width" : "20%"
			},{
				"name" :  "Action",
				"index" :  "Action",
				"width" : "10%",
				"sortable" : false
			}
			]
			
		},
		gridId : "#jqgrid-usertask ",
		gridPagerId : "#jqgrid-pager-usertask",
		"data" : [{
			"User Name"   : 'Sathish',
			"Role Name"        : "Admin",
			"Components"        : "Configurator",
			"Date/Time"  : "2014-11-05 12:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-member.html" data-keypage="userEditModal" data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"  data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"User Name"   : 'Kalaivanan',
			"Role Name"        : "System Admin",
			"Components"        : "Compossor",
			"Date/Time"  : "2014-12-06 09:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-member.html" data-keypage="userEditModal" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"   data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"User Name"   : 'Manavalan',
			"Role Name"        : "HR Department",
			"Components"        : "Viewer",
			"Date/Time"  : "2014-10-20 06:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-member.html" data-keypage="userEditModal" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"   data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"User Name"   : 'Ponvananan',
			"Role Name"        : "Office Assistant",
			"Components"        : "Modular",
			"Date/Time"  : "2014-09-30 12:30",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-member.html" data-keypage="userEditModal" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"   data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"User Name"   : 'Antony',
			"Role Name"        : "UI Development",
			"Components"        : "Evalutor",
			"Date/Time"  : "2014-08-27 07:20",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-member.html" data-keypage="userEditModal" data-ui="modal"  ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"   data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"User Name"   : 'Vijayguru',
			"Role Name"        : "Head Tech Lead",
			"Components"        : "Configurator",
			"Date/Time"  : "2014-12-12 12:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-member.html" data-keypage="userEditModal" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"   data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"User Name"   : 'Purushothaman',
			"Role Name"        : "Team Leader",
			"Components"        : "Modular",
			"Date/Time"  : "2014-12-26 04:30",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-member.html" data-keypage="userEditModal" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"   data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"User Name"   : 'Harikumaran',
			"Role Name"        : "Assistant",
			"Components"        : "Evalutor",
			"Date/Time"  : "2014-09-24 09:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-member.html"   data-keypage="userEditModal" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"   data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},{
			"User Name"   : 'Gopinath',
			"Role Name"        : "Developer",
			"Components"        : "Configruator",
			"Date/Time"  : "2014-12-06 11:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-member.html" data-keypage="userEditModal" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"   data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"User Name"   : 'Tamilselvan',
			"Role Name"        : "Developer",
			"Components"        : "Viewer",
			"Date/Time"  : "2014-09-08 10:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-member.html"  data-keypage="userEditModal" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"   data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"User Name"   : 'Saravanan',
			"Role Name"        : "UI Development",
			"Components"        : "Compossor",
			"Date/Time"  : "2014-10-13 09:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-member.html" data-keypage="userEditModal" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"   data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"User Name"   : 'Prakash',
			"Role Name"        : "HR Department",
			"Components"        : "Modular",
			"Date/Time"  : "2014-12-06 11:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-member.html" data-keypage="userEditModal" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"   data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',		
		}]
	};
	
	
	
	this.recentroleJSON = {
		"totalPage" : 1,
		"totalRows" : 4,
		"rowNum" : 4,
		"start" : 1,
		"end" : 10,
		"title" : "Recent Logged in Users",
		"column" : {
			"colNames" : ["Role Name", "Role Description", "Access Rights", "Last Modified By", "Last Modified On", "Action"],
			"colModel" : [{
				"name" :  "Role Name",
				"index" : "Role Name",
				"width" : "16%"
			}, {
				"name" :  "Role Description",
				"index" : "Role Description",
				"width" : "17%"
			}, {
				"name" :  "Access Rights",
				"index" : "Access Rights",
				"width" : "17%"
			},{
				"name" :  "Last Modified By",
				"index" :  "Last Modified By",
				"width" : "20%"
			},{
				"name" :  "Last Modified On",
				"index" :  "Last Modified On",
				"width" : "20%"
			},{
				"name" :  "Action",
				"index" :  "Action",
				"width" : "10%",
				"sortable" : false
			}
			]
			
		},
		gridId : "#jqgrid-roletask ",
		gridPagerId : "#jqgrid-pager-roletask",
		"data" : [{
			"Role Name"   : 'Sathish',
			"Role Description"        : "Admin",
			"Access Rights"  : "2014-12-06 11:00",
			"Last Modified By"  : "2014-12-06 11:00",
			"Last Modified On"  : "2014-12-06 11:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-role.html" data-keypage="userEditModal"  data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"Role Name"   : 'Kalaivanan',
			"Role Description"        : "System Admin",
			"Access Rights"  : "2014-11-09 12:00",
			"Last Modified By"  : "2014-11-09 12:00",
			"Last Modified On"  : "2014-11-09 12:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-role.html" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"Role Name"   : 'Manavalan',
			"Role Description"        : "Customer Support",
			"Access Rights"  : "2014-09-22 07:40",
			"Last Modified By"  : "2014-09-22 07:40",
			"Last Modified On"  : "2014-09-22 07:40",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-role.html" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"Role Name"   : 'Ponvananan',
			"Role Description"        : "Developers",
			"Access Rights"  : "2014-02-12 05:30",
			"Last Modified By"  : "2014-02-12 05:30",
			"Last Modified On"  : "2014-02-12 05:30",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-role.html" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"Role Name"   : 'Antony',
			"Role Description"        : "UI Team",
			"Access Rights"  : "2014-03-20 09:00",
			"Last Modified By"  : "2014-03-20 09:00",
			"Last Modified On"  : "2014-03-20 09:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-role.html" data-ui="modal"  ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"Role Name"   : 'Vijayguru',
			"Role Description"        : "Developers",
			"Access Rights"  : "2014-12-06 11:00",
			"Last Modified By"  : "2014-12-06 11:00",
			"Last Modified On"  : "2014-12-06 11:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-role.html" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"Role Name"   : 'Purushothaman',
			"Role Description"        : "Finance Department",
			"Access Rights"  : "2014-11-31 03:00",
			"Last Modified By"  : "2014-11-31 03:00",
			"Last Modified On"  : "2014-11-31 03:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-role.html" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"Role Name"   : 'Harikumaran',
			"Role Description"        : "Developers",
			"Access Rights"  : "2014-12-06 11:00",
			"Last Modified By"  : "2014-12-06 11:00",
			"Last Modified On"  : "2014-12-06 11:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-role.html" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},{
			"Role Name"   : 'Gopinath',
			"Role Description"        : "System Admin",
			"Access Rights"  : "2014-12-06 11:00",
			"Last Modified By"  : "2014-12-06 11:00",
			"Last Modified On"  : "2014-12-06 11:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-role.html" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"Role Name"   : 'Tamilselvan',
			"Role Description"        : "Admin",
			"Access Rights"  : "2014-11-31 05:00",
			"Last Modified By"  : "2014-11-31 05:00",
			"Last Modified On"  : "2014-11-31 05:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-role.html" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"Role Name"   : 'Saravanan',
			"Role Description"        : "Admin",
			"Access Rights"  : "2014-12-06 11:00",
			"Last Modified By"  : "2014-12-06 11:00",
			"Last Modified On"  : "2014-12-06 11:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle  margin-right5px margintopbot" data-modal-url="edit-role.html" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"Role Name"   : 'Prakash',
			"Role Description"        : "Admin",
			"Access Rights"  : "2014-10-31 08:00",
			"Last Modified By"  : "2014-10-31 08:00",
			"Last Modified On"  : "2014-10-31 08:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-role.html" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',		
		}]
	};
	
	
	this.recentengineJSON = {
		"totalPage" : 1,
		"totalRows" : 5,
		"rowNum" : 5,
		"start" : 1,
		"end" : 10,
		"title" : "Engine",
		"column" : {
			"colNames" : ["Engine Name", "Description", "Last Modified By", "Last Modified Date", "Action"],
			"colModel" : [{
				"name" :  "Engine Name",
				"index" : "Engine Name",
				"width" : "16%"
			}, {
				"name" :  "Description",
				"index" : "Description",
				"width" : "17%"
			}, {
				"name" :  "Last Modified By",
				"index" : "Last Modified By",
				"width" : "17%"
			},{
				"name" :  "Last Modified Date",
				"index" :  "Last Modified Date",
				"width" : "20%"
			},{
				"name" :  "Action",
				"index" :  "Action",
				"width" : "10%",
				"sortable" : false
			}
			]
			
		},
		gridId : "#jqgrid-enginetask",
		gridPagerId : "#jqgrid-pager-enginetask",
		"data" : [{
			"Engine Name"   : 'Claims',
			"Description"        : "Claims Engine",
			"Last Modified By"  : "Aum",
			"Last Modified Date"  : "2014-12-06 09:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-engine.html" data-ui="modal" data-keypage="editEngine"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox" ><i class="fa fa-trash-o"></i></button>',			
		}, {
			"Engine Name"   : 'SSBP',
			"Description"        : "Social Services",
			"Last Modified By"  : "Aum",
			"Last Modified Date"  : "2014-02-26 11:00",	
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-engine.html" data-ui="modal"  data-keypage="editEngine"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"Engine Name"   : 'Claims1',
			"Description"        : "Claims",
			"Last Modified By"  : "RuleIT",
			"Last Modified Date"  : "2014-11-28 10:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-engine.html" data-ui="modal"  data-keypage="editEngine"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"Engine Name"   : 'SSP',
			"Description"        : "SSP Engine",
			"Last Modified By"  : "Antony",
			"Last Modified Date"  : "2014-02-15 08:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-engine.html" data-ui="modal"  data-keypage="editEngine"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Engine Name"   : 'Claims',
			"Description"        : "Claims Engine",
			"Last Modified By"  : "Aum",
			"Last Modified Date"  : "2014-12-06 09:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-engine.html" data-ui="modal"  data-keypage="editEngine"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"Engine Name"   : 'SSBP',
			"Description"        : "Social Services",
			"Last Modified By"  : "Aum",
			"Last Modified Date"  : "2014-02-26 11:00",	
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-engine.html" data-ui="modal"  data-keypage="editEngine"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"Engine Name"   : 'Claims1',
			"Description"        : "Claims",
			"Last Modified By"  : "RuleIT",
			"Last Modified Date"  : "2014-11-28 10:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-engine.html" data-ui="modal"  data-keypage="editEngine"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}, {
			"Engine Name"   : 'SSP',
			"Description"        : "SSP Engine",
			"Last Modified By"  : "Antony",
			"Last Modified Date"  : "2014-02-15 08:00",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-engine.html" data-ui="modal"  data-keypage="editEngine"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"  data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}]
	};
	
	this.recentconstantJSON = {
		"totalPage" : 1,
		"totalRows" : 5,
		"rowNum" : 5,
		"start" : 1,
		"end" : 10,
		"title" : "Application Constant",
		"column" : {
			"colNames" : ["Application Constant Name", "Application Constant Value", "Description", "Last Modified by", "Last Modified Date", "Action"],
			"colModel" : [{
				"name" :  "Application Constant Name",
				"index" : "Application Constant Name",
				"width" : "20%"
			}, {
				"name" :  "Application Constant Value",
				"index" : "Application Constant Value",
				"width" : "20%"
			}, {
				"name" :  "Description",
				"index" : "Description",
				"width" : "20%"
			},{
				"name" :  "Last Modified by",
				"index" :  "Last Modified by",
				"width" : "15%"
			},
			{
				"name" :  "Last Modified Date",
				"index" :  "Last Modified Date",
				"width" : "15%"
			},{
				"name" :  "Action",
				"index" :  "Action",
				"width" : "10%",
				"sortable" : false
			}
			]
			
		},
	 
		gridId : "#jqgrid-constanttask",
		gridPagerId : "#jqgrid-pager-constanttask",
		"data" : [{
			"Application Constant Name" : 'Taxonomy for Type=28',
			"Application Constant Value" : "152W00000x",
			"Description"	: "Taxonomy",
			"Last Modified by"  : "Aum Prasad",	
			"Last Modified Date"  : "2014-02-28 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-application-constant.html" data-ui="modal" data-keypage="constantEdit"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"  data-toggle="deleteConfirmbox" data-keypage="constantDelete"  data-button-for="constantDelete" data-link-button="application-constant-delete.html"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Application Constant Name" : 'Taxonomy for Type=29',
			"Application Constant Value" : "153W00000x",
			"Description"	: "xxxxx",
			"Last Modified by"  : "Naidu Purushothaman",	
			"Last Modified Date"  : "2014-03-20 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-application-constant.html" data-ui="modal"  data-keypage="constantEdit"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"   data-toggle="deleteConfirmbox" data-keypage="constantRemove"  data-modal-for="constantDelete"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Application Constant Name" : 'Taxonomy for Type=29',
			"Application Constant Value" : "154W00000x",
			"Description"	: "yyyyy",
			"Last Modified by"  : "Antony",	
			"Last Modified Date"  : "2014-04-10 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-application-constant.html" data-ui="modal"  data-keypage="constantEdit"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"   data-toggle="deleteConfirmbox" data-keypage="constantRemove" data-modal-for="constantDelete"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Application Constant Name" : 'Taxonomy for Type=28',
			"Application Constant Value" : "155W00000x",
			"Description"	: "wwwww",
			"Last Modified by"  : "Aum Prasad",	
			"Last Modified Date"  : "2014-02-28 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-application-constant.html" data-ui="modal"  data-keypage="constantEdit"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"   data-toggle="deleteConfirmbox" data-keypage="constantRemove" data-modal-for="constantDelete"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Application Constant Name" : 'Taxonomy for Type=31',
			"Application Constant Value" : "156W00000x",
			"Description"	: "eeeee",
			"Last Modified by"  : "Naidu Purushothaman",	
			"Last Modified Date"  : "2014-03-20 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-application-constant.html" data-ui="modal"  data-keypage="constantEdit"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"   data-toggle="deleteConfirmbox" data-keypage="constantRemove" data-modal-for="constantDelete" data-modal-for="constantDelete" data-modal-for="constantDelete"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Application Constant Name" : 'Taxonomy for Type=32',
			"Application Constant Value" : "157W00000x",
			"Description"	: "ggggg",
			"Last Modified by"  : "Antony",	
			"Last Modified Date"  : "2014-04-10 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-application-constant.html" data-ui="modal"  data-keypage="constantEdit"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"><i class="fa fa-trash-o"></i></button>',			
		}
		]
	};
	
	
	this.recentconstantupdateJSON = {
		"totalPage" : 1,
		"totalRows" : 4,
		"rowNum" : 4,
		"start" : 1,
		"end" : 10,
		"title" : "Edit Application Constant",
		"column" : {
			"colNames" : ["Rule Name", "Description", "Checked Out by", "Checked Out Status"],
			"colModel" : [{
				"name" :  "Rule Name",
				"index" : "Rule Name",
				"width" : "25%"
			},{
				"name" :  "Description",
				"index" : "Description",
				"width" : "25%"
			},{
				"name" :  "Checked Out by",
				"index" :  "Checked Out by",
				"width" : "55%"
			},
			{
				"name" :  "Checked Out Status",
				"index" :  "Checked Out Status",
				"width" : "25%"
			}, 
			]
			
		},
	 
		gridId : "#jqgrid-constantupdatetask",
		gridPagerId : "#jqgrid-pager-constantupdatetask",
		"data" : [{
			"Rule Name" : 'Nursing Facility Co-Insurance Days Pricing',
			"Description"	: "Nursing Facility Co-Insurance Days Pricing",
			"Checked Out by"  : "&nbsp;",	
			"Checked Out Status"  : "No",
		}, 
		{
			"Rule Name" : '1278-Medicare Logic',
			"Description"	: "1278-Medicare Logic",
			"Checked Out by"  : "ruleIT",	
			"Checked Out Status"  : "Yes",
		},
		{
			"Rule Name" : 'Load Occurrence Span Info',
			"Description"	: "Load Occurrence Span Info",
			"Checked Out by"  : "&nbsp;",	
			"Checked Out Status"  : "No",
		}, 
		{
			"Rule Name" : 'MDCH Cost Of Care Header Pricing',
			"Description"	: "MDCH Cost Of Care Header Pricing",
			"Checked Out by"  : "ruleIT",	
			"Checked Out Status"  : "Yes",
		}, 
		]
	};
	
	
	this.recentremoveconstantJSON = {
		"totalPage" : 1,
		"totalRows" : 2,
		"rowNum" : 2,
		"start" : 1,
		"end" : 10,
		"title" : "Application Constant Remove",
		"column" : {
			"colNames" : ["Rule Name", "Description"],
			"colModel" : [{
				"name" :  "Rule Name",
				"index" : "Rule Name",
				"width" : "25%"
			},{
				"name" :  "Description",
				"index" : "Description",
				"width" : "25%"
			}  
			]
			
		},
	 
		gridId : "#jqgrid-constantremovetask",
		gridPagerId : "#jqgrid-pager-constantremovetask",
		"data" : [{
			"Rule Name" : 'Nursing Facility Co-Insurance Days Pricing',
			"Description"	: "Nursing Facility Co-Insurance Days Pricing" 
		}, 
		{
			"Rule Name" : '1278-Medicare Logic',
			"Description"	: "1278-Medicare Logic" 
		},
		{
			"Rule Name" : 'Load Occurrence Span Info',
			"Description"	: "Load Occurrence Span Info" 
		}, 
		{
			"Rule Name" : 'MDCH Cost Of Care Header Pricing',
			"Description"	: "MDCH Cost Of Care Header Pricing" 
		}, 
		]
	};
	
	
	
	this.recentpicklistJSON = {
		"totalPage" : 1,
		"totalRows" : 6,
		"rowNum" : 6,
		"start" : 1,
		"end" : 10,
		"title" : "Pick List",
		"column" : {
			"colNames" : ["Pick List Name", "Pick List Query", "Description", "Last Modified by", "Last Modified Date", "Action"],
			"colModel" : [{
				"name" :  "Pick List Name",
				"index" : "Pick List Name",
				"width" : "20%"
			}, {
				"name" :  "Pick List Query",
				"index" : "Pick List Query",
				"width" : "20%"
			}, {
				"name" :  "Description",
				"index" : "Description",
				"width" : "20%"
			},{
				"name" :  "Last Modified by",
				"index" :  "Last Modified by",
				"width" : "15%"
			},
			{
				"name" :  "Last Modified Date",
				"index" :  "Last Modified Date",
				"width" : "15%"
			},{
				"name" :  "Action",
				"index" :  "Action",
				"width" : "10%",
				"sortable" : false
			}
			]
			
		},
	 
		gridId : "#jqgrid-picklisttask",
		gridPagerId : "#jqgrid-pager-picklisttask",
		"data" : [{
			"Pick List Name" : 'Member State Variables',
			"Pick List Query" : "Select Name from Application_Field 'Member' ",
			"Description"	: "All the state variables of the member details",
			"Last Modified by"  : "Admin",	
			"Last Modified Date"  : "2014-02-28 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-picklist.html" data-ui="modal" ><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Pick List Name" : 'Limit Data Services',
			"Pick List Query" : "Select Name from service where class_ID in  ",
			"Description"	: "All the state variables of the member details",
			"Last Modified by"  : "Office Assistant",	
			"Last Modified Date"  : "2014-05-30 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-picklist.html" data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Pick List Name" : 'Member State Variables',
			"Pick List Query" : "Select Name from Application_Field 'Member' ",
			"Description"	: "All the state variables of the member details",
			"Last Modified by"  : "Developer",	
			"Last Modified Date"  : "2014-02-28 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-picklist.html" data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Pick List Name" : 'Limit Data Services',
			"Pick List Query" : "Select Name from service where class_ID in   ",
			"Description"	: "User Details",
			"Last Modified by"  : "System Admin",	
			"Last Modified Date"  : "2014-05-30 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-picklist.html" data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Pick List Name" : 'Member State Variables',
			"Pick List Query" : "Select Name from Application_Field 'Member' ",
			"Description"	: "User details",
			"Last Modified by"  : "Developer",	
			"Last Modified Date"  : "2014-02-28 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-picklist.html" data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Pick List Name" : 'Limit Data Services',
			"Pick List Query" : "Select Name from service where class_ID in  ",
			"Description"	: "All the state variables of the member details",
			"Last Modified by"  : "Finance",	
			"Last Modified Date"  : "2014-05-30 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-picklist.html" data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Pick List Name" : 'Member State Variables',
			"Pick List Query" : "Select Name from Application_Field 'Member' ",
			"Description"	: "All the state variables of the user details",
			"Last Modified by"  : "System Admin",	
			"Last Modified Date"  : "2014-04-20 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-picklist.html" data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Pick List Name" : 'Limit Data Services',
			"Pick List Query" : "Select Name from service where class_ID in  ",
			"Description"	: "All the state variables",
			"Last Modified by"  : "Office Assistant",	
			"Last Modified Date"  : "2014-02-22 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-picklist.html" data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"  data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}
		]
	};
	
	
	this.recentruletagJSON = {
		"totalPage" : 1,
		"totalRows" : 5,
		"rowNum" : 5,
		"start" : 1,
		"end" : 10,
		"title" : "Rule Tag",
		"column" : {
			"colNames" : ["Tag Name", "Description", "Last Modified by", "Last Modified Date", "Action"],
			"colModel" : [{
				"name" :  "Tag Name",
				"index" : "Tag Name",
				"width" : "20%"
			},{
				"name" :  "Description",
				"index" : "Description",
				"width" : "20%"
			},{
				"name" :  "Last Modified by",
				"index" :  "Last Modified by",
				"width" : "15%"
			},
			{
				"name" :  "Last Modified Date",
				"index" :  "Last Modified Date",
				"width" : "15%"
			},{
				"name" :  "Action",
				"index" :  "Action",
				"width" : "10%",
				"sortable" : false
			}
			]
			
		},
	 
		gridId : "#jqgrid-ruletagtask",
		gridPagerId : "#jqgrid-pager-ruletagtask",
		"data" : [{
			"Tag Name" : 'Claim', 
			"Description"	: "Claim Description",
			"Last Modified by"  : "RuleIT",	
			"Last Modified Date"  : "2014-02-28 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-ruletag.html" data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Tag Name" : 'Process Claim', 
			"Description"	: "Process Claim",
			"Last Modified by"  : "Admin",	
			"Last Modified Date"  : "2014-04-10 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-ruletag.html" data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Tag Name" : 'Provider', 
			"Description"	: "Provider",
			"Last Modified by"  : "RuleIT",	
			"Last Modified Date"  : "2014-03-19 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-ruletag.html" data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"  data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Tag Name" : 'Claim', 
			"Description"	: "Claim Description",
			"Last Modified by"  : "RuleIT",	
			"Last Modified Date"  : "2014-02-28 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-ruletag.html" data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Tag Name" : 'Process Claim', 
			"Description"	: "Process Claim",
			"Last Modified by"  : "Admin",	
			"Last Modified Date"  : "2014-04-10 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-ruletag.html" data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Tag Name" : 'Provider', 
			"Description"	: "Provider",
			"Last Modified by"  : "RuleIT",	
			"Last Modified Date"  : "2014-03-19 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-ruletag.html" data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Tag Name" : 'Claim', 
			"Description"	: "Claim Description",
			"Last Modified by"  : "RuleIT",	
			"Last Modified Date"  : "2014-02-28 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-ruletag.html" data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Tag Name" : 'Process Claim', 
			"Description"	: "Process Claim",
			"Last Modified by"  : "Admin",	
			"Last Modified Date"  : "2014-04-10 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-ruletag.html" data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Tag Name" : 'Provider', 
			"Description"	: "Provider",
			"Last Modified by"  : "RuleIT",	
			"Last Modified Date"  : "2014-03-19 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-ruletag.html" data-ui="modal"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot" data-toggle="deleteConfirmbox"><i class="fa fa-trash-o"></i></button>',			
		}
		]
	};
	
	
	this.recentinstantserviceJSON = {
		"multiselect":true,
		"totalPage" : 1,
		"totalRows" : 5,
		"rowNum" : 5,
		"start" : 1,
		"end" : 10,
		"title" : "Instant Services Registration",		 
		"column" : {
			"colNames" : [  "Jar", "Version", "Updated Date"],
			"colModel" : [ {
				"name" :  "Jar",
				"index" : "Jar",
				"width" : "30%"
			},{
				"name" :  "Version",
				"index" :  "Version",
				"width" : "30%"
			},
			{
				"name" :  "Updated Date",
				"index" :  "Updated Date",
				"width" : "35%",
				 
			}
			]
			
		},
	 
		gridId : "#jqgrid-instanttask",
		//gridPagerId : "#jqgrid-pager-instanttask",
		"data" : [{
			"Box" : 'Box', 
			"Jar"	: "Jar",
			"Version"  : "7.0.0",	
			"Updated Date"  : "2014-02-11 11:00",			 
			 			
		},
		{
			"Box" : 'Box', 
			"Jar"	: "Cnsi Jar",
			"Version"  : "6.0.0",	
			"Updated Date"  : "2014-05-20 11:00",			 
			 			
		},
		{
			"Box" : 'Box', 
			"Jar"	: "ISR Jar",
			"Version"  : "6.6.0",	
			"Updated Date"  : "2014-07-28 11:00",			 
			 			
		},
		{
			"Box" : 'Box', 
			"Jar"	: "User Audit Jar",
			"Version"  : "2.1.0",	
			"Updated Date"  : "2014-10-18 11:00",		 
		}
		]
	};
	
	
	this.recentinstantserviceRegisterJSON = {
		"multiselect":true,
		"totalPage" : 1,
		"totalRows" : 5,
		"rowNum" : 5,
		"start" : 1,
		"end" : 10,
		"title" : "Instant Services Error Registration",		 
		"column" : {
			"colNames" : [  "Jar", "Component", "Class", "No.of.Arguments", "Argument Type", "Business Name", "Status", "Action"],
			"colModel" : [ {
				"name" :  "Jar",
				"index" : "Jar",
				"width" : "5%"
			},{
				"name" :  "Component",
				"index" :  "Component",
				"width" : "15%"
			},
			{
				"name" :  "Class",
				"index" :  "Class",
				"width" : "10%",
			},
			{
				"name" :  "No.of.Arguments",
				"index" :  "No.of.Arguments",
				"width" : "15%",
			},
			{
				"name" :  "Argument Type",
				"index" :  "Argument Type",
				"width" : "15%",
			},
			{
				"name" :  "Business Name",
				"index" :  "Business Name",
				"width" : "15%",
			},
			{
				"name" :  "Status",
				"index" :  "Status",
				"width" : "15%",
			},
			{
				"name" :  "Action",
				"index" :  "Action",
				"width" : "10%",
				"sortable" : false
			},
			]
			
		},
	 
		gridId : "#jqgrid-instantregistertask",		 
		"data" : [{			  
			"Jar"	: "ISR",
			"Component"  : "Com.test.one",	
			"Class"  : "Get Service",
			"No.of.Arguments"  : "2",
			"Argument Type"  : "int.String",
			"Business Name"  : "Bypass the Limit",
			"Status"  : "Registered",
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-for="instantService" data-modal-url="add-instant-service-register.html" data-ui="modal" title="Register"><i class="fa fa-pencil-square-o"></i></button>',
		},
		{			  
			"Jar"	: "ISR",
			"Component"  : "Com.test.Two",	
			"Class"  : "Impl Abstract",
			"No.of.Arguments"  : "3",
			"Argument Type"  : "String.int.String",
			"Business Name"  : "Load next Claim Line",
			"Status"  : "Registered",
			"Action"  : '<button class="btn cust-btn btn-danger btn-circle margintopbot" data-keypage="instantServiceUNRegister" data-ui="modal" data-modal-url="add-instant-service-un-register.html" title="Un Register"><i class="fa fa-file-text-o"></i></button>',
		},
		{			  
			"Jar"	: "ISRR",
			"Component"  : "Com.test.Three",	
			"Class"  : "Impl Abstract-one",
			"No.of.Arguments"  : "1",
			"Argument Type"  : "String",
			"Business Name"  : "&nbsp;",
			"Status"  : "Not Registered",
			"Action"  : '  <button class="btn cust-btn btn-info btn-circle margintopbot" data-keypage="instantServiceModify" data-ui="modal" data-modal-url="add-instant-service-modify.html" title="Modify"><i class="fa fa-sliders"></i></button>',
		}
		]
	};
	
	this.recentinstantServiceUNRegisterJSON = {	 
		"totalPage" : 1,
		"totalRows" : 2,
		"rowNum" : 2,
		"start" : 1,
		"end" : 4,
		"title" : "Instant Services UN Registration",		 
		"column" : {
			"colNames" : [  "Rule Name", "Description"],
			"colModel" : [ {
				"name" :  "Rule Name",
				"index" : "Rule Name",				 
			},{
				"name" :  "Description",
				"index" :  "Description",
			 
			}
			]
			
		},
	 
		gridId : "#jqgrid-instantunregistertask",		 
		"data" : [{			  
			"Rule Name"	: "Total Cost Things",
			"Description"  : "Total Claim",	
		},
		{			  
			"Rule Name"	: "Claim Pre Process Things",
			"Description"  : "Process",	
		},
		{			  
			"Rule Name"	: "90354-Edit",
			"Description"  : "Edit",	
		},
		{			  
			"Rule Name"	: "SSBP Determine Claim Category",
			"Description"  : "Claim Category",	
		},
		
		]
	};
	
	this.recentinstantServiceModifyJSON = {	 
		"totalPage" : 1,
		"totalRows" : 2,
		"rowNum" : 2,
		"start" : 1,
		"end" : 4,
		"title" : "Instant Services Modify Screen",		 
		"column" : {
			"colNames" : [  "Rule Name", "Description"],
			"colModel" : [ {
				"name" :  "Rule Name",
				"index" : "Rule Name",				 
			},{
				"name" :  "Description",
				"index" :  "Description",
			 
			}
			]
			
		},
	 
		gridId : "#jqgrid-instantmodifytask",		 
		"data" : [{			  
			"Rule Name"	: "Antony Cost Things",
			"Description"  : "Total Claim",	
		},
		{			  
			"Rule Name"	: "Claim Pre Process Things",
			"Description"  : "Process",	
		},
		{			  
			"Rule Name"	: "90354-Edit",
			"Description"  : "Edit",	
		},
		{			  
			"Rule Name"	: "SSBP Determine Claim Category",
			"Description"  : "Claim Category",	
		},
		
		]
	};
	
	
	this.recentstatevariableJSON = {
		"totalPage" : 1,
		"totalRows" : 6,
		"rowNum" : 6,
		"start" : 1,
		"end" : 10,
		"title" : "Application Constant",
		"column" : {
			"colNames" : ["Application Constant Name", "Application Constant Value", "Description", "Last Modified by", "Last Modified Date", "Action"],
			"colModel" : [{
				"name" :  "Application Constant Name",
				"index" : "Application Constant Name",
				"width" : "20%"
			}, {
				"name" :  "Application Constant Type",
				"index" : "Application Constant Type",
				"width" : "20%"
			}, {
				"name" :  "Description",
				"index" : "Description",
				"width" : "20%"
			},{
				"name" :  "Last Modified by",
				"index" :  "Last Modified by",
				"width" : "15%"
			},
			{
				"name" :  "Last Modified Date",
				"index" :  "Last Modified Date",
				"width" : "15%"
			},{
				"name" :  "Action",
				"index" :  "Action",
				"width" : "10%",
				"sortable" : false
			}
			]
			
		},
	 
		gridId : "#jqgrid-statevariabletask",
		gridPagerId : "#jqgrid-pager-statevariabletask",
		"data" : [{
			"Application Constant Name" : 'SSBP Determine Claim Category',
			"Application Constant Type" : "Normal",
			"Description"	: "SSBP Determine Claim Category",
			"Last Modified by"  : "Aum Prasad",	
			"Last Modified Date"  : "2014-02-28 11:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-statevariable.html" data-ui="modal" data-keypage="statevariableEdit"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"  data-toggle="deleteConfirmbox" data-keypage="statevariableRemove"   data-modal-for="stateVariableDelete"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Application Constant Name" : 'Claim Adjustment Reason Code',
			"Application Constant Type" : "Dynamic",
			"Description"	: "Claim Adjustment Reason Code",
			"Last Modified by"  : "Naidu",	
			"Last Modified Date"  : "2014-06-20 10:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-statevariable.html" data-ui="modal" data-keypage="statevariableEdit"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"  data-toggle="deleteConfirmbox" data-keypage="statevariableRemove"   data-modal-for="stateVariableDelete"><i class="fa fa-trash-o"></i></button>',			
		},
		{
			"Application Constant Name" : 'RuleIT Reason Code',
			"Application Constant Type" : "Excel Type",
			"Description"	: "RuleIT Reason Code",
			"Last Modified by"  : "Hari",	
			"Last Modified Date"  : "2014-12-28 10:00",			 
			"Action"  : '<button class="btn cust-btn btn-warning btn-circle margin-right5px margintopbot" data-modal-url="edit-statevariable.html" data-ui="modal" data-keypage="statevariableEdit"><i class="fa fa-pencil"></i></button> <button class="btn cust-btn btn-danger btn-circle margintopbot"  data-toggle="deleteConfirmbox" data-keypage="statevariableRemove"   data-modal-for="stateVariableDelete"><i class="fa fa-trash-o"></i></button>',			
		} 
		]
	};
	
	
	this.recentvalidatorupdateJSON = {
		"totalPage" : 1,
		"totalRows" : 7,
		"rowNum" : 7,
		"start" : 1,
		"end" : 10,
		"title" : "Validation Summary",
		"column" : {
			"colNames" : ["Rule Name", "Rule Type", "Engine","Activation Date", "PhaseOut Date", "Error Count", "Status"],
			"colModel" : [{
				"name" :  "Rule Name",
				"index" : "Rule Name",
				"width" : "20%"
			},
			{
				"name" :  "Rule Type",
				"index" : "Rule Type",
				"width" : "15%"
			},
			{
				"name" :  "Engine",
				"index" : "Engine",
				"width" : "10%"
			},
			{
				"name" :  "Activation Date",
				"index" : "Activation Date",
				"width" : "15%"
			},
			{
				"name" :  "PhaseOut Date",
				"index" : "PhaseOut Date",
				"width" : "15%"
			},
			{
				"name" :  "Error Count",
				"index" : "Error Count",
				"width" : "15%"
			},
			{
				"name" :  "Status",
				"index" : "Status",
				"width" : "10%"
			}]
			
		},
		gridId : "#jqgrid-validatortask",
		gridPagerId : "#jqgrid-pager-validatortask",
		"data" : [{
			"Rule Name"   : 'Claim Process',
			"Rule Type"   : 'Normal',
			"Engine"   : 'SSBP',
			"Activation Date"   : '2014-12-28 10:00',
			"PhaseOut Date"   : '2014-05-20 10:00',
			"Error Count"   : '<a href="javascript:void(0)" class="validatererror"  data-modal-url="validatior-error.html" data-ui="modal" data-keypage="validatorError" data-modal-class="modal-dialog-big">5</a>',
			"Status"   : 'Invalid'
		},
		{
			"Rule Name"   : 'SSBP Determine',
			"Rule Type"   : 'Normal',
			"Engine"   : 'SSBP',
			"Activation Date"   : '2014-03-05 10:00',
			"PhaseOut Date"   : '&nbsp;',
			"Error Count"   : '<a href="javascript:void(0)" class="validatererror"  data-modal-url="validatior-error.html" data-ui="modal" data-keypage="validatorError" data-modal-class="modal-dialog-big">10</a>',
			"Status"   : 'Invalid'
		},
		{
			"Rule Name"   : 'Day Limit Recoup Logic',
			"Rule Type"   : 'Time Sensitive',
			"Engine"   : 'Claims',
			"Activation Date"   : '2014-11-30 10:00',
			"PhaseOut Date"   : '2014-12-25 10:00',
			"Error Count"   : '<a href="javascript:void(0)" class="validatererror"  data-modal-url="validatior-error.html" data-ui="modal" data-keypage="validatorError" data-modal-class="modal-dialog-big">7</a>',
			"Status"   : 'Valid'
		},
		{
			"Rule Name"   : '00005-Missing DOS',
			"Rule Type"   : 'Time Sensitive',
			"Engine"   : 'SSBP',
			"Activation Date"   : '2014-11-30 10:00',
			"PhaseOut Date"   : '2014-12-25 10:00',
			"Error Count"   : '<a href="javascript:void(0)" class="validatererror"  data-modal-url="validatior-error.html" data-ui="modal" data-keypage="validatorError" data-modal-class="modal-dialog-big">1</a>',
			"Status"   : 'Corrupted'
		},
		{
			"Rule Name"   : 'Day Limit Recoup Logic',
			"Rule Type"   : 'Time Sensitive',
			"Engine"   : 'Claims',
			"Activation Date"   : '2014-11-30 10:00',
			"PhaseOut Date"   : '2014-12-25 10:00',
			"Error Count"   : '<a href="javascript:void(0)" class="validatererror"  data-modal-url="validatior-error.html" data-ui="modal" data-keypage="validatorError" data-modal-class="modal-dialog-big">2</a>',
			"Status"   : 'Valid'
		}]
	};
	
	
	this.recentvalidatorerrorJSON = {	 
		"totalPage" : 1,
		"totalRows" : 4,
		"rowNum" : 4,
		"start" : 1,
		"end" : 4,
		"title" : "Validity Error Screen",		 
		"column" : {
			"colNames" : [  "S-No", "Deifination Compiled", "Error Details", "Suggestion"],
			"colModel" : [ {
				"name" :  "S-No",
				"index" : "S-No",
				"width" : "8%"
				 			 
			},{
				"name" :  "Deifination Compiled",
				"index" :  "Deifination Compiled",
				"width" : "20%"
				 
			 
			},
			{
				"name" :  "Error Details",
				"index" :  "Error Details",
			 "width" : "20%" 
			},
			{
				"name" :  "Suggestion",
				"index" :  "Suggestion",
				"width" : "50%"
			}
			]
			
		},
	 
		gridId : "#jqgrid-validatorerrortask",
		gridPagerId : "#jqgrid-pager-validatorerrortask",
		"data" : [{			  
			"S-No"	: "1",
			"Deifination Compiled"  : "Compiled",	
			"Error Details"  : "Invalid Syntax at Line #2",
			"Suggestion"  : "Please check any spaces between service name and arguments"
		},
		{			  
			"S-No"	: "2",
			"Deifination Compiled"  : "Compiled",	
			"Error Details"  : "Invalid Syntax at Line #2",
			"Suggestion"  : "Please check any spaces between service name and arguments"
		},
		{			  
			"S-No"	: "3",
			"Deifination Compiled"  : "Compiled",	
			"Error Details"  : "Invalid Syntax at Line #2",
			"Suggestion"  : "Please check any spaces between service name and arguments"
		},
		{			  
			"S-No"	: "4",
			"Deifination Compiled"  : "Compiled",	
			"Error Details"  : "Invalid Syntax at Line #2",
			"Suggestion"  : "Please check any spaces between service name and arguments"
		}]
	};
	
	
	
	this.recentcomparatorJSON = {
		"multiselect":true,
		"rowNum": true, 
		"totalPage" : 1,
		"totalRows" : 5,
		"rowNum" : 5,
		"start" : 1,
		"end" : 10,
		"title" : "Comparator List",
		"column" : {
			"colNames" : [ "Compared Objects", "Obeject Type", "Status", "Action"],
			"colModel" : [
			{
				"name" :  "Compared Objects",
				"index" : "Compared Objects",
				"width" : "35%"
			},
			{
				"name" :  "Obeject Type",
				"index" : "Obeject Type",
				"width" : "20%"
			},
			{
				"name" :  "Status",
				"index" : "Status",
				"width" : "20%"
			},
			{
				"name" :  "Action",
				"index" :  "Action",
				"width" : "20%",
				"sortable" : false
			}
			]
			
		},
	 
		gridId : "#jqgrid-comparatortask",
		gridPagerId : "#jqgrid-pager-comparatortask",
		"data" : [{
			"Compared Objects" : "Read Content ",
			"Obeject Type"	: "Application Constant",
			"Status"  : "Source and Destination are not identicial",			 		 
			"Action"  : '<button class="btn btn-clear" data-modal-url="comparator-diffmerge.html" data-ui="modal"  data-modal-class="modal-dialog-big" > Show Details</button>',
		},
		{
			"Compared Objects" : "Convert to string ",
			"Obeject Type"	: "Group",
			"Status"  : "xxxxxxxxxxxxxxxxxxxx",			 		 
			"Action"  : '<button class="btn btn-clear" data-modal-url="comparator-diffmerge.html" data-ui="modal"  data-modal-class="modal-dialog-big" > Show Details</button>',
		},
		{
			"Compared Objects" : "RSA Decrypt",
			"Obeject Type"	: "Application Field",
			"Status"  : "yyyyyyyyyyyyyyyyyyyyy",			 		 
			"Action"  : '<button class="btn btn-clear" data-modal-url="comparator-diffmerge.html" data-ui="modal"  data-modal-class="modal-dialog-big" > Show Details</button>',
		},
		{
			"Compared Objects" : "Clean",
			"Obeject Type"	: "Group",
			"Status"  : "yyyyyyyyyyyyyyyyyyyyy",			 		 
			"Action"  : '<button class="btn btn-clear" data-modal-url="comparator-diffmerge.html" data-ui="modal"  data-modal-class="modal-dialog-big" > Show Details</button>',
		},
		{
			"Compared Objects" : "Tea",
			"Obeject Type"	: "Service",
			"Status"  : "Source and Destination are not identicial",			 		 
			"Action"  : '<button class="btn btn-clear" data-modal-url="comparator-diffmerge.html" data-ui="modal"  data-modal-class="modal-dialog-big" > Show Details</button>',
		},
		{
			"Compared Objects" : "Cofee",
			"Obeject Type"	: "Rule",
			"Status"  : "xxxxxxxxxxxxxxxxxxxx",			 		 
			"Action"  : '<button class="btn btn-clear" data-modal-url="comparator-diffmerge.html" data-ui="modal"  data-modal-class="modal-dialog-big" > Show Details</button>',
		},
		{
			"Compared Objects" : "Milk",
			"Obeject Type"	: "Application Constant",
			"Status"  : "yyyyyyyyyyyyyyyyyyyyy",			 		 
			"Action"  : '<button class="btn btn-clear" data-modal-url="comparator-diffmerge.html" data-ui="modal"  data-modal-class="modal-dialog-big" > Show Details</button>',
		},
		{
			"Compared Objects" : "Horlicks",
			"Obeject Type"	: "Rule",
			"Status"  : "yyyyyyyyyyyyyyyyyyyyy",			 		 
			"Action"  : '<button class="btn btn-clear" data-modal-url="comparator-diffmerge.html" data-ui="modal"  data-modal-class="modal-dialog-big" > Show Details</button>',
		},
		{
			"Compared Objects" : "Horlicks",
			"Obeject Type"	: "Application Field",
			"Status"  : "yyyyyyyyyyyyyyyyyyyyy",			 		 
			"Action"  : '<button class="btn btn-clear" data-modal-url="comparator-diffmerge.html" data-ui="modal"  data-modal-class="modal-dialog-big" > Show Details</button>',
		},
		{
			"Compared Objects" : "Horlicks",
			"Obeject Type"	: "Group",
			"Status"  : "yyyyyyyyyyyyyyyyyyyyy",			 		 
			"Action"  : '<button class="btn btn-clear" data-modal-url="comparator-diffmerge.html" data-ui="modal"  data-modal-class="modal-dialog-big" > Show Details</button>',
		},
		{
			"Compared Objects" : "Horlicks",
			"Obeject Type"	: "Service",
			"Status"  : "yyyyyyyyyyyyyyyyyyyyy",			 		 
			"Action"  : '<button class="btn btn-clear" data-modal-url="comparator-diffmerge.html" data-ui="modal"  data-modal-class="modal-dialog-big" > Show Details</button>',
		},
		]
	};
	
	
};
