<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class SubTopic extends Model {

    protected $table = 'sub_topic';
    protected $primaryKey   = 'sub_topic_id';

}