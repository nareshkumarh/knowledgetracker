<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'WelcomeController@index');

//Route::get('home', 'HomeController@index');


Route::get('/', function()
{
	return View::make('index');
});

Route::get('/index.html', function() { return View::make('index'); });


Route::get('/home', function() { return View::make('home'); });
Route::get('/admin', function() { return View::make('admin'); });

//Route::get('/users', 'UserController@index');
Route::get('/users', function() { return View::make('user/list'); });
Route::get('/adduser', function() { return View::make('user/add'); });
Route::get('/addrole', function() { return View::make('role/add'); });
Route::get('/addcompetency', function() { return View::make('competency/add'); });
Route::get('/addcourse', function() { return View::make('course/add'); });
Route::get('/addtrack', function() { return View::make('careertrack/add'); });
Route::get('/pricing', function() { return View::make('pricing/list'); });
Route::get('/careertracks', function() { return View::make('careertrack/list'); });
Route::get('/topics', function() { return View::make('topic/list'); });
Route::get('/companies', function() { return View::make('company/list'); });
Route::get('/usercompetency/{username}', function() { return View::make('usercompetency/list'); });
Route::get('/usertopic/{username}', function() { return View::make('usertopics/list'); });
Route::get('/subtopics', function() { return View::make('subtopic/list'); });
Route::get('/usersubtopic/{username}', function() { return View::make('usersubtopics/list'); });
Route::get('/usersubtopic_interview/{username}', function() { return View::make('usersubtopics/interviewlist'); });
Route::get('/usersubtopic_interviewcleared/{username}', function() { return View::make('usersubtopics/interviewclearedlist'); });
Route::get('/usersubtopic_interviewfailed/{username}', function() { return View::make('usersubtopics/interviewfailedlist'); });
Route::get('/usercourses/{user_login_id}',  function() { return View::make('usercourse/list'); });
Route::get('/assigntopic/{user_login_id}',  function() { return View::make('usertopics/assigntopic'); });


Route::get('/usercompetencygap/{username}', function() { return View::make('usercompetencygap/list'); });
Route::post('/updateUserCompetency/{username}', 'UserCompetencyController@update');
Route::post('/freezeUserCompetency/{username}', 'UserCompetencyController@freeze');
Route::post('/syncUserCompetency/{username}', 'UserCompetencyController@sync');
//Route::get('/usercompetency/{user_id}', 'UserCompetencyController@index' );
Route::post('/user/login', 'UserController@login' );
//Route::get('/user/login', 'UserController@login' );
Route::get('/roles', function() { return View::make('role/list'); });
Route::get('/courses', function() { return View::make('course/list'); });
Route::get('/userrole/{user_login_id}', 'UserRoleController@index' );
Route::get('/usersubtopics/{user_login_id}', 'UserSubTopicController@index' );
Route::get('/usertopics/{user_login_id}', 'UserTopicController@index' );
Route::get('/usercourse/{user_login_id}', 'UserCourseController@index' );
Route::get('/userassigntopics/{user_login_id}', 'UserTopicController@listtopicstoassign' );
Route::post('/adduser/{user_login_id}', 'UserController@addUser' );

Route::get('/userCompetency/show/{user_acct_sid}', 'UserCompetencyController@show' );
Route::get('/roleCompetency/show/{role_sid}', 'RoleCompetencyController@show' );
Route::get('/userCompetencyGap/{user_login_id}', 'UserCompetencyGapController@show' );
Route::get('/usersubtopics_interview/{user_login_id}', 'UserSubTopicController@findInterviewPendingSubTopics' );
Route::get('/usersubtopics_interviewcleared/{user_login_id}', 'UserSubTopicController@findInterviewClearedSubTopics' );
Route::get('/usersubtopics_interviewfailed/{user_login_id}', 'UserSubTopicController@findInterviewFailedSubTopics' );
Route::get('/updateStatusUserSubTopics/{user_login_id}', 'UserSubTopicController@updateStatus' );
Route::get('/updateInterviewStatusUserSubTopics/{user_login_id}', 'UserSubTopicController@updateInterviewStatus' );
Route::post('/userCompetency/update/{user_login_id}', 'UserCompetencyController@updateCompetency' );

/* Admin*/
Route::get('/courses', function() { return View::make('course/list'); });
Route::get('/competencyy', function() { return View::make('competency/list'); });

Route::resource('user', 'UserController');
Route::resource('competency', 'CompetencyController');
Route::resource('topic', 'TopicController');
Route::resource('subtopic', 'SubTopicController');
Route::resource('usersubtopic', 'UserSubTopicController');
Route::resource('careertrack', 'CareerTrackController');
Route::resource('role', 'RoleController');
Route::resource('userCompetency', 'UserCompetencyController');
Route::resource('company', 'CompanyController');
Route::resource('course', 'CourseController');

/*Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);*/
