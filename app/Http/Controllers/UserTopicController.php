<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Response;
use \Illuminate\Support\Facades\DB;

class UserTopicController extends Controller {

	
	public function index($user_name)
	{
		 $userCompetency = DB::table('sub_topic_vw')
        ->join('topic', 'topic.topic_id', '=', 'sub_topic_vw.topic_id')
        ->join('competency', 'competency.comp_sid', '=', 'topic.competency_id')
        ->join('user_account', 'user_account.user_acct_sid','=', 'sub_topic_vw.user_acct_sid')
        ->select('competency.cname', 'topic.topic_name', 'sub_topic_vw.total','sub_topic_vw.cleared')  
        ->where('user_account.user_login_id',$user_name)
       // ->where('user_account.user_login_id',$user_name)
        ->get();
        return Response::json($userCompetency);
	}
	



}
