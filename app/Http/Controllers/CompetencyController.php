<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Response;
use App\User;
use \Illuminate\Support\Facades\DB;

class CompetencyController extends Controller {

	
	public function index()
	{
		 $userCompetency = DB::table('competency')
        ->join('course', 'course.comp_set_sid', '=', 'competency.comp_set_id')
        ->select('competency.comp_sid', 'competency.cname', 'course.set_name')
        //->where('user_competency.user_acct_sid',$user_acct_sid)
        //->where('user_account.user_login_id',$user_login_id)
        ->get();
        return Response::json($userCompetency);
	}



}
