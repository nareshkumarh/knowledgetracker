<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Response;
use App\User;
use \Illuminate\Support\Facades\DB;

class UserRoleController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($user_login_id)
	{
		
		$userCompetency = DB::table('user_role')
		->join('user_account', 'user_account.user_acct_sid','=', 'user_role.user_acct_sid')
		->join('role', 'role.role_sid','=', 'user_role.role_sid')
		->where('user_account.user_login_id',$user_login_id)
		->select('user_role.role_sid','role.role_name')		
		->get();
		return Response::json($userCompetency);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($user_acct_sid)
	{
		$users = User::find($user_acct_sid);
       return Response::json($users);	
	}
	
	public function login()
	{
		$user_login_id = $name =  $request -> input('user_login_id');
		$password = $name =  $request -> input('password');
		$users = DB::table('user_account')->where('user_login_id', $user_login_id)->where('user_password', $password)
		->select('user_acct_sid','oprtnl_flag')
		->get(); 
		return Response::json($users);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
