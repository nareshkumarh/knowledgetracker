<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Response;
use App\User;
use \Illuminate\Support\Facades\DB;

class UserController extends Controller {

	
	public function index()
	{		
		$users = DB::table('user_account')
		->select('user_acct_sid','user_login_id','oprtnl_flag')
		->get();
       return Response::json($users);
	}


	public function show($user_acct_sid)
	{
		$users = User::find($user_acct_sid);
       return Response::json($users);	
	}
	
	public function login(Request $request)
	{
		$user_login_id = $name =  $request -> input('user_login_id');
		$password = $name =  $request -> input('password');
		$users = DB::table('user_account')
		->where('user_login_id', $user_login_id)
		->where('user_password', $password)
		->where('oprtnl_flag', 'A')
		//->where('lock_flag', 'Y')
		->select('user_acct_sid','user_login_id','oprtnl_flag','lock_flag','user_type_cid')
		//->select('user_acct_sid','oprtnl_flag')
		->get(); 
		return Response::json($users);
	}
	
	public function addUser($user_login_id)
	{
		//"usertype":"2","username":"sada","password":"jkajdsk","pwdHint":"1","lockUser":"N"}
		$usertype =  $request -> input('usertype');
		$username =  $request -> input('username');
		$password =  $request -> input('password');
		$pwdHint =  $request -> input('pwdHint');
		$pwdHintAns =  $request -> input('pwdHintAns');
		$lockUser =  $request -> input('lockUser');
		
		$insertdata = array(
				'user_type_cid' => $usertype ,
				'user_login_id' => $username,
				'user_password' => $password ,
				'pwd_hint' => $pwdHint,
				'pwd_answer' => $pwdHintAns ,
				'pwd_expiry_date' => 'NOW()' ,
				'pwd_reminder_date' => 'NOW()' ,
				'lock_flag' => $lockUser,
				'oprtnl_flag' => 'A',
				'created_by' => '1' ,
				'created_date' => 'NOW()',
				'modified_by' => '1' ,
				'modified_date' => 'NOW()'
			);
		//echo $insertdata;
		DB::table('user_account')->insert( $insertdata	);
	
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
