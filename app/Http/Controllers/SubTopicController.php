<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Response;
use App\User;
use \Illuminate\Support\Facades\DB;

class SubTopicController extends Controller {

	
	public function index()
	{
		 $userCompetency = DB::table('sub_topic')
		 ->join('topic', 'topic.topic_id', '=', 'sub_topic.topic_id')
		 ->join('competency', 'competency.comp_sid', '=', 'topic.competency_id')
        ->join('course', 'course.comp_set_sid', '=', 'competency.comp_set_id')
        ->select('competency.comp_sid', 'competency.cname', 'course.set_name', 'topic.topic_name','sub_topic.sub_topic_name','sub_topic.topic_id')      
        ->get();
        return Response::json($userCompetency);
	}

	
	public function findByTopic($topic_id)
	{
		$userCompetency = DB::table('sub_topic')
		->join('topic', 'topic.topic_id', '=', 'sub_topic.topic_id')
		->join('competency', 'competency.comp_sid', '=', 'topic.competency_id')
		->join('course', 'course.comp_set_sid', '=', 'competency.comp_set_id')
		->select('competency.comp_sid', 'competency.cname', 'course.set_name', 'topic.topic_name','sub_topic.sub_topic_name','sub_topic.topic_id')
		->where('user_account.user_login_id',$user_login_id)
		->get();
		return Response::json($userCompetency);
	}
	


}
