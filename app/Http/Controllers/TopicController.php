<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Response;
use App\User;
use \Illuminate\Support\Facades\DB;

class TopicController extends Controller {

	
	public function index()
	{
		 $userCompetency = DB::table('topic')
		 ->join('competency', 'competency.comp_sid', '=', 'topic.competency_id')
        ->join('course', 'course.comp_set_sid', '=', 'competency.comp_set_id')
        ->select('competency.comp_sid', 'competency.cname', 'course.set_name', 'topic.topic_name')      
        ->get();
        return Response::json($userCompetency);
	}
	
	
	



}
