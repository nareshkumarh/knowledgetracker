<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Response;
use App\User;
use \Illuminate\Support\Facades\DB;

class RoleController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//$roles = DB::table('role')->get();
        //return Response::json($roles);	
        
        $userCompetency = DB::table('role')
        ->join('career_track', 'role.role_track_sid', '=', 'career_track.track_sid')
        ->select('role.role_sid', 'role.role_name', 'career_track.track_name')
        //->where('user_competency.user_acct_sid',$user_acct_sid)
        //->where('user_account.user_login_id',$user_login_id)
        ->get();
        return Response::json($userCompetency);
	}
	
	
}
