<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Response;
use App\User;
use \Illuminate\Support\Facades\DB;

class UserCompetencyController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$userCompetencies = UserCompetency::all();
       return Response::json($userCompetencies);
	}

	public function sync($username)
	{
		
	}
	
	public function update($username)
	{
	
	}
	
	public function freeze($username)
	{
	
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($user_login_id)
	{
		//$userCompetency = DB::table('user_competency')->where('user_acct_sid', $user_acct_sid)->get();
		$userCompetency = DB::table('user_competency')
		->join('competency', 'competency.comp_sid', '=', 'user_competency.comp_sid')
		->join('course', 'course.comp_set_sid', '=', 'competency.comp_set_id')
		->join('user_account', 'user_account.user_acct_sid','=', 'user_competency.user_acct_sid')
		->select('user_competency.user_comp_sid', 'user_competency.comp_sid', 'competency.cname', 'course.set_name', 'user_competency.scale')
		//->where('user_competency.user_acct_sid',$user_acct_sid)
		->where('user_account.user_login_id',$user_login_id)
		->get();
		return Response::json($userCompetency);
			
	}

	public function updateCompetency($user_login_id)
	{
		$userCompetencies = Input::all();
		if(is_array($userCompetencies)) 
   		{
	        for($i=0;$i < count($userCompetencies);$i++)
	        {
	        	echo $userCompetencies[$i];
	        }
   		}
		return Response::json($userCompetencies);
	}
	

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
