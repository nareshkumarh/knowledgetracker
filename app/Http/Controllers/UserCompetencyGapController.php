<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Response;
use App\User;
use \Illuminate\Support\Facades\DB;

class UserCompetencyGapController extends Controller {

	public function show($user_login_id)
	{
		$role_sid=  $request -> input('role_sid');
		$userCompetency = DB::table('user_competency')
		->join('competency', 'competency.comp_sid', '=', 'user_competency.comp_sid')
		->join('competency_set', 'competency_set.comp_set_sid', '=', 'competency.comp_set_id')
		->join('user_account', 'user_account.user_acct_sid','=', 'user_competency.user_acct_sid')		
		->join('user_role', 'user_role.user_acct_sid','=', 'user_account.user_acct_sid')
		->join('role', 'role.role_sid','=', 'user_role.role_sid')		
		->join('career_track', 'career_track.track_sid','=', 'role.role_track_sid')
		->join('role_competency', 'role_competency.competency_sid','=', 'competency.comp_sid')
		->select('user_competency.user_comp_sid', 'career_track.track_name','user_role.role_sid','role.role_name','user_competency.comp_sid', 'competency.cname', 'competency_set.set_name', 'user_competency.scale',
				'role_competency.scale as expected_scale')
		//->whereRaw('role_competency.role_sid = user_role.role_sid')
		->where('user_account.user_login_id',$user_login_id)
		->where('role_competency.role_sid',$role_sid)
		->get();
		return Response::json($userCompetency);
	}

}
