<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Response;
use App\User;
use \Illuminate\Support\Facades\DB;

class UserSubTopicController extends Controller {

	
	public function index($username)
	{
		 $userCompetency = DB::table('user_sub_topic')
		 ->join('sub_topic', 'sub_topic.sub_topic_id', '=', 'user_sub_topic.sub_topic_id')
		 ->join('topic', 'topic.topic_id', '=', 'user_sub_topic.topic_id')		 
		 ->join('user_account', 'user_account.user_acct_sid', '=', 'user_sub_topic.user_acct_sid')
        ->select('user_sub_topic.user_subtopic_id','topic.topic_name','sub_topic.sub_topic_name','sub_topic.topic_id','user_sub_topic.assigned_date' ,'user_sub_topic.start_date','user_sub_topic.end_date',
        		'user_sub_topic.status_code')
        ->where('user_account.user_login_id',$username)  
		->whereNotIn('user_sub_topic.status_code',array('L2'))		
        ->get();
        return Response::json($userCompetency);
	}
	
	public function findInterviewPendingSubTopics($username)
	{
		$userCompetency = DB::table('user_sub_topic')
		->join('topic', 'topic.topic_id', '=', 'user_sub_topic.topic_id')
		->join('sub_topic', 'sub_topic.sub_topic_id', '=', 'user_sub_topic.sub_topic_id')
		->join('user_account', 'user_account.user_acct_sid', '=', 'user_sub_topic.user_acct_sid')
		->select('user_sub_topic.user_subtopic_id','topic.topic_name','sub_topic.sub_topic_name','sub_topic.topic_id','user_sub_topic.assigned_date' ,'user_sub_topic.start_date','user_sub_topic.end_date',
				'user_sub_topic.status_code','user_sub_topic.interview_status_code')
		->where('user_account.user_login_id',$username)
		->where('user_sub_topic.status_code','L2')
		->whereNotIn('user_sub_topic.interview_status_code',array('F1','F2'))
		->get();
		return Response::json($userCompetency);
	}
	
	public function findInterviewClearedSubTopics($username)
	{
		$userCompetency = DB::table('user_sub_topic')
		->join('topic', 'topic.topic_id', '=', 'user_sub_topic.topic_id')
		->join('sub_topic', 'sub_topic.sub_topic_id', '=', 'user_sub_topic.sub_topic_id')
		->join('user_account', 'user_account.user_acct_sid', '=', 'user_sub_topic.user_acct_sid')
		->select('user_sub_topic.user_subtopic_id','topic.topic_name','sub_topic.sub_topic_name','sub_topic.topic_id','user_sub_topic.assigned_date' ,'user_sub_topic.start_date','user_sub_topic.end_date',
				'user_sub_topic.status_code','user_sub_topic.interview_status_code')
				->where('user_account.user_login_id',$username)
				->where('user_sub_topic.interview_status_code','F1')
				->get();
		return Response::json($userCompetency);
	}
	
	public function findInterviewFailedSubTopics($username)
	{
		$userCompetency = DB::table('user_sub_topic')
		->join('topic', 'topic.topic_id', '=', 'user_sub_topic.topic_id')
		->join('sub_topic', 'sub_topic.topic_id', '=', 'user_sub_topic.sub_topic_id')
		->join('user_account', 'user_account.user_acct_sid', '=', 'user_sub_topic.user_acct_sid')
		->select('user_sub_topic.user_subtopic_id','topic.topic_name','sub_topic.sub_topic_name','sub_topic.topic_id','user_sub_topic.assigned_date' ,'user_sub_topic.start_date','user_sub_topic.end_date',
				'user_sub_topic.status_code','user_sub_topic.interview_status_code')
				->where('user_account.user_login_id',$username)
				->where('user_sub_topic.interview_status_code','F2')
				->get();
		return Response::json($userCompetency);
	}

	function updateStatus (Request $request  )
	{		
		$user_subtopic_id =  $request -> input('user_subtopic_id');
		$status = $name =  $request -> input('status');
		if ( $status == 'L2') 
		{
			DB::table('user_sub_topic')
            ->where('user_subtopic_id', $user_subtopic_id)
            ->update(array('status_code' => $status, 'interview_status_code' => 'F0'));			
		}
		else
		{
				DB::table('user_sub_topic')
            ->where('user_subtopic_id', $user_subtopic_id)
            ->update(array('status_code' => $status));
		
		}
	
	}
	
	function updateInterviewStatus ( Request $request  )
	{		
		$user_subtopic_id =  $request -> input('user_subtopic_id');
		$status = $name =  $request -> input('status');
		if ( $status == 'F2') 
		{
			DB::table('user_sub_topic')
            ->where('user_subtopic_id', $user_subtopic_id)
             ->update(array('status_code' => 'L1', 'interview_status_code' => $status, 'modified_date' => 'NOW()',  'end_date'=>'NOW()'));			
		}
		else
		{
			DB::table('user_sub_topic')
            ->where('user_subtopic_id', $user_subtopic_id)
            ->update(array('interview_status_code' => $status,  'modified_date' => 'NOW()'));
		}
	
	}
	
	function insert1()
	{
		DB::table('users')->insert(
		array('email' => 'john@example.com', 'votes' => 0)
);
	}
	
	
	


}
