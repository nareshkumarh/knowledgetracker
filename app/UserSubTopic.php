<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class UserSubTopic extends Model {

    protected $table = 'user_sub_topic';
    protected $primaryKey   = 'user_subtopic_id';

}