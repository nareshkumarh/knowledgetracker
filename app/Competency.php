<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Competency extends Model {

    protected $table = 'competency';
    protected $primaryKey   = 'comp_sid';

}