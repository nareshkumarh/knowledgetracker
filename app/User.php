<?php 
namespace App;
use Illuminate\Database\Eloquent\Model;

class User extends Model {

    protected $table = 'user_account';
    protected $primaryKey   = 'user_acct_sid';

}