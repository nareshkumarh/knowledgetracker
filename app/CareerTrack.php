<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class CareerTrack extends Model {

    protected $table = 'career_track';
    protected $primaryKey   = 'track_sid';

}