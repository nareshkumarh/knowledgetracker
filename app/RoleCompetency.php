<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class RoleCompetency extends Model {

    protected $table = 'role_competency';
    protected $primaryKey   = 'rc_sid';

}