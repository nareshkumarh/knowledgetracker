<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class UserCompetency extends Model {

    protected $table = 'user_competency';
    protected $primaryKey   = 'user_comp_sid';

}