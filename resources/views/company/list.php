<script id="users-template" type="text/x-handlebars-template"> 		 
{{#each users}}    
		<tr><td>{{ plusOne @index }}</td><td>{{this.company_name}}</td></tr>
{{/each}}  
	</script>
<script>

	var source   = $("#users-template").html();
	var template = Handlebars.compile(source);
	
	
	
		$.getJSON('company', function (data) {
			console.log(data);			
			var html    = template({ "users" : data });			
			$("#user-tbl tbody").append(html);					 
		});
	
	Handlebars.registerHelper('plusOne', function(number) {
	    return number + 1;
	});
	
	</script>


<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-dashboard"></i> <a href="index.html">Home</a></li>
			<li class="active"><i class="fa fa-table"></i> Company</li>
		</ol>
	</div>
</div>
<!-- /.row -->

<div class="row">
	<div class="col-md-12">
		<h4 align='center'>Company</h4>
		
		<table border='1' id='user-tbl' class='table-condensed'>
			<thead>
				<tr>
					<th>Sno</th>
					<th>Company Name</th>
				</tr>
			</thead>
			<tbody></tbody>

		</table>
	</div>
</div>
<!-- /.row -->
