 <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>  <a href="index.html">Home</a>
                            </li>
							<li>
								Courses
							</li>
                            <li class="active">
                                 List all Courses
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
				<div class='title-bar' >
					<div class='title-text'>
						<h4> Courses</h4>
					</div>
					
				</div>
				<div class='loadcontent' style='padding: 20px;'>
<div class="row">
	<div class="col-md-4">
		<h4 align='center'>Course</h4>
		
		<table border='1' id='user-tbl' class='table-condensed'>
			<thead>
				<tr>
					<th>Sno</th>
					<th>Course Name</th>
					<th>Cost </th>
				</tr>
			</thead>
			<tbody>
				<tr>
				<td> 1</td>
				<td> Core Java </td>
				<td> Rs.1,000 </td>
				</tr>
				<tr>
				<td> 2</td>
				<td> Eclipse IDE </td>
				<td> Rs.500 </td>
				</tr>				
				<tr>
				<td> 3</td>
				<td> Oracle 11g (SQL) </td>
				<td> Rs.1,000 </td>
				</tr>
				<tr>
				<td> 4</td>
				<td> Hibernate </td>
				<td> Rs.1,000 </td>
				</tr>
				<tr>
				<th colspan='2'> Total </th>
				<th> Rs.3,500 </th>
				</tr>
			</tbody>

		</table>
	</div>
		<div class="col-md-4">
		<h4 align='center'>Course</h4>
		
		<table border='1' id='user-tbl' class='table-condensed'>
			<thead>
				<tr>
					<th>Sno</th>
					<th>Course Name</th>
					<th>Cost </th>
				</tr>
			</thead>
			<tbody>
				
				
				<tr>
				<td> 5</td>
				<td> Servlets/JSP </td>
				<td> Rs.1,000 </td>
				</tr>
				<tr>
				<td> 6</td>
				<td> Apache Tomcat  </td>
				<td> Rs.1,000 </td>
				</tr>
				<tr>
				<td> 7</td>
				<td> HTML 5  </td>
				<td> Rs.500 </td>
				</tr>
				<tr>
				<td> 8</td>
				<td> Javascript - JQuery, Ajax, JSON </td>
				<td> Rs.500 </td>
				</tr>
				<tr>
				<td> 9</td>
				<td> Spring </td>
				<td> Rs.1,000 </td>
				</tr>
				<th colspan='2'> Total </th>
				<th> Rs.4,000 </th>
				</tr>
			</tbody>

		</table>
	</div>
	<div class="col-md-4">
		<h4 align='center'>Course</h4>
		
		<table border='1' id='user-tbl' class='table-condensed'>
			<thead>
				<tr>
					<th>Sno</th>
					<th>Course Name</th>
					<th>Cost </th>
				</tr>
			</thead>
			<tbody>				
				<tr>
				<td> 10</td>
				<td> Log4J </td>
				<td> Rs. 500 </td>
				</tr>
				<tr>
				<td> 11</td>
				<td> Design Patterns </td>
				<td> Rs. 500 </td>
				</tr>
				<tr>
				<th colspan='2'> Total </th>
				<th> Rs.1,000 </th>
				</tr>
			</tbody>

		</table>
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<h4 align='center'>Continuous Integration</h4>
		
		<table border='1' id='user-tbl' class='table-condensed'>
			<thead>
				<tr>
					<th>Sno</th>
					<th>Course Name</th>
					<th>Cost </th>
				</tr>
			</thead>
			<tbody>
				<tr>
				<td> 12</td>
				<td> Unit Testing </td>
				<td> Rs.500 </td>
				</tr>
				<tr>
				<td> 13</td>
				<td> Maven </td>
				<td> Rs.500 </td>
				</tr>
				<tr>
				<td> 14</td>
				<td> PMD, FindBugs </td>
				<td> Rs.500 </td>
				</tr>
				<tr>
				<td> 15</td>
				<td> Hudson </td>
				<td> Rs.500 </td>
				</tr>
				<tr>
				<th colspan='2'> Total </th>
				<th> Rs.2,000 </th>
				</tr>
			</tbody>

		</table>
	</div>
		<div class="col-md-4">
		<h4 align='center'>Cloud </h4>
		
		<table border='1' id='user-tbl' class='table-condensed'>
			<thead>
				<tr>
					<th>Sno</th>
					<th>Course Name</th>
					<th>Cost </th>
				</tr>
			</thead>
			<tbody>
				
				<tr>
				<td> 16</td>
				<td> Amazon EC2 Cloud </td>
				<td> Rs.250 </td>
				</tr>
				<tr>
				<td> 17</td>
				<td> Amazon RDS </td>
				<td> Rs.250 </td>
				</tr>
				<tr>
				<td> 18</td>
				<td> Laravel Forge </td>
				<td> Rs. 250 </td>
				</tr>
				<tr>
				<td> 19</td>
				<td> Digital Ocean cloud </td>
				<td> Rs. 250 </td>
				</tr>
				
				<th colspan='2'> Total </th>
				<th> Rs.1,000 </th>
				</tr>
			</tbody>

		</table>
	</div>
	<div class="col-md-4">
		<h4 align='center'>BigData</h4>
		
		<table border='1' id='user-tbl' class='table-condensed'>
			<thead>
				<tr>
					<th>Sno</th>
					<th>Course Name</th>
					<th>Cost </th>
				</tr>
			</thead>
			<tbody>
				
				
				<tr>
				<td> 20</td>
				<td> Apache Hadoop</td>
				<td> Rs. 1,000 </td>
				</tr>
				<tr>
				<td> 21</td>
				<td> Hive </td>
				<td> Rs. 1,000 </td>
				</tr>
				<tr>
				<td> 22</td>
				<td> Pig </td>
				<td> Rs. 1,000 </td>
				</tr>
				<tr>
				<th colspan='2'> Total </th>
				<th> Rs.3,000 </th>
				</tr>
			</tbody>

		</table>
	</div>
</div>
</div>
                    </div>
                </div>