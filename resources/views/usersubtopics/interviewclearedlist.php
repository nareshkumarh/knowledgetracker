<script id="users-template" type="text/x-handlebars-template">
<div class="tab-content">
{{#each subtopics }}
<div class="tab-pane active" id={{@key}}'>
<h4> {{ @key }} </h4>
<div> 
<table class='table table-bordered table-striped table-condensed table-hover'><thead><tr><th width='5%'>Sno</th>
<th width='60%'> Questions </th> <th width='10%'> Completed Date </th>
<th width='10%'>Interview Status	 </th></tr></thead><tbody> 		 
{{#each this }}
<tr><td>  {{plusOne @index }} </td><td> {{ this.sub_topic_name }} </td><td> {{ this.end_date }}</td><td> {{ listscale user_comp_sid interview_status_code }} </td></tr>
{{/each}}
</tbody></table>
 </div>
{{/each}}
</div>
</div>

</script>  

<script>
var source   = $("#users-template").html();
var template = Handlebars.compile(source);

var username='naresh';
$.getJSON('usersubtopics_interviewcleared/' + username, function (data) {
	console.log(data);	
	var topic ={};
	var topicNames = _.unique( _.pluck(data, 'topic_name'));
	console.log(topicNames);
	var subtopics = {};
	for ( var c in topicNames )
	{
		var topicname = topicNames[c];
		var sub_topics = _.where( data, {"topic_name": topicname});
		var topic_id = sub_topics != null ? sub_topics[0].topic_id:"";
		topic[topic_id]= topicname;				
		subtopics[topicname]= sub_topics;
	}
	
	var html    = template({"topicNames": topic , "subtopics" : subtopics });			
	$("#accordion").append(html);
	//$( "#accordion" ).accordion({ heightStyle: "content" });
});

Handlebars.registerHelper('plusOne', function(number) {
    return number + 1;
});

Handlebars.registerHelper('listscale', function(cid,scale) {
	var content = "";
	if ( scale=='F0' )
	{
		content+="Waiting for Interview </option>";
	}
	else if ( scale =='F1')
	{
		content+="Cleared </option>";
	}
	else if ( scale == 'F2' )
	{
		content+="Failed </option>";
	}
	
	return new Handlebars.SafeString(content);
});

$("#syncBtn").on ('click', function(e) {
	e.preventDefault();
	var username = localStorage.getItem("username");
	console.log("username" + username);
	if ( username != null )
	{
		$.post("syncUserCompetency/" + username, function(data) {
			console.log("sync completed " + data );
			
		}); 
	}
	else
	{
		console.log("Invalid User" );
	}
});

$("#updateBtn").on ('click', function(e) {
	e.preventDefault();
	var username = localStorage.getItem("username");
	console.log("username" + username);
	if ( username != null )
	{
		$.post("updateUserCompetency/" + username, function(data) {
			console.log("update completed " + data );
			
		}); 
	}
	else
	{
		console.log("Invalid User" );
	}
});

$("#freezeBtn").on ('click', function(e) {
	e.preventDefault();
	var username = localStorage.getItem("username");
	console.log("username" + username);
	if ( username != null )
	{
		$.post("freezeUserCompetency/" + username, function(data) {
			console.log("Freeze completed " + data );
			
		}); 
	}
	else
	{
		console.log("Invalid User" );
	}
});

</script>

 <!-- Page Heading -->
       <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>  <a href="index.html">Home</a>
                            </li>
							<li>
								User Sub Topics 
							</li>
                            <li class="active">
                                 List all Cleared Interview Topics
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
				<div class='title-bar' >
					<div class='title-text'>
						<h4> My Interview Sub Topics - Cleared</h4>
					</div>
					
				</div>
				<div class='loadcontent' style='padding: 20px;'>
                <div class="row">
                    <div class="col-xs-12.col-md-12"> 
						<div class='widget'>							
							<div class='widget-body'>							
							
							<div class="tabbable">
							  <form id='mycompetencyFrm'>
									<div id="accordion">
									</div>
									<br>
									<br>
							
									</form>
							</div>
							</div>
						</div>
                    </div>
                </div>
                <!-- /.row -->
                
