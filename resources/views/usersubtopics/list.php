<script id="users-template" type="text/x-handlebars-template">
<div class="tab-content">
{{#each subtopics }}
<div class="tab-pane active" id={{@key}}'>
<h4> {{ @key }} </h4>
<div> 
<table class='table table-bordered table-striped table-condensed table-hover'><thead><tr><th width='5%'>Sno</th>
<th width='60%'> Questions </th> <th width='10%'> Assigned Date </th><th width='10%'> Start Date </th>
<th width='10%'>Status </th></tr></thead><tbody> 		 
{{#each this }}
<tr><td>  {{plusOne @index }} </td><td> {{ this.sub_topic_name }} </td><td> {{ this.assigned_date }}</td><td> {{ this.start_date }}</td><td> {{ listscale user_subtopic_id status_code }} </td></tr>
{{/each}}
</tbody></table>
 </div>
{{/each}}
</div>
</div>

</script>  

<script>

var username='naresh';
function updateStatus(cid)
{
	var status = $("#"+ cid ).val();
	console.log("rowid :" + cid  + ":status=" + status);
	var url = "updateStatusUserSubTopics/"+ username + "?user_subtopic_id=" + cid +"&status="+ status ;
	console.log(url);
	$.get( url, function(data) {
		console.log(data);
	});
}


var source   = $("#users-template").html();
var template = Handlebars.compile(source);


$.getJSON('usersubtopics/' + username, function (data) {
	console.log(data);	
	var topic ={};
	var topicNames = _.unique( _.pluck(data, 'topic_name'));
	console.log(topicNames);
	var subtopics = {};
	for ( var c in topicNames )
	{
		var topicname = topicNames[c];
		var sub_topics = _.where( data, {"topic_name": topicname});
		var topic_id = sub_topics != null ? sub_topics[0].topic_id:"";
		topic[topic_id]= topicname;				
		subtopics[topicname]= sub_topics;
	}
	
	var html    = template({"topicNames": topic , "subtopics" : subtopics });			
	$("#accordion").append(html);
	//$( "#accordion" ).accordion({ heightStyle: "content" });
});

Handlebars.registerHelper('plusOne', function(number) {
    return number + 1;
});

Handlebars.registerHelper('listscale', function(cid,scale) {
	var content = "";
	content+= "<select id="+ cid +" onchange='updateStatus("+ cid +")' >"; 
	content+="<option value='L0' "+ ( scale=='L0' ? 'selected' : '') +"> L0-Yet To Prepare </option>";
	content+="<option value='L1' "+ ( scale=='L1' ? 'selected' : '') +"> L1-Started Preparing </option>";
	content+="<option value='L2' "+ ( scale=='L2' ? 'selected' : '') + "> L2-Ready for Interview </option>";
	return new Handlebars.SafeString(content);
});

$("#syncBtn").on ('click', function(e) {
	e.preventDefault();
	var username = localStorage.getItem("username");
	console.log("username" + username);
	if ( username != null )
	{
		$.post("syncUserCompetency/" + username, function(data) {
			console.log("sync completed " + data );
			
		}); 
	}
	else
	{
		console.log("Invalid User" );
	}
});

$("#updateBtn").on ('click', function(e) {
	e.preventDefault();
	var username = localStorage.getItem("username");
	console.log("username" + username);
	if ( username != null )
	{
		$.post("updateUserCompetency/" + username, function(data) {
			console.log("update completed " + data );
			
		}); 
	}
	else
	{
		console.log("Invalid User" );
	}
});

$("#freezeBtn").on ('click', function(e) {
	e.preventDefault();
	var username = localStorage.getItem("username");
	console.log("username" + username);
	if ( username != null )
	{
		$.post("freezeUserCompetency/" + username, function(data) {
			console.log("Freeze completed " + data );
			
		}); 
	}
	else
	{
		console.log("Invalid User" );
	}
});

</script>

 <!-- Page Heading -->
       <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>  <a href="index.html">Home</a>
                            </li>
							<li>
								User Sub Topics 
							</li>
                            <li class="active">
                                 List all Sub Topics
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
				<div class='title-bar' >
					<div class='title-text'>
						<h4> My Technical Sub Topics</h4>
					</div>
					
				</div>
				<div class='loadcontent' style='padding: 20px;'>
                <div class="row">
                    <div class="col-xs-12.col-md-12"> 
						<div class='widget'>							
							<div class='widget-body'>							
							
							<div class="tabbable">
							  <form id='mycompetencyFrm'>
									<div id="accordion">
									</div>
									<br>
									<br>
							
							<!--
									<div class="col-md-12">
										<input type="button" id="updateBtn" name="Update" value="Update"> <input
											type="button" id="freezeBtn" name="Freeze" value="Freeze">
											<input type="button" id="syncBtn" name="Sync" value="Sync">
									</div>
									-->
									</form>
							</div>
							</div>
						</div>
                    </div>
                </div>
                <!-- /.row -->
                
