<script id="track-template" type="text/x-handlebars-template"> 		 
{{#each tracks}}    
		<tr><td>{{ plusOne @index }}</td><td>{{this.set_name}}</td><td>{{this.cname}}</td></tr>
{{/each}}  
	</script>
<script>

	var source   = $("#track-template").html();
	var template = Handlebars.compile(source);

	$.getJSON('competency', function (data) {
			console.log(data);			
			var html    = template({ "tracks" : data });			
			$("#user-tbl tbody").append(html);					 
		});
	
	Handlebars.registerHelper('plusOne', function(number) {
	    return number + 1;
	});
	
	</script>

	
      <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>  <a href="index.html">Home</a>
                            </li>
							<li>
								Competency
							</li>
                            <li class="active">
                                 List all Competency
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
				<div class='title-bar' >
					<div class='title-text'>
						<h4> Competencies</h4>
					</div>
					
				</div>
				<div class='loadcontent' style='padding: 20px;'>
                <div class="row">
                    <div class="col-xs-12.col-md-12"> 
						<div class='widget'>							
							<div class='widget-body'>
								<table border='1' id='user-tbl' class='table-condensed'>
			<thead>
				<tr>
					<th>Sno</th>
					<th> Set Name </th>
					<th>Name</th>
				</tr>
			</thead>
			<tbody></tbody>

		</table>
						</div>
					</div>
					</div>
					<br><br>
				</form>
							</div>
						</div>
                    </div>
                </div>
