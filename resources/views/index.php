<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Welcome to Knowledge Assessment Test</title>
	<!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<style>
		
	</style>
	<script src="../js/jquery-2.1.1.min.js"></script>
	  <script src="../js/bootstrap.min.js"></script>
	<script>

	$(document).ready ( function () {

		console.log("jquery loaded");
		$("#submitBtn").on('click', submitFrm);

		//clear credentials in localStorage 
		if ( localStorage.getItem("user_acct_id") != null )
		{
			//localStorage.removeItem("user_acct_id" );
			//localStorage.removeItem("username");
		}
		
	});

	function submitFrm(e){
		e.preventDefault();
		console.log('Form Submitted');
		var username = $("#username").val();
		var password = $("#password").val();
		
		$.post("user/login", {"user_login_id": username , "password": password }, function(data){
			console.log(data);
			if (data){
				if ( data.length == 0 )
				{
					alert("Invalid Login Credentials" );
				}
				else if ( data.length == 1 )
				{
					
					var obj = data[0];
					if ( obj.lock_flag =='Y')
					{
						alert("User Account is Locked");
					}
					else
					{	
						console.log(obj.user_acct_sid);
						var user_acct_id = obj.user_acct_sid;	
						var userType = obj.user_type_cid ;					
						localStorage.setItem("user_acct_id" , user_acct_id);
						localStorage.setItem("username" , username);						
						localStorage.setItem("user_type" , userType);
						document.getElementById("loginfrm").method="get"; 				 
						document.getElementById("loginfrm").action = userType == 1 ? "admin": "home";
						$("#loginfrm").submit();
					}
				}
			}
		});
		
	}
	
	</script>
</head>
<body>
	 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Knowledge Assessment Test</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right" role="form" id="loginfrm" method="post">
            <div class="form-group">
              <input type="text" id="username" placeholder="User Name" class="form-control"  required autofocus>
            </div>
            <div class="form-group">
              <input type="password" id="password" placeholder="Password" class="form-control"  required>
            </div>
            <button id="submitBtn" class="btn btn-success">Sign in</button>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h2>Assessment Services!</h2>
        <p>We are group of IT professional who are passionate to assess the knowledge of job seekers.</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">
          <h2>Java Track</h2>
          <p>Core Java, Servlets, JSP, JEE , Spring, Hibernate</p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-md-4">
          <h2> Database Track</h2>
          <p>SQL, PLSQL, Unit Testing</p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
       </div>
        <div class="col-md-4">
          <h2>BigData</h2>
          <p>Hadoop,HBase,Hive,Pig,Sqoop</p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div>
      </div>

    </div> <!-- /container -->
</body>
</html>
