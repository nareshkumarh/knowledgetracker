<script id="users-template" type="text/x-handlebars-template">
<div id="accordion">
{{#each userCompetency }}
<h4> {{ @key }} </h4>
<div> 
<table class='table table-bordered table-striped table-condensed table-hover'>
<thead><tr><th width='5%'>Sno</th><th width='70%'> Description </th><th width='10%'>Actual Scale </th><th> Expected Scale </th><th>Gap </th></tr></thead><tbody> 		 
{{#each this }}

  <tr {{if_scale  scale expected_scale }}>

<td>  {{plusOne @index }} </td><td> {{ this.cname }} </td><td> {{ scale }} </td><td> {{expected_scale}}</td> <td> {{ gap_score scale expected_scale}} </td></tr>
{{/each}}
</tbody></table>
 </div>
{{/each}}
</div>
		  
	</script>  
	<script>

	function getScore( scale )
	{
			if ( scale =='L1') return 1;
			else if ( scale =='L2') return 2;
			else if ( scale == 'L3' ) return 3;
			else if ( scale == 'L4' ) return 4;
			else return 0;
	}
	
	function getGapScore(actualScale,expectedScale){		
		return ( getScore(expectedScale) - getScore(actualScale)  );
	}
	
	Handlebars.registerHelper('if_scale', function(actual_scale,expected_scale) {

		var color = "";
		var score = getGapScore(actual_scale, expected_scale);		
		if ( score == 0 )
		{
			
		}
		else if ( score == 1)
		{
			color = "bgcolor ='orange'";
		}
		else if ( score > 1 )
		{
			color = "bgcolor ='red'";
		}
		else if ( score < 0 ){
			color="bgcolor='yellow'";
		}
		return new Handlebars.SafeString(color);
	});

	Handlebars.registerHelper('gap_score', function(actual_scale,expected_scale) {

		var score = getGapScore(actual_scale, expected_scale);	
		var gap = "";	
		if ( score == 0 )
		{
			
		}
		else if ( score == 1)
		{
			gap = score;
		}
		else if ( score > 1 )
		{
			gap = score;
		}	
		return new Handlebars.SafeString(gap);
	});
	
	var source   = $("#users-template").html();
	var template = Handlebars.compile(source);
	
	
		var user_login_id ='naresh';

		$("#next_role").on('change', function(){
			var next_role = $("#next_role").val();
			loadCompetencyGapData (user_login_id, next_role );
		});
		
		$("#updateBtn").on ('click', function (e) {
			console.log("Update Btn clicked");
			var competencies = {};
			$('#mycompetencyFrm select').each(function() {
				   console.log('select' + this.id + ":"+ $(this).val() );
				   competencies[this.id] = $(this).val();
			});
			var formData = {};
			formData["competencies"] =  competencies;
			alert(JSON.stringify(formData) );

			$.post('../userCompetency/update/naresh', {data:competencies} , {dataType: 'json'}, function(data) {
				alert(data);
			});
		});

		$.getJSON('../role', function (data) {
			console.log(data);
			for ( var i in data){
				var obj = data[i];
				$("#next_role").append("<option value=" + obj.role_sid +">" + obj.role_name +"</option>");
			}	
		});

		var roleSid=0;
		var current_role = "";
		console.log(user_login_id);
		$.getJSON('../userrole/'+ user_login_id, function (data) {
			console.log(data[0].role_sid);
			roleSid = data[0].role_sid;
			current_role= data[0].role_name;
			console.log('current_role'+current_role);
			loadCompetencyGapData (user_login_id, roleSid, current_role );
			console.log('roleSid1' + roleSid );
		});
		console.log('roleSid' + roleSid );
	

	function loadCompetencyGapData(user_login_id ,roleSid, current_role){

		console.log('arguments' + roleSid);
		$.getJSON('../userCompetencyGap/' + user_login_id +"?role_sid=" +roleSid, function (data) {
			console.log(data);		
			var competencySetNames = _.unique( _.pluck(data, 'set_name' ));
			var competencyNames = _.unique( _.pluck(data, 'cname' ));
			var current_role_sid = _.unique( _.pluck(data, 'role_sid' ));
			
			var track = _.unique( _.pluck(data, 'track_name' ));
			
			var userCompetencyData = {};
			for ( var c in competencySetNames )
			{
				var setName = competencySetNames[c];
				var userComp = _.where( data, {"set_name": setName});				
				userCompetencyData[setName]= userComp;
			}
			console.log(competencyNames);
			var html    = template({ "competencySetNames": competencySetNames , "competencyNames": competencyNames , "userCompetency" : userCompetencyData });
			//$("#c_accordion").empty();			
			$("#c_accordion").html(html);
			/*$( "#accordion" ).accordion({ heightStyle: "content" ,
		        autoHeight: false,
		        active: false,
		        collapsible: true
		    });*/
			$("#current_role").empty();
			$("#current_role").append(current_role);
			$("#track").empty();
			$("#track").append(track);
			
			
		});
			
	}

	Handlebars.registerHelper('plusOne', function(number) {
	    return number + 1;
	});

</script>

      <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>  <a href="index.html">Home</a>
                            </li>
							<li>
								Competency Gap
							</li>
                            <li class="active">
                                 List all Technical Competencies Gap
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
				<div class='title-bar' >
					<div class='title-text'>
						<h4> My Technical Competencies Gap</h4>
					</div>
					
				</div>
				<div class='loadcontent' style='padding: 20px;'>
                <div class="row">
                    <div class="col-xs-12.col-md-12"> 
						<div class='widget'>							
							<div class='widget-body'>
							  <form id='mycompetencyFrm' >
		        	<table width='100%'>
		        	
		        	<tr>
		        	<td>  	Career Track : <label id='track' > </label> 
		        	</td>
		        	<td>  	Current Role : <label id='current_role' > </label> 
		        	</td>
		        	<td> 
		        	Next Role: <select id="next_role"></select>
		        	</td>
		        	</tr>
		        	</table>
					<div id="c_accordion">
					</div>
					<br><br>
				</form>
							</div>
						</div>
                    </div>
                </div>
                <!-- /.row -->
                

				
	