<script id="users-template" type="text/x-handlebars-template">
{{#each userCompetency }}
<h4> {{ @key }} </h4>
<div> 
<table class='table table-bordered table-striped table-condensed table-hover'><thead><tr><th width='5%'>Sno</th><th width='80%'> Description </th><th width='10%'>Scale </th></tr></thead><tbody> 		 
{{#each this }}
<tr><td>  {{plusOne @index }} </td><td> {{ this.cname }} </td><td> {{ listscale user_comp_sid scale }} </td></tr>
{{/each}}
</tbody></table>
 </div>
{{/each}}
</script>  

<script>
var source   = $("#users-template").html();
var template = Handlebars.compile(source);

var username='naresh';
$.getJSON('../userCompetency/'+ username, function (data) {
	console.log(data);		
	var competencySetNames = _.unique( _.pluck(data, 'set_name' ));
	var competencyNames = _.unique( _.pluck(data, 'cname' ));
	console.log(competencySetNames);
	var userCompetencyData = {};
	for ( var c in competencySetNames )
	{
		var setName = competencySetNames[c];
		var userComp = _.where( data, {"set_name": setName});				
		userCompetencyData[setName]= userComp;
	}
	console.log(competencyNames);
	var html    = template({ "competencySetNames": competencySetNames , "competencyNames": competencyNames , "userCompetency" : userCompetencyData });			
	$("#accordion").append(html);
	//$( "#accordion" ).accordion({ heightStyle: "content" });
});

Handlebars.registerHelper('plusOne', function(number) {
    return number + 1;
});

Handlebars.registerHelper('listscale', function(cid,scale) {
	var content = "";
	content+= "<select id="+ cid +">"; 
	content+="<option value='L0' "+ ( scale=='L0' ? 'selected' : '') +"> L0-Yet To Explore </option>";
	content+="<option value='L1' "+ ( scale=='L1' ? 'selected' : '') +"> L1-Remembering </option>";
	content+="<option value='L2' "+ ( scale=='L2' ? 'selected' : '') + "> L2-Understanding </option>";
	content+="<option value ='L3' "+ ( scale=='L3' ? 'selected' : '') + "> L3-Applying </option>";
	content+="<option value ='L4' "+ ( scale=='L4' ? 'selected' : '') + "> L4-Analyzing </option>";
	content+="<option value ='L5' "+ ( scale=='L5' ? 'selected' : '') + "> L5-Evaluating </option>";
	content+="<option value ='L6' "+ ( scale=='L6' ? 'selected' : '') + "> L6-Creating </option>";
	return new Handlebars.SafeString(content);
});

$("#syncBtn").on ('click', function(e) {
	e.preventDefault();
	var username = localStorage.getItem("username");
	console.log("username" + username);
	if ( username != null )
	{
		$.post("syncUserCompetency/" + username, function(data) {
			console.log("sync completed " + data );
			
		}); 
	}
	else
	{
		console.log("Invalid User" );
	}
});

$("#updateBtn").on ('click', function(e) {
	e.preventDefault();
	var username = localStorage.getItem("username");
	console.log("username" + username);
	if ( username != null )
	{
		$.post("updateUserCompetency/" + username, function(data) {
			console.log("update completed " + data );
			
		}); 
	}
	else
	{
		console.log("Invalid User" );
	}
});

$("#freezeBtn").on ('click', function(e) {
	e.preventDefault();
	var username = localStorage.getItem("username");
	console.log("username" + username);
	if ( username != null )
	{
		$.post("freezeUserCompetency/" + username, function(data) {
			console.log("Freeze completed " + data );
			
		}); 
	}
	else
	{
		console.log("Invalid User" );
	}
});

</script>

 <!-- Page Heading -->
       <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>  <a href="index.html">Home</a>
                            </li>
							<li>
								Competency 
							</li>
                            <li class="active">
                                 List all Technical Competencies
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
				<div class='title-bar' >
					<div class='title-text'>
						<h4> My Technical Competencies</h4>
					</div>
					
				</div>
				<div class='loadcontent' style='padding: 20px;'>
                <div class="row">
                    <div class="col-xs-12.col-md-12"> 
						<div class='widget'>							
							<div class='widget-body'>
							  <form id='mycompetencyFrm'>
									<div id="accordion"></div>
									<br>
									<br>
							
									<div class="col-md-12">
										<input type="button" id="updateBtn" name="Update" value="Update"> <input
											type="button" id="freezeBtn" name="Freeze" value="Freeze">
											<input type="button" id="syncBtn" name="Sync" value="Sync">
									</div>
									</form>
							</div>
						</div>
                    </div>
                </div>
                <!-- /.row -->
                
