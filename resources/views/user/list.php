<script id="users-template" type="text/x-handlebars-template"> 		 
{{#each users}}    
		<tr><td>{{ plusOne @index }}</td><td>{{this.user_login_id}}</td></tr>
{{/each}}  
	</script>
<script>

	var source   = $("#users-template").html();
	var template = Handlebars.compile(source);
	
	
	
		$.getJSON('user', function (data) {
			console.log(data);			
			var html    = template({ "users" : data });			
			$("#user-tbl tbody").empty();
			$("#user-tbl tbody").append(html);					 
		});
	
	Handlebars.registerHelper('plusOne', function(number) {
	    return number + 1;
	});
	
	</script>

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i> <a href="index.html">Home</a></li>
			<li>Users</li>
			<li class="active">List all Users</li>
		</ol>
	</div>
</div>
<!-- /.row -->
<div class='title-bar'>
	<div class='title-text'>
		<h4>Users</h4>
		
	</div>

</div>
<div class='loadcontent' style='padding: 20px;'>
	<div class="row">
		<div class="col-xs-12.col-md-12">
			<div class='widget'>
			
				<div class='widget-body'>
				
					<table border='1' id='user-tbl' class='table-condensed'>
						<thead>
							<tr>
								<th>Sno</th>
								<th>Name</th>
							</tr>
						</thead>
						<tbody></tbody>

					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">	
		<br>
		<form>
		<a href="#" class="btn btn-sm btn-success" data-toggle="modal"  data-target="#basicModal" align='right'>Add User</a>
		<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    	<div class="modal-dialog">
        	<div class="modal-content">
	            <div class="modal-header">
	           	 	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	            	<h4 class="modal-title" id="myModalLabel">Add User</h4>
	            </div>
	            <div class="modal-body">
	                <h3>Modal Body</h3>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                <button type="button" class="btn btn-primary">Save changes</button>
	       		</div>
    		</div>
  		</div>
		</div>
		</form>
	</div>
</div>
</div>
</div>
<!-- /.row -->