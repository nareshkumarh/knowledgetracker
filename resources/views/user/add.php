<script>

$("#adduser").on('click', function (e){
	e.preventDefault();
	console.log('Add User Button clicked');
	var usertype = $("#usertype").val();
	var username = $("#username").val();
	var password = $("#password").val();
	var pwdHint = $("#pwdHint").val();
	var pwdHintAns =  $("#pwdHintAns").val();
	var lockUser = $("#lockUser").val();

	var formdata = { "usertype" : usertype , "username": username , "password": password , "pwdHint": pwdHint , "pwdHintAns": pwdHintAns , "lockUser" : lockUser };
	console.log("add user form data :" + JSON.stringify(formdata)) ; 
	console.log('Username' + username);
	console.log('Password' + password);

	var username = getUserName();
	
	$.post("adduser/" + username , formdata , function(data) {
		console.log("add user response:" + data );
	});
});
</script>

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-dashboard"></i> <a href="index.html">Home</a></li>
			<li class="active"><i class="fa fa-table"></i> Add User</li>
		</ol>
	</div>
</div>
<!-- /.row -->

<div class="row">
	<div class="col-md-12">
		<h4 align='center'>New User Registration</h4>
		<form id='adduserfrm' >
		<table border='1' id='user-tbl' class='table-condensed'>
			<tbody>
				<tr>
					<th>User Type</th>
					<td>
					<select id='usertype'>
						<option value='-1'> --Select--</option>
						<option value='2' selected> Student </option>
						<option value='1'> Admin </option>						
						<option value='3'> Trainer </option>						
					</select></td>
				</tr>
				<tr>
					<th>Username</th>
					<td> <input type='text' id='username' placeholder='UserName' autofocus></td>
				</tr>
				<tr>
					<th>Password</th>
					<td> <input type='password' id='password' placeholder='Password' ></td>
				</tr>
				<tr>
					<th>Password Hint</th>
					<td> <input type='text' id='pwdHint' placeholder='Password Hint' value ='1' readonly ></td>
				</tr>
				<tr>
					<th>Pwd Answer</th>
					<td> <input type='text' id='pwdHintAns' placeholder='Secret Answer' ></td>
				</tr>
				<tr>
					<th>Lock User</th>
					<td> <select id='lockUser' >
						<option value='Y'> Y </option>
						<option value='N' selected> N </option>
					</select>
					</td>
				</tr>				
				<tr>
					<th><input type='reset' name='reset'></th>
					<th><button id='adduser' > Add User</button></th>
										
				</tr>
				
			</tbody>

		</table>
		</form>
		<br>
		
	</div>
</div>
<!-- /.row -->

