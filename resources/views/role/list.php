<script id="users-template" type="text/x-handlebars-template"> 		 
{{#each users}}    
		<tr><td>{{ plusOne @index }}</td><td>{{this.track_name}}</td><td>{{this.role_name}}</td></tr>
{{/each}}  
	</script>
<script>

	var source   = $("#users-template").html();
	var template = Handlebars.compile(source);
	
	
	
		$.getJSON('role', function (data) {
			console.log(data);			
			var html    = template({ "users" : data });			
			$("#user-tbl tbody").append(html);					 
		});
	
	Handlebars.registerHelper('plusOne', function(number) {
	    return number + 1;
	});
	
	</script>


	
      <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>  <a href="index.html">Home</a>
                            </li>
							<li>
								Roles
							</li>
                            <li class="active">
                                 List all Roles
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
				<div class='title-bar' >
					<div class='title-text'>
						<h4> Roles</h4>
					</div>
					
				</div>
				<div class='loadcontent' style='padding: 20px;'>
                <div class="row">
                    <div class="col-xs-12.col-md-12"> 
						<div class='widget'>							
							<div class='widget-body'>
								<table border='1' id='user-tbl' class='table-condensed'>
			<thead>
				<tr>
					<th>Sno</th>
					<th>Track</th>
					<th>Role Name</th>
				</tr>
			</thead>
			<tbody></tbody>

		</table>
						</div>
					</div>
					</div>
					<br><br>
				</form>
							</div>
						</div>
                    </div>
                </div>
                <!-- /.row -->
	