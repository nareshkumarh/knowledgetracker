<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Welcome to Knowledge Assessment Test</title>
	<!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/jquery-ui.min.css">  
	
	<script src="js/jquery-2.1.1.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/bootstrap.min.js"></script>	  
	<script>

	var competencySet = { "OOPS": [{ "cid":"c1","name" :"Encapsulation" , "scale":'L2' }, { "cid":"c2","name" :"Inheritance" , "scale":'L1' }],
						"JDBC": [{ "name" :"Statement" , "scale":'L1' }, { "name" :"PreparedStatement" , "scale":'L2' }],
						"Collection": [{ "name" :"ArrayList" , "scale":'L3' }, { "name" :"HashMap" , "scale":'L1' }]};
	
	var dataMap = {};
	$(document).ready ( function () {
		console.log("jquery loaded");
		for ( var key in competencySet ){
			console.log(competencySet);
			
			var content = "<table border='1' width='100%'><thead><tr><th width='5%'>Sno</th><th width='80%'> Description </th><th width='10%'>Scale </th></tr></thead><tbody>";
			var competencies = competencySet[key];
			
			var i =0 ;
			for ( var k in competencies )
			{
				var c = competencies[k];
				content += "<tr><td>" + ++i + "</td><td> " + c.name + "</td><td>" + getScale(c.id, c.scale )+ "</td></tr>";
			}
			content+= "</tbody></table>";
			$("#accordion").append("<h3>"+ key + "</h3><div> " + content +"</div> ");
		}
		$( "#accordion" ).accordion({ heightStyle: "content" });
	});
	
	function getScale(cid , scale){
	
	var content = "";
	content+= "<select id="+ cid +">"; 
	content+="<option value='L1' "+ ( scale=='L1' ? 'selected' : '') +"> L1-Knowledge </option>";
	content+="<option value='L2' "+ ( scale=='L2' ? 'selected' : '') + "> L2-Comprehension </option>";
	content+="<option value ='L3' "+ ( scale=='L3' ? 'selected' : '') + "> L3-Application </option>";
	return content;
	}
	
	</script>
</head>
<body>
	 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Knowledge Assessment Test</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right" role="form">
            <div class="form-group">
              <input type="email" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <div class="container">
	  <h3> &nbsp;</h3>	
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-12">
		        <h3> Core Java </h3>	
		<div id="accordion">
		</div>
		<br><br>
		 <div class="col-md-12">
		<input type="button" name="Update" value="Update" > 
		<input type="button" name="Freeze" value="Freeze" > 
		</div>
      </div>

      <hr>
    </div> <!-- /container -->
</body>
</html>
