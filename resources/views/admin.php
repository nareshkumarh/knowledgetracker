
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Knowledge Management Tool</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/custom-theme-default.css" rel="stylesheet">

    <!-- Custom CSS -->
    <!--<link href="css/sb-admin.css" rel="stylesheet">-->

    <!-- Custom Fonts -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- jQuery -->
    <script src="../js/jquery-2.1.1.min.js"></script>
    <script src="../js/jquery-ui.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>	  
	<script src="../js/underscore-min.js"></script>	  
	<script src="../js/handlebars-v2.0.0.js"></script>		
	<script src="../js/jquery.easing.min.js"></script>		
	
	<script src="../js/jquery.easypiechart.min.js"></script>
	
	
	<script id="profile-template" type="text/x-handlebars-template">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>{{username}} <b class="caret"></b></a>
 
			<ul class="dropdown-menu">
                 <li>
                     <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                  </li>
                  <li>
                     <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                   </li>
                   <li>
                         <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#" onclick='logout()'><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
			
            </ul>
	
                       
	</script>  
	<script id="left-menu-template" type="text/x-handlebars-template">

	<ul>
                    <li>
                        <a href="javascript:location.reload()"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" data-link="../usercompetency/{{username}}" ><i class="fa fa-fw fa-bar-chart-o"></i> My Competencies</a>
                    </li>
                    <li >
                        <a href="javascript:void(0)" data-link="../usercompetencygap/{{username}}"><i class="fa fa-fw fa-table"></i> My Competency Gap</a>
                    </li>
 					<li >
                        <a href="javascript:void(0)" data-link="../assigntopic/{{username}}"><i class="fa fa-fw fa-table"></i> Assign Topics</a>
                    </li>
                     <li >
                        <a href="javascript:void(0)" data-link="../usertopic/{{username}}"><i class="fa fa-fw fa-table"></i> My Topics</a>
                    </li>
                     <li >
                        <a href="javascript:void(0)" data-link="../usersubtopic/{{username}}"><i class="fa fa-fw fa-table"></i> My Sub Topics</a>
                    </li>
                    <li>
                   		 <a href="javascript:void(0)" ><i class="fa fa-fw fa-table"></i> Interview </a>
                    <ul style='display:block'>
                    
                     <li >
                     
                        <a href="javascript:void(0)" data-link="../usersubtopic_interview/{{username}}"><i class="fa fa-fw fa-table"></i> Pending</a>
                    </li>
                     <li >
                        <a href="javascript:void(0)" data-link="../usersubtopic_interviewcleared/{{username}}"><i class="fa fa-fw fa-table"></i> Cleared  </a>
                    </li>
                     <li >
                        <a href="javascript:void(0)" data-link="../usersubtopic_interviewfailed/{{username}}"><i class="fa fa-fw fa-table"></i> Failed  </a>
                    </li>
                    </ul>
                    </li>
                    <!--  <li >
                        <a href="javascript:void(0)" data-link="../pricing"><i class="fa fa-fw fa-table"></i> Courses</a>
                    </li> -->
					
                    
                </ul>

	</script>
	
	<script>

	function getUserName()
	{
		var username = localStorage.getItem("username");	
		if ( !username )
		{
			window.location ="index.php";
		}
		return username;	
	}

	function selectUser(username )
	{
		localStorage.setItem("admin_selected_user", username );
	}
	
	var source   = $("#profile-template").html();
	var template = Handlebars.compile(source);
	var leftmenusource = $("#left-menu-template").html();
	var leftmenutemplate = Handlebars.compile(leftmenusource);
	 
	

	function logout()
	{
		localStorage.removeItem("username");
		localStorage.removeItem("user_type");
		localStorage.removeItem("user_acct_id");
		window.location ="index.php";
	}
	 
	$(document).ready ( function () {
			
			console.log("jquery loaded");


			var username = getUserName();
			var adminselecteUser = localStorage.getItem( "admin_selected_user");

			var html    = template({ "username": username});
			$("#profiledropdown").html(html); 
			$("#navigation_left").html( leftmenutemplate ({ "username": adminselecteUser ,"adminselecteUser" : adminselecteUser}));
			
			
			
			var username = localStorage.getItem("username");
			 $('a').on('click',function(){
				 	var link = $(this).attr("data-link");
				 	console.log('clicked' + link);

				 	if (link) {
					 	$.get( link, function( data ) {
						 	 //console.log(data);
					 		  $("#page-section").html(data);
					 	});
				 	}
				 	else
				 	{
						console.log("No data-link present");
				 	}
			       
			 });

			// update charts
			//$('.chartt').data('easyPieChart').update(40);
			
			var json = [ { "title" : "Core Java" , "percent": 80 } ,{ "title" : "Servlets" , "percent": 60 } ,{ "title" : "JSP" , "percent": 20 } ];
			for ( var i in json) 
			{
				var obj = json[i];
				drawChart ( i,  obj.title , obj.percent );
			}
	});
	
function drawChart( id, title, percent )
{

var widget ="<div class='col-lg-2 col-md-4 col-sm-6 col-xs-12 col-lg-push-1'>"+						
				"<div class='widget text-center'><div class='widget-header bg-theme custom-widget-header'>" +
						"<span class='widget-caption custom-widget-caption'> " + title  +"</span>"+
				"</div>"+
				"<div class='widget-body'>" ;
	var color = ( percent >= 80 ? 'green' : ( percent >= 50 && percent <80 ) ? 'orange' :'red');
	var charthtml = "<span class='chart_"+ id+"' data-percent=" + percent + "><span class='percent percent-sign'> "  + percent + "</span></span>";
	widget+= charthtml + "</div></div></div>";
	$("#mychart").append(widget);
	console.log( widget );
	updateBar(id,color,percent);
}

function updateBar(id, color, percent)
{

	$(".chart_" + id).easyPieChart({
				lineWidth: 10,
				trackColor:'#dfe0e0',
				barColor: color,
				easing: 'easeOutBounce',
				onStep: function(from, to, percent) {
					$(this.el).find('.percent').text(Math.round(percent));
				}
			});
}

	
</script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <div role="navigation" class="navbar navbar-inverse navbar-fixed-top">
           <div class="navbar-header pull-relative">
					<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand" title="Knowledge Tracker">Knowledge Tracker <small>1.0</small></a>
					<!--<div id="sidebar-collapse" class="sidebar-collapse" data-trigger="leftmenu-collapse">
						<i class="collapse-icon fa fa fa-dedent" title="Show/Hide Left Navigation"></i>
					</div>-->
			</div>
            <!-- Top Menu Items -->
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
               
        <!--  User Section -->
                <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Users <b class="caret"></b></a>
 
					<ul class="dropdown-menu">
		                 <li>
		                     <a href="javascript:void(0)" data-link="../users"><i class="fa fa-fw fa-user"></i> List Users</a>
		                  </li>
		                  <li>
		                     <a href="javascript:void(0)" data-link="../adduser"><i class="fa fa-fw fa-envelope"></i> Add User</a>
		                   </li>                  
		                 
		            </ul>
		         </li>
		         
		         <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Career Track <b class="caret"></b></a>
 
					<ul class="dropdown-menu">
					 <li >
                        <a href="javascript:void(0)" data-link="../careertracks"><i class="fa fa-fw fa-table"></i> Career Track</a>
                    </li>
		                  <li >
                        		<a href="javascript:void(0)" data-link="../roles"><i class="fa fa-fw fa-table"></i> Roles</a>
                    		</li>
                    		 <li >
                        <a href="javascript:void(0)" data-link="../addtrack"><i class="fa fa-fw fa-table"></i> Add Track</a>
                    </li>
		                  <li >
                        		<a href="javascript:void(0)" data-link="../addrole"><i class="fa fa-fw fa-table"></i> Add Role</a>
                    		</li>
		            </ul>
		         </li>
		         
		          <!--  User Section -->
                <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Competency <b class="caret"></b></a>
 
					<ul class="dropdown-menu">
		               
                    
                      <li >
                        <a href="javascript:void(0)" data-link="../courses"> Courses</a>
                        <ul>
                        <li>
                        	<a href="javascript:void(0)" data-link="../addcourse"> Add </a>
                    	</li>		            
                        </ul>
                    </li>
                    <li >
                        <a href="javascript:void(0)" data-link="../competencyy"> Competency</a>
                    <ul>
                     <li >
                        <a href="javascript:void(0)" data-link="../addcompetency" > Add </a>
                    </li>
                        </ul>
                        </li>                     
		           
		             <li >
                        <a href="javascript:void(0)" data-link="../topics"> Topics</a>
                    <ul>
                     <li >
                        <a href="javascript:void(0)" data-link="../addtopic"> Add </a>
                    </li>
                        </ul>
                        </li>      
                         <li >
                        <a href="javascript:void(0)" data-link="../subtopics"> Sub Topic</a>
                    <ul>
                     <li >
                        <a href="javascript:void(0)" data-link="../addsubtopic"> Add </a>
                    </li>
                        </ul>
                        </li>                     
		            </ul>
		          </li>
		           
                <li class="dropdown" id="profiledropdown">
                   <!-- Profile Template will get loaded -->
                </li>
               
            </ul>
			</div>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
			<aside id="left-panel">
			<nav>
            <div id="navigation_left" class="collapse navbar-collapse navbar-ex1-collapse">
                <!--  left menu -->				
            </div>
			</nav>
			</aside>
            <!-- /.navbar-collapse -->
        </div>

        <div id="page-container" class="page-container" >
            <div class="page-content" id="page-section">
				
				<div class='title-bar' >
					<div class='title-text'>
						<h4> Reports</h4>
					</div>
					
				</div>
				<div class='loadcontent' style='padding: 20px;'>
                
					<!--start row -->
                     <div class='row'>
						<!-- start chart -->
						<div id='mychart'>
						</div>
					</div>
				</div>
			</div>
			
        </div>
                </div>
				</div>
			</div>
		</div>
    <!-- /#wrapper -->
<div class="footer">
					<!-- <span class="pull-left margin-left10px">Copyrights &copy; 2014 </span><span class="last-modify">Last Modified on: 23/12/2014</span> <span class="poweredby">Powered by <a class="link" href="#" target="_blank">Talent Search Team </span>
				</div>
		<a href="#" class="scrolltop" data-scrolltop="true" title="Scroll to top"><i class="fa fa-angle-up"></i></a>
     -->
</body>

</html>
