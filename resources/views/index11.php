<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./assets/application-b91d681229d71801b5a32a320edab0f8.css" media="all" rel="stylesheet" type="text/css">
<link href="./assets/vendors-40d684d068a9bd2ef3a9ec206264cc01.css" media="all" rel="stylesheet" type="text/css">
<style type="text/css"></style>

<link href="../assets/views/welcome/index-f85aed7f267eb20a83e8c9cb2a298a88.css" media="screen" rel="stylesheet">

<!--<script src="./assets/index-4055bc0e07286434f9c8ff4a85984e94.js"></script>-->
<link href="./assets/lib_override-192ae0a4c6252657d1a95ef5d13f4723.css" media="all" rel="stylesheet" type="text/css">
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<title>
Knowledge Assessment | Welcome

</title>
<meta content="Welcome to Flat World." name="description">
<link href="./assets/css" rel="stylesheet" type="text/css">
<link href="http://188.226.202.114/assets/common/favicon-6edbbe76ca6db34cdd7122938bf1d62b.ico" rel="shortcut icon" type="image/x-icon">
</head>
<body>
<div class="top_header" role="navigation">
<div class="container">
<div class="login">
<ul class="list-inline">

<li class="desktop-only text">
Need Help? Call
<a href="tel:+1-877-257-9243">1.877.257.9243</a>
</li>
<li class="desktop-only text" id="sign_in">
<a href="http://catalog.188.226.202.114.com/users/sign_in">SIGN IN</a>
</li>
<li class="desktop-only bar">|</li>
<li class="desktop-only text">
<a href="http://catalog.188.226.202.114.com/users/sign_up">REGISTER</a>
</li>
</ul>
</div>
</div>
</div>
<nav class="bottom_header" role="navigation">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle" data-target="#menu" data-toggle="collapse" type="button">
<span class="sr-only">Menu</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a href="./assets/Flat World   Welcome.html" id="top-logo"><img alt="Logo" src="./assets/logo-599ec2b1330e05fff21558084f3b71db.png"></a>
</div>
<div id="menu">
<ul class="nav navbar-right navbar-nav">
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="http://188.226.202.114/#">Products</a>
<ul class="dropdown-menu">
<li>
<a href="http://students.188.226.202.114.com/">For Students</a>
</li>
<li>
<a href="http://catalog.188.226.202.114.com/">For Educators</a>
</li>
<li>
<a href="http://188.226.202.114/institution">For Institutions</a>
</li>
<li>
<a href="http://188.226.202.114/cbe">Learning Platform</a>
</li>
</ul>
</li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="http://188.226.202.114/#">About</a>
<ul class="dropdown-menu">
<li>
<a href="http://188.226.202.114/vision">Our Vision</a>
</li>
<li>
<a href="http://188.226.202.114/our_management">Executive Team</a>
</li>
<li>
<a href="http://188.226.202.114/our_culture">Our Culture</a>
</li>
<li>
<a href="http://188.226.202.114/blog">Blog</a>
</li>
<li>
<a href="http://188.226.202.114/blog#newsroom">Newsroom</a>
</li>
</ul>
</li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="http://188.226.202.114/#">Contact</a>
<ul class="dropdown-menu">
<li>
<a href="http://catalog.188.226.202.114.com/help/tickets">Contact Us</a>
</li>
<li>
<a href="http://catalog.188.226.202.114.com/customer_support">Support</a>
</li>
<li>
<a href="http://188.226.202.114/careers">Careers</a>
</li>
<li>
<a href="http://catalog.188.226.202.114.com/help">FAQs</a>
</li>
</ul>
</li>
</ul>
</div>
</div>
</nav>

<div data-ride="carousel" id="main_slides">
<!-- Wrapper for slides -->
<div class="carousel-inner">
<div class="item">
<img alt="Main slider 1" class="img-responsive" src="./assets/main_slider_1-10dfd5e3ce06923bcc859e1ee3a00a2c.jpg">
<div class="carousel-caption">
<h3>What if we started from scratch?</h3>
<p>We are Flat World. Learn on.</p>
<a class="standard-btn" href="http://188.226.202.114/vision">Our Vision</a>
</div>
</div>
<div class="item">
<img alt="Main slider 2" class="img-responsive" src="./assets/main_slider_2-4bf054cfeb99761afbe75d0d3771fc09.jpg">
<div class="carousel-caption">
<h3>What if we started from scratch?</h3>
<p>We are Flat World. Learn on.</p>
<a class="standard-btn" href="http://188.226.202.114/vision">Our Vision</a>
</div>
</div>
<div class="item active">
<img alt="Main slider 3" class="img-responsive" src="./assets/main_slider_3-c4d4910db8d552abdf483761244459e5.jpg">
<div class="carousel-caption">
<h3>What if we started from scratch?</h3>
<p>We are Flat World. Learn on.</p>
<a class="standard-btn" href="http://188.226.202.114/vision">Our Vision</a>
</div>
</div>
</div>
</div>

<div class="separator"></div>
<div class="down_arrow page-scroll" id="focus_arrow">
<a href="http://188.226.202.114/#focus"><img alt="Down arrow" class="img-responsive" src="./assets/down_arrow-676e4a6e469f85604589342fa4761aec.png"></a>
</div>
<div id="focus">
<div class="container">
<h2>Agile Digital Learning Solutions</h2>
<div class="focus_specializations">
<div class="focus_icon">
<img alt="Red textbooks icon" class="img-responsive" src="./assets/red_textbooks_icon-ea442230bfbabeb4c43df06d21f28560.png">
</div>
<div class="box" id="digital_textbooks">
<div class="header">
<h3>Digital Textbooks</h3>
<p>FOR EDUCATORS &amp; STUDENTS</p>
</div>
<div class="body">
<p>Educators can personalize and adopt our award-winning, peer-reviewed textbooks for their students.</p>
<p class="content_link">
<a href="http://catalog.188.226.202.114.com/">Search the Catalog</a>
</p>
</div>
</div>
</div>
<div class="focus_specializations">
<div class="focus_icon">
<img alt="Yellow school icon" class="img-responsive" src="./assets/yellow_school_icon-807995251f649755d277a7d5a97150f3.png">
</div>
<div class="box" id="custom_courses">
<div class="header">
<h3>Courseware Solutions</h3>
<p>FOR INSTITUTIONS</p>
</div>
<div class="body">
<p>Featuring embedded assessments, learning journal, social learning, analytics and more.</p>
<p class="content_link">
<a href="http://188.226.202.114/institution">See Courseware Solutions</a>
</p>
</div>
</div>
</div>
<div class="focus_specializations">
<div class="focus_icon">
<img alt="Green brain icon" class="img-responsive" src="./assets/green_brain_icon-abf15f5b340ad0820310e5f8b2db571b.png">
</div>
<div class="box" id="learning_platform">
<div class="header">
<h3>Learning Platform</h3>
<p>FOR PERSONALIZATION</p>
</div>
<div class="body">
<p>Our mobile platform powers many of the most progressive competency-based education initiatives.</p>
<p class="content_link">
<a href="http://188.226.202.114/cbe">Learn More</a>
</p>
</div>
</div>
</div>
</div>
</div>
<div class="down_arrow page-scroll">
<a href="http://188.226.202.114/#first_story"><img alt="Down arrow" class="img-responsive" src="./assets/down_arrow-676e4a6e469f85604589342fa4761aec.png"></a>
</div>
<div class="story_container" id="first_story">
<div class="story">
<div class="story_header">
<div class="story_icon">
<img alt="Story icon1" class="img-responsive" src="./assets/story_icon1-7941428f22b03b74e61b105172865bac.png">
</div>
<div class="story_title">
The Great
<br>
Opportunity
</div>
</div>
<div class="story_content">
<p>The rapid speed with which knowledge and skills become obsolete will soon create a global society of lifelong learners, greatly increasing and extending the market for postsecondary education.</p>
</div>
</div>
</div>
<div class="story_container" id="second_story">
<div class="story right_side_story">
<div class="story_header">
<div class="story_icon">
<img alt="Story icon2" class="img-responsive" src="./assets/story_icon2-bc3aa0b00d8fa5a76383598577292c1b.png">
</div>
<div class="story_title">
Success in
<br>
Digital Textbooks
</div>
</div>
<div class="story_content">
<p>Rethinking the accessibility of educational content, our digital-first textbooks and custom courses provide rich, engaging experiences that adapt to any learning style.</p>
<a class="standard-btn" href="http://catalog.188.226.202.114.com/">Learn More</a>
</div>
</div>
</div>
<div class="story_container" id="third_story">
<div class="story">
<div class="story_header">
<div class="story_icon">
<img alt="Story icon3" class="img-responsive" src="./assets/story_icon3-9579995957e11b89362dd6e0384320c9.png">
</div>
<div class="story_title">
Next Generation
<br>
of Higher Education
</div>
</div>
<div class="story_content">
<p>Turning our focus to the greatest challenges in higher education today — high student attrition rates and a postsecondary system unable to serve a growing population of learners —we continue to move beyond digital textbooks to engineer a more comprehensive solution.</p>
</div>
</div>
</div>
<div class="story_container" id="fourth_story">
<div class="story right_side_story">
<div class="story_header">
<div class="story_icon">
<img alt="Story icon4" class="img-responsive" src="./assets/story_icon4-8444db0a5203f72051d96ca90f9d42a4.png">
</div>
<div class="story_title">
Education as
<br>
an Equalizer
</div>
</div>
<div class="story_content">
<p>Breakthroughs in mobile, adaptive, and social platform design offer students a more learner-centered experience. Our platform provides big-data analytics that allow faculty to teach with a personalized approach to improve learning outcomes, while reaching a wider audience. Through the credit model or mastery of competencies, we can show your institution a whole new world of possibilities.</p>
<a class="standard-btn" href="http://188.226.202.114/cbe">Learn More</a>
</div>
</div>
</div>
<div class="large-separator">
<h2>See What People are Saying</h2>
</div>
<div class="testimonials" id="quotes_testimonials">
<div class="container">
<div id="slider_container">
<div class="testimonial_icon">
<img alt="Chat bubbles" src="./assets/chat_bubbles-8e3708c2eb332fc6fed1c2d50fb85666.png" draggable="false">
</div>
<ul class="slides">
<li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
<p class="testimonial">
"I look forward to making Flat World texts my foundation whenever I have a choice in the matter."
</p>
<p class="testimonial_author">
Alan Adams, from Dean College on Digital Textbooks
</p>
</li>
<li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
<p class="testimonial">
"They’ve developed a scalable business model in a huge market while helping more college students to get a personalized education experience at an affordable cost."
</p>
<p class="testimonial_author">
Evan Burfield, Co-Founder of 1776 on Flat World
</p>
</li>
<li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" class="">
<p class="testimonial">
"They are very particular about the authors and titles they choose to publish which means you can count on their quality."
</p>
<p class="testimonial_author">
Whitney Kilgore, Faculty at eCommons on Flat World content
</p>
</li>
<li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" class="">
<p class="testimonial">
"It’s eTextbook platform and digital content...give Flat World a leg up...in adaptive learning and personalization tools."
</p>
<p class="testimonial_author">
Rip Emson, from TechCrunch.com on Flat World products
</p>
</li>
</ul>
</div>
</div>
</div>

<div class="down_arrow page-scroll" id="statistics_arrow">
<a href="http://188.226.202.114/#statistics"><img alt="Down arrow" class="img-responsive" src="./assets/down_arrow-676e4a6e469f85604589342fa4761aec.png"></a>
</div>
<div id="statistics">
<div id="statistics_container">
<div id="learning_outcomes">
<h4>
Improving
<br>
Learning Outcomes
</h4>
<p>
Our learning platform is designed to meet the individual needs of a diverse student audience and positively impact learning outcomes.
</p>
</div>
<div id="statistics_slider">
<div class="previous_statistic">
<img alt="Back arrow" class="img-responsive" src="./assets/back_arrow-3f7b96342a2f5d80d16010f0d357ba87.png">
</div>
<div class="slide" id="slide_1" style="display: none;">
<div class="statistics_amount_container">
<img alt="Briefcase statistic" class="img-responsive" src="./assets/briefcase_statistic-64bb496f19ed13b8b3b580fbf88dc325.png">
</div>
<div class="statistics_info">
<p class="headline">PREPARING FOR THE WORKFORCE</p>
<p class="statistic">By 2018, 63% of job openings will require workers with at least some college education.</p>
<p class="source">Georgetown 2010</p>
</div>
</div>
<div class="slide" id="slide_2" style="display: block;">
<div class="statistics_amount_container">
<img alt="Clock statistic" class="img-responsive" src="./assets/clock_statistic-a82a438bf27f76e821fab661527a7463.png">
</div>
<div class="statistics_info">
<p class="headline">IT’S ABOUT WHAT YOU KNOW</p>
<p class="statistic">On average, it takes 1/2 the time to complete a competency-based bachelor degree compared to a traditional program.</p>
<p class="source">New America Foundation 2014</p>
</div>
</div>
<div class="slide" id="slide_3" style="display: none;">
<div class="statistics_amount_container">
<img alt="Man with tie statistic" class="img-responsive" src="./assets/man_with_tie_statistic-4bcb7c211010f5fb36c19c53d7b0ebc3.png">
</div>
<div class="statistics_info">
<p class="headline">ADDRESSING THE ISSUE OF ACCESS</p>
<p class="statistic">36.3 million working-age adults (ages 25-64) have some college but no degree.</p>
<p class="source">Lumina Foundation 2013</p>
</div>
</div>
<div class="slide" id="slide_4" style="display: none;">
<div class="statistics_amount_container">
<img alt="Brain statistic" class="img-responsive" src="./assets/brain_statistic-4c46f8706b9185721c4dc76f72cae3f8.png">
</div>
<div class="statistics_info">
<p class="headline">COMPETENCY-BASED VS CREDIT HOUR</p>
<p class="statistic">70% of Americans believe that students should receive credit for demonstrating mastery of material.</p>
<p class="source">Lumina and Gallup 2013</p>
</div>
</div>
<div class="next_statistic">
<img alt="Forward arrow" class="img-responsive" src="./assets/forward_arrow-0683fd6d604638005b6e8b6e4e4ed20c.png">
</div>
</div>
</div>
</div>

<div id="newsletter_signup">
<h1>
Sign Up For Our Newsletter
</h1>
<p>
Stay up to date with Flat World
</p>
<div class="field">
<div class="input-group">
<form accept-charset="UTF-8" action="http://www.188.226.202.114.com/join-friends-list?email_address=" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="✓"></div>
<input class="form-control" id="email_field" name="email_address" placeholder="Email" type="email" value="">
<span class="input-group-btn">
<input id="newsletter_submit_button" name="commit" type="submit" value="Submit">
</span>
</form>

</div>
</div>
</div>


<div class="mobile"></div>
<div id="upper_footer">
<div class="container">
<div class="footer_section">
<div class="footer_section_info" id="contact">
<div class="header">
<a href="http://catalog.188.226.202.114.com/help/tickets">
<img alt="Phone" class="img-responsive" src="./assets/phone-6bbd0e475432e7b2f60100b29917c606.png">
<span>Contact</span>
</a>
</div>
<p>
55,2nd Street, 
<br>
Ganesh Nagar,
<br>
Arakkonam.
</p>
<p>
Phone
<a href="tel:+919994194773">
+91-9994194773
</a>
<br>
</p>
</div>
</div>
<div class="footer_section">
<div class="footer_section_info" id="support">
<div class="header">
<a href="http://catalog.188.226.202.114.com/help/tickets">
<img alt="Wrenches" class="img-responsive" src="./assets/wrenches-8d6c8f0f4fc61607a32eeac77888b0e7.png">
<span>Support</span>
</a>
</div>
<p>
<a href="https://server.iad.liveperson.net/hc/91746575/?cmd=file&file=visitorWantsToChat&site=91746575&byhref=1&imageUrl=https://server.iad.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/Small/2a" target="_blank">Live Chat</a>
<br>
<a href="mailto:nareshkumarh@live.com">nareshkumarh@live.com</a>
</p>
<p>
<strong>Hours</strong>
<br>
Monday-Friday   7am-9am IST
<br>
Saturday-Sunday 7am-7pm IST
</p>
</div>
</div>
<div class="footer_section">
<div class="footer_section_info" id="bookstore">
<div class="header">
<a href="http://bookstore.188.226.202.114.com/">
<img alt="Book" class="img-responsive" src="./assets/book-a4d116a7cbfa35e77196beb78f8441b4.png">
<span>Bookstore</span>
</a>
</div>
<p>
Visit our bookstore and bookseller information page to learn more.
</p>
</div>
</div>
</div>
</div>
<footer>
<div class="container">
<div id="social-networks-footer-block">
<a href="http://facebook.com/188.226.202.114">
<i class="ion-social-facebook"></i>
</a>
<a href="http://twitter.com/flat_world">
<i class="ion-social-twitter"></i>
</a>
<a href="https://www.linkedin.com/company/flat-world-knowledge-inc">
<i class="ion-social-linkedin"></i>
</a>
<a href="https://www.youtube.com/user/188.226.202.114">
<i class="ion-social-youtube"></i>
</a>
</div>
<div id="copyright-block">
<p>
©2014 Knowledge Assessment Pvt Ltd.
|
<a href="http://catalog.188.226.202.114.com/legal">Legal</a>
</p>
</div>
</div>
</footer>
<div id="back-to-top" style="display: none;">
<i class="ion-ios7-arrow-up"></i>
</div>



<div id="lightboxOverlay" class="lightboxOverlay" style="display: none;"></div><div id="lightbox" class="lightbox" style="display: none;"><div class="lb-outerContainer"><div class="lb-container"><img class="lb-image" src=""><div class="lb-nav"><a class="lb-prev" href=""></a><a class="lb-next" href=""></a></div><div class="lb-loader"><a class="lb-cancel"></a></div></div></div><div class="lb-dataContainer"><div class="lb-data"><div class="lb-details"><span class="lb-caption"></span><span class="lb-number"></span></div><div class="lb-closeContainer"><a class="lb-close"></a></div></div></div></div></body></html>