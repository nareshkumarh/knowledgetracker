<script>

$.getJSON('competencySet', function (data) {
	console.log(data);			
	for ( var i in data)
	{
		var obj = data[i];
		var option = "<option value=" + obj.comp_set_sid  +"> "+ obj.set_name + "</option>";
		$("#competencyset").append(option);					 
	}			
	
});

$("#addcompetency").on('click', function (e){
	e.preventDefault();
	console.log('Add User Button clicked');
	var set_id = $("#competencyset").val();
	var cname = $("#cname").val();
	var desc = $("#desc").val();
	console.log( "setid=" + set_id +",cname:" + cname +",desc="+desc );
	
});
</script>

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-dashboard"></i> <a href="index.html">Home</a></li>
			<li class="active"><i class="fa fa-table"></i> Add Competency</li>
		</ol>
	</div>
</div>
<!-- /.row -->

<div class="row">
	<div class="col-md-12">
		<h4 align='center'>New Competency Registration</h4>
		<form id='adduserfrm'>
		<table border='1' id='user-tbl' class='table-condensed'>
			<tbody>
				<tr>
					<th>Competency Set Name</th>
					<td> 
					<select id='competencyset' >				
					</select></td>
				</tr>
				<tr>
					<th>Competency Name</th>
					<td> <input type='text' id='cname' placeholder='Competency Name' ></td>
				</tr>
				<tr>
					<th>Description</th>
					<td> <input type='text' id='desc' placeholder='Description' ></td>
				</tr>
				<tr>
					<th><input type='reset' name='reset'></th>
					<th><button id='addcompetency' > Add </button></th>
										
				</tr>
				
			</tbody>

		</table>
		</form>
		<br>
		
	</div>
</div>
<!-- /.row -->

