<script id="users-template" type="text/x-handlebars-template">
{{#each userCompetency }}
<h4> {{ @key }} </h4>
<div> 
<table class='table table-bordered table-striped table-condensed table-hover'><thead><tr><th width='5%'>Sno</th>
<th width='80%'> Topic Name </th><th> Stats </th>
<th width='10%'>Status </th></tr></thead><tbody> 		 
{{#each this }}
<tr><td>  {{plusOne @index }} </td><td> {{ this.topic_name }} </td><td> {{ this.cleared }}/ {{this.total}} </td><td> {{listscale this.cleared this.total }}</td></tr>
{{/each}}
</tbody></table>
 </div>
{{/each}}
</script>  

<script>
var source   = $("#users-template").html();
var template = Handlebars.compile(source);

var username='naresh';
$.getJSON('usertopics/'+ username, function (data) {
	console.log(data);
	var competencyNames = _.unique( _.pluck(data, 'cname' ));	
	var userCompetencyData = {};
	for ( var c in competencyNames )
	{
		var competencyname = competencyNames[c];
		var userComp = _.where( data, {"cname": competencyname});				
		userCompetencyData[competencyname]= userComp;
	}
	console.log(competencyNames);
	var html    = template({"competencyNames": competencyNames , "userCompetency" : userCompetencyData });			
	$("#accordion").append(html);
	//$( "#accordion" ).accordion({ heightStyle: "content" });
});

Handlebars.registerHelper('plusOne', function(number) {
    return number + 1;
});

Handlebars.registerHelper('listscale', function(cleared,total) {
	var percent = 0;
	var pendingpercent = 100;
	if ( parseInt(cleared) > 0 && parseInt(total) > 0)
	{
		percent = Math.round(100*parseInt(cleared)/parseInt(total));
		pendingpercent = 100 - percent;
		console.log( percent + ':' + pendingpercent );
	}
	
	
	var content = "";
	content+='<div class="progress" >';	
	content+='<div class="progress-bar progress-bar-success" style="width: '+percent+'%">' +  percent+'% Complete </div>'+
  			 '<div class="progress-bar progress-bar-warning progress-bar-striped" style="width: '+ pendingpercent+'%">' +  +pendingpercent+'% Pending </div>';	  
	  content+='</div>';
	return new Handlebars.SafeString(content);
});

$("#syncBtn").on ('click', function(e) {
	e.preventDefault();
	var username = localStorage.getItem("username");
	console.log("username" + username);
	if ( username != null )
	{
		$.post("syncUserCompetency/" + username, function(data) {
			console.log("sync completed " + data );
			
		}); 
	}
	else
	{
		console.log("Invalid User" );
	}
});

$("#updateBtn").on ('click', function(e) {
	e.preventDefault();
	var username = localStorage.getItem("username");
	console.log("username" + username);
	if ( username != null )
	{
		$.post("updateUserCompetency/" + username, function(data) {
			console.log("update completed " + data );
			
		}); 
	}
	else
	{
		console.log("Invalid User" );
	}
});

$("#freezeBtn").on ('click', function(e) {
	e.preventDefault();
	var username = localStorage.getItem("username");
	console.log("username" + username);
	if ( username != null )
	{
		$.post("freezeUserCompetency/" + username, function(data) {
			console.log("Freeze completed " + data );
			
		}); 
	}
	else
	{
		console.log("Invalid User" );
	}
});

</script>

 <!-- Page Heading -->
       <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>  <a href="index.html">Home</a>
                            </li>
							<li>
								User Topics 
							</li>
                            <li class="active">
                                 List all Technical Topics
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
				<div class='title-bar' >
					<div class='title-text'>
						<h4> My Topics</h4>
					</div>
					
				</div>
				<div class='loadcontent' style='padding: 20px;'>
                <div class="row">
                    <div class="col-xs-12.col-md-12"> 
						<div class='widget'>							
							<div class='widget-body'>
							  <form id='mycompetencyFrm'>
									<div id="accordion"></div>
									<br>
									<br>
							<!--
									<div class="col-md-12">
										<input type="button" id="updateBtn" name="Update" value="Update"> <input
											type="button" id="freezeBtn" name="Freeze" value="Freeze">
											<input type="button" id="syncBtn" name="Sync" value="Sync">
									</div> -->
									</form>
							</div>
						</div>
                    </div>
                </div>
                <!-- /.row -->
                
