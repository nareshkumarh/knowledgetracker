<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Welcome to Knowledge Assessment Test</title>
	<!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/compass.css" rel="stylesheet">
	<link rel="stylesheet" href="../css/jquery-ui.min.css">  
	
	<script src="../js/jquery-2.1.1.min.js"></script>
	<script src="../js/jquery-ui.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>	  
	<script src="../js/underscore-min.js"></script>
	<script src="../js/underscore-min.map"></script>	  
	<script src="../js/handlebars-v2.0.0.js"></script>
	<script src="../js/jqGrid/jquery.jqGrid.js"></script>	
	
	<script src="../js/jqGrid/i18n/grid.locale-en.js"></script>
		<script src="../js/jqplot/jquery.jqplot.min.js"></script>
		<script class="include" type="text/javascript" src="../js/jqplot/jquery.jqplot.min.js"></script>
		<script class="include" type="text/javascript" src="../js/jqplot/scripts/shCore.min.js"></script>
		<script class="include" type="text/javascript" src="../js/jqplot/scripts/shBrushJScript.min.js"></script>
		<script class="include" type="text/javascript" src="../js/jqplot/scripts/shBrushXml.min.js"></script>
		
		<script src="../js/jqplot/jqplot.pieRenderer.min.js"></script>
		<script src="../js/jqplot/jqplot.donutRenderer.min.js"></script>
	<script src="../js/app-json.js"></script>
	<script src="../js/app.js"></script>
	<script src="../js/cnsiJS.js"></script>
	<script src="../js/apply.js"></script>	
	
	<script id="users-template" type="text/x-handlebars-template">
{{#each userCompetency }}
<h3> {{ @key }} </h3>
<div> 
<table border='1' width='100%'><thead><tr><th width='5%'>Sno</th><th width='80%'> Description </th><th width='10%'>Scale </th></tr></thead><tbody> 		 
{{#each this }}
<tr><td>  {{plusOne @index }} </td><td> {{ this.cname }} </td><td> {{ listscale user_comp_sid scale }} </td></tr>
{{/each}}
</tbody></table>
 </div>
{{/each}}
		  
	</script>  
	<script>

	var source   = $("#users-template").html();
	var template = Handlebars.compile(source);
	
	
	
	$(document).ready ( function () {
		
		console.log("jquery loaded");
		var username = localStorage.getItem("username");
		console.log('usercompetency:' + username);
		if ( !localStorage.getItem("username") )
		{
			console.log("Invalid Credentials");
		}
		else
		{
			$("#username").val(username);
		}
		$("#updateBtn").on ('click', function (e) {
			console.log("Update Btn clicked");
			var competencies = {};
			$('#mycompetencyFrm select').each(function() {
				   console.log('select' + this.id + ":"+ $(this).val() );
				   competencies[this.id] = $(this).val();
			});
			var formData = {};
			formData["competencies"] =  competencies;
			alert(JSON.stringify(formData) );

			$.post('../userCompetency/update/naresh', {data:competencies} , {dataType: 'json'}, function(data) {
				alert(data);
			});
		});
		
		$.getJSON('../userCompetency/'+ username, function (data) {
			console.log(data);		
			var competencySetNames = _.unique( _.pluck(data, 'set_name' ));
			var competencyNames = _.unique( _.pluck(data, 'cname' ));
			console.log(competencySetNames);
			var userCompetencyData = {};
			for ( var c in competencySetNames )
			{
				var setName = competencySetNames[c];
				var userComp = _.where( data, {"set_name": setName});				
				userCompetencyData[setName]= userComp;
			}
			console.log(competencyNames);
			var html    = template({ "competencySetNames": competencySetNames , "competencyNames": competencyNames , "userCompetency" : userCompetencyData });			
			$("#accordion").append(html);
			$( "#accordion" ).accordion({ heightStyle: "content" });
		});
	});


	Handlebars.registerHelper('plusOne', function(number) {
	    return number + 1;
	});

	Handlebars.registerHelper('listscale', function(cid,scale) {
		var content = "";
		content+= "<select id="+ cid +">"; 
		content+="<option value='L1' "+ ( scale=='L1' ? 'selected' : '') +"> L1-Knowledge </option>";
		content+="<option value='L2' "+ ( scale=='L2' ? 'selected' : '') + "> L2-Comprehension </option>";
		content+="<option value ='L3' "+ ( scale=='L3' ? 'selected' : '') + "> L3-Application </option>";
		return new Handlebars.SafeString(content);
	});
	
	
	</script>
</head>
<body class="pagecontainer"> <!-- custom -->
<!-- Top Navigation Start -->
	 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../../">Knowledge Assessment Test</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
         Welcome <label id="username" ></label>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <!-- Dashboard Start -->
		<div id="page-container">
			<div id="navigation_left" class="sidebar" data-load-page="../../pages/left.html" data-keypage="navigationLeft">

			</div>
			<div id="page_content" class="page-content" role="main-container">
			
			<div id="load_page" data-load-page="../../pages/dashboard.html" data-keypage="dashboard">

				</div>
 
				<!--//footer-->
				<!-- <div class="footer" data-load-page="footer.html" data-keypage="footer">-->

				</div>
				<!--//footer-->

			</div>
		</div>
		</div>
		<!-- Dashboard End -->
		
    </div>
    
</body>
</html>
